#ifndef VERIFICATOR_H
#define VERIFICATOR_H

#include "GameData.h"

#include <algorithm>

namespace abalone
{
  class Verificator
  {
    typedef std::shared_ptr<pointVector> pointVecShr;
  public:
    static sf::Vector2i calculateNewPos(const sf::Vector2i pos, const int angle);
    static bool verificate_R_L(pointVecShr &clicked, int angle, GameData::Ptr &data, pointVecShr &moveVec);
    static bool verificate_D_U(pointVecShr &clicked, int angle, GameData::Ptr &data, pointVecShr &moveVec);
    static bool verificate_D_R(pointVecShr &clicked, int angle, GameData::Ptr &data, pointVecShr &moveVec);
    static void sortClicked(pointVecShr &clicked);

  protected:
    static bool verificate_0_R_L(pointVecShr &clicked, GameData::Ptr &data, pointVecShr &moveVec);
    static bool verificate_60_R_L(pointVecShr &clicked, GameData::Ptr &data);
    static bool verificate_120_R_L(pointVecShr &clicked, GameData::Ptr &data);
    static bool verificate_180_R_L(pointVecShr &clicked, GameData::Ptr &data, pointVecShr &moveVec);
    static bool verificate_240_R_L(pointVecShr &clicked, GameData::Ptr &data);
    static bool verificate_300_R_L(pointVecShr &clicked, GameData::Ptr &data);

    static bool verificate_0_D_U(pointVecShr &clicked, GameData::Ptr &data);
    static bool verificate_60_D_U(pointVecShr &clicked, GameData::Ptr &data, pointVecShr &moveVec);
    static bool verificate_120_D_U(pointVecShr &clicked, GameData::Ptr &data);
    static bool verificate_180_D_U(pointVecShr &clicked, GameData::Ptr &data);
    static bool verificate_240_D_U(pointVecShr &clicked, GameData::Ptr &data, pointVecShr &moveVec);
    static bool verificate_300_D_U(pointVecShr &clicked, GameData::Ptr &data);

    static bool verificate_0_D_R(pointVecShr &clicked, GameData::Ptr &data);
    static bool verificate_60_D_R(pointVecShr &clicked, GameData::Ptr &data);
    static bool verificate_120_D_R(pointVecShr &clicked, GameData::Ptr &data, pointVecShr &moveVec);
    static bool verificate_180_D_R(pointVecShr &clicked, GameData::Ptr &data);
    static bool verificate_240_D_R(pointVecShr &clicked, GameData::Ptr &data);
    static bool verificate_300_D_R(pointVecShr &clicked, GameData::Ptr &data, pointVecShr &moveVec);
  };
}
#endif //VERIFICATOR_H