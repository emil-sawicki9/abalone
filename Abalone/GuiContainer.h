#ifndef CONTAINER_H
#define CONTAINER_H

#include "Component.h"

#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/RenderTarget.hpp>

#include <vector>

namespace GUI
{
  class GuiContainer : public Component
  {
  public:
    typedef std::shared_ptr<GuiContainer> Ptr;
    
  public:
    GuiContainer();
    GuiContainer(bool horizontalEvent);

    void				    push(Component::Ptr component);

    virtual bool		isSelectable() const;
    virtual void		handleEvent(const sf::Event &event);
    
  private:
    virtual void		draw(sf::RenderTarget &target, sf::RenderStates states) const;

    bool				hasSelection() const;
    void				select(std::size_t index);
    void				selectNext();
    void				selectPrevious();
    
  private:
    bool                          _horizontalEvents;
    std::vector<Component::Ptr>		_children;
    int								            _selectedChild;
  };
}

#endif // CONTAINER_H
