#ifndef PAUSESTATE_H
#define PAUSESTATE_H

#include "State.h"
#include "Utility.h"
#include "ResourceHolder.h"

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Text.hpp>

namespace state
{
  class PauseState : public State
  {
  public:
    PauseState(StateStack &stack, Context::Ptr &context);

    virtual void		draw();
    virtual bool		update(sf::Time dt);
    virtual bool		handleEvent(const sf::Event &event);


  private:
    void            initlialize(Context::Ptr &context);

    sf::Sprite		  _backgroundSprite;
    sf::Text			  _pausedText;
    sf::Text			  _instructionText, _instructionText2;
  };
}
#endif // PAUSESTATE_H