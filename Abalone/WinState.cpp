#include "WinState.h"

state::WinState::WinState(StateStack &stack, Context::Ptr &context)
  : State(stack, context)
  , _backgroundSprite()
  , _winText()
  , _instructionText()
{
  initialize(context);
}

state::WinState::~WinState()
{

}

void state::WinState::initialize(Context::Ptr &context)
{
  sf::Font &font = context->fonts->get(fonts::Main);
  sf::Vector2f viewSize = context->window->getView().getSize();

  _winText.setFont(font);
  
  int &winColor = context->winColor;
  if (winColor == PAWN_WHITE)
  {
    _winText.setString("White won the game!");
  }
  else if (winColor == PAWN_BLACK)
  {
    _winText.setString("Black won the game!");
  }
  else
  {
    _winText.setString("Won the game!\nError, player color not found!");
    _winText.setColor(sf::Color::Red);
  }

  _winText.setCharacterSize(70);
  utility::centerOrigin(_winText);
  _winText.setPosition(0.5f * viewSize.x, 0.4f * viewSize.y);

  _instructionText.setFont(font);
  _instructionText.setString("(Press Enter to return to the main menu)");
  utility::centerOrigin(_instructionText);
  _instructionText.setPosition(0.5f * viewSize.x, 0.6f * viewSize.y);

  _backgroundSprite.setFillColor(sf::Color(0, 0, 0, 150));
}

void state::WinState::draw()
{
  sf::RenderWindow &window = *State::getContext()->window;
  window.setView(*getContext()->gameView);

  _backgroundSprite.setSize(window.getView().getSize());

  window.draw(_backgroundSprite);
  window.draw(_winText);
  window.draw(_instructionText);
}

bool state::WinState::update(sf::Time)
{
  return false;
}

bool state::WinState::handleEvent(const sf::Event &event)
{
  if (event.type != sf::Event::KeyReleased)
    return false;

  if (event.key.code == sf::Keyboard::Return)
  {
    requestStateClear();
    requestStackPush(state::Menu);
  }

  return false;
}