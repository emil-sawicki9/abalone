#ifndef GAMEDATA_H
#define GAMEDATA_H

#include "Globals.h"

#include <SFML/System/Vector2.hpp>

#include <memory>
#include <vector>

typedef std::vector<sf::Vector2i> pointVector;

namespace abalone
{
  struct MoveData
  {
    std::shared_ptr<pointVector>  clickedPawns;
    MoveDirection                     direction;
    MoveAngle                     angle;
  };

  class GameData
  {
  public:
    typedef std::unique_ptr<GameData> Ptr;

    GameData();
    GameData(const GameData &otherData);
    ~GameData();

    void            writeBoard(); // debug
    int             getColor(const int x, const int y) const;
    int             getColor(const sf::Vector2i pos) const;
    int             getNumberOfPawns(const int color) const;
    sf::Vector2i    getCenter() const;

    void            makeMove(std::shared_ptr<pointVector> &clickedPawns, const int angle, const int dir);
    void            makeMove_R_L(std::shared_ptr<pointVector> &clickedPawns, const int angle);
    void            makeMove_D_U(std::shared_ptr<pointVector> &clickedPawns, const int angle);
    void            makeMove_D_R(std::shared_ptr<pointVector> &clickedPawns, const int angle);
    void            setColor(const int x, const int y, const int color);
    void            setColor(const sf::Vector2i pos, const  int color);
    void            countNumberOfPawns();
    void            cleanOutside();
    int             calculateFieldNumber(sf::Vector2i pos);

  private:
    void            initialize();

    unsigned char   _board[BOARD_SIZE][BOARD_SIZE];
    int             _numberOfBlackPawns;
    int             _numberOfWhitePawns;
  };
}
#endif // GAMEDATA_H