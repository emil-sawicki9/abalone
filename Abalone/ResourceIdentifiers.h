#ifndef RESOURCEIDENTIFIERS_H
#define RESOURCEIDENTIFIERS_H

// Forward declaration of SFML classes
namespace sf
{
  class Texture;
  class Font;
}

namespace textures
{
  enum ID
  {
    TitleScreen,
    ButtonNormal,
    ButtonSelected,
    ButtonPressed,
    PawnWhite,
    LoadingSpin
  };
}

namespace fonts
{
  enum ID
  {
    Main
  };
}

// Forward declaration and a few type definitions
template <typename Resource, typename Identifier>
class ResourceHolder;

typedef ResourceHolder<sf::Texture, textures::ID>	TextureHolder;
typedef ResourceHolder<sf::Font, fonts::ID>			  FontHolder;

#endif // RESOURCEIDENTIFIERS_H
