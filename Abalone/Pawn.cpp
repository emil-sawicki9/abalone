#include "Pawn.h"

abalone::Pawn::Pawn(sf::Sprite sprite, sf::Vector2f position, sf::Vector2i position_on_board )
: _on_board(true)
, _is_clicked(false)
, _isDead(false)
, _pawnSprite(sprite)
, _absolutePos(position)
, _positionOnBoard(position_on_board)
{
  initialize();
};

abalone::Pawn::~Pawn()
{
}

void abalone::Pawn::initialize()
{
  _pawnSprite.setPosition(_absolutePos);
  _mainColor = _pawnSprite.getColor();
}

sf::Vector2i abalone::Pawn::getPosition() const
{
  return _positionOnBoard;
}

void abalone::Pawn::move(sf::Vector2i newPosOnBoard, sf::Vector2f newPosAbsolute)
{
  _positionOnBoard = newPosOnBoard; // change board position
  _absolutePos = newPosAbsolute; // change texture position in window
  _pawnSprite.setPosition(_absolutePos);
  setClicked(false);  // unclick
}

void abalone::Pawn::die(sf::Vector2f deadPos)
{
  _positionOnBoard = sf::Vector2i(-100,-100); // change board position
  _absolutePos = deadPos; // change texture position in window
  _pawnSprite.setPosition(_absolutePos);
  setClicked(false);  // unclick
  _isDead = true;
}

bool abalone::Pawn::isDead()
{  return _isDead;
}

void abalone::Pawn::setClicked(bool status)
{
  if (_isDead)
    return;

  if (status)
    _pawnSprite.setColor(sf::Color(40,40,255,235));
  else
    _pawnSprite.setColor(_mainColor);
  _is_clicked = status;
}

bool abalone::Pawn::isClicked()
{
  return _is_clicked;
}

sf::Sprite abalone::Pawn::getPawn() const
{
  return _pawnSprite;
}