#ifndef GAMEPLAY_H
#define GAMEPLAY_H

#include <SFML/Graphics.hpp>

#include <iostream>
#include <vector>
#include <algorithm>


#include "Pawn.h"

struct PawnPos
{
  int index;
  sf::CircleShape moveCircle;
  sf::Vector2i pos;
};

// Sort clicked pawns
bool sortClickedByPosition(const PawnPos &lhs, const PawnPos &rhs);
bool sortClickedByPositionInvert(const PawnPos &lhs, const PawnPos &rhs);

class GamePlay : public sf::Drawable
{
public:
  GamePlay() :_is_active(false), 
    _whiteTurn(true),
    _numberDeadWhite(0),
    _numberDeadBlack(0)
  {
    initialize();
  }
  ~GamePlay() {};

  bool isActive();
  void setActive(bool);
  void pawnClicked(PawnPos);  // clicked pawn handler
  void clickEvent(sf::Event); // click event handler
  void lightMove(sf::Vector2i); // Hightlight possible moves

private:
  bool _is_active;  // if gameplay is active
  bool _whiteTurn;
  sf::Font _font;
  sf::Text _currentTurn, _deadWhite, _deadBlack;
  int _numberDeadWhite, _numberDeadBlack;
  std::vector<PawnPos> _clickedPawns;
  std::vector<sf::Vector2i> _can_click; // Where can click
  std::vector<std::vector<PawnPos>> _can_move; // Where can move
  std::vector<std::vector<PawnPos>> _can_hit; // Where can hit

  std::vector<std::vector<int>> _board; // gameplay board
  std::vector<std::vector<sf::CircleShape>> _pawnPlace; 
  std::vector<Pawn> _blackPawns; 
  std::vector<Pawn> _whitePawns; 
  sf::RectangleShape _boardRect; // background

  void initialize(); 
  bool checkClickAvailable(PawnPos,bool); // Check if pawn can be clicked
  void calculateCanClick(); // Check which pawn can be clicked
  void calculateCanMove(); // Check where pawns can be moved
  void calculateCanHit(); // Check which pawns can be hit
  std::vector<PawnPos> calculateMoveRightLeft(PawnPos); // Moving right or left
  void changeTurn();
  void makeMove(int);
  void makeHit(int);
  int min(int, int);
  int max(int, int);
  int mid(int, int, int);

  //DEBUG
  void debugWriteBoard();
  //DEBUG

  virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

};

#endif // GAMEPLAY_H