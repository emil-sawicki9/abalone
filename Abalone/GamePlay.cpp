#include "GamePlay.h"

void GamePlay::debugWriteBoard()
{
  // DEBUG -- wypisanie tablicy planszy
  std::cout << std::endl;
  int floor = 4;
  for (size_t i = 0; i < _board.size(); i++)
  {
    for (int j = 0; j < floor; j++)
    {
      std::cout << " ";
    }
    for (size_t j = 0; j < _board[i].size(); j++)
    {
      int nr = _board[i][j];
      if (nr / 10 > 0 || nr < 0)
        std::cout << _board[i][j] << " ";
      else
        std::cout << _board[i][j] << "  ";
    }
    std::cout << std::endl;
    if (i >= 4)
      floor++;
    else
      floor--;
  }
  std::cout << std::endl;
}



void GamePlay::calculateCanHit()
{
  // Clear attack possibilities vector
  _can_hit.clear();
  
  if (_clickedPawns.size() < 2)
    return;

  std::vector<sf::Vector2i> hitLocation1, hitLocation2;
  int maxX = 0;
  int minX = 10;
  int maxY = 0;
  int minY = 10;
  for (size_t i = 0; i < _clickedPawns.size(); i++)
  {
    maxX = max(maxX, _clickedPawns[i].pos.x);
    minX = min(minX, _clickedPawns[i].pos.x);
    maxY = max(maxY, _clickedPawns[i].pos.y);
    minY = min(minY, _clickedPawns[i].pos.y);
  }
  // TODO: optymalizacja powtorzen
  //==================================================================
  // If in same row
  //==================================================================
  if (maxX == minX)
  {
    int rowLastIndex = _board[maxX].size() - 1;
    //==================================================================
    // left
    //==================================================================
    if (minY == 1)
    {
      hitLocation1.push_back(sf::Vector2i(maxX, minY - 1));
      hitLocation1.push_back(sf::Vector2i(maxX, minY - 2));
    }
    else if (minY > 1)
    {
      hitLocation1.push_back(sf::Vector2i(maxX, minY - 1));
      hitLocation1.push_back(sf::Vector2i(maxX, minY - 2));
      if (_clickedPawns.size() == 3)
        hitLocation1.push_back(sf::Vector2i(maxX, minY - 3));
    }
    //==================================================================
    // right
    //==================================================================
    if (maxY == rowLastIndex - 1)
    {
      hitLocation2.push_back(sf::Vector2i(maxX, maxY + 1));
      hitLocation2.push_back(sf::Vector2i(maxX, maxY + 2));
    }
    else if (maxY < rowLastIndex - 1)
    {
      hitLocation2.push_back(sf::Vector2i(maxX, maxY + 1));
      hitLocation2.push_back(sf::Vector2i(maxX, maxY + 2));
      if (_clickedPawns.size() == 3)
        hitLocation2.push_back(sf::Vector2i(maxX, maxY + 3));
    }
  }
  //==================================================================
  // If in same column
  //==================================================================
  else if (maxY == minY)
  {
    if (_clickedPawns.size() == 3)
    {
      switch (minX)
      {
        //==================================================================
        // Top part with 3
        //==================================================================
        case 0 :
          // down
          hitLocation1.push_back(sf::Vector2i(maxX + 1, minY));
          hitLocation1.push_back(sf::Vector2i(maxX + 2, minY));
          hitLocation1.push_back(sf::Vector2i(maxX + 3, minY - 1));
          break;
        case 1 :
          // down
          hitLocation1.push_back(sf::Vector2i(maxX + 1, minY));
          hitLocation1.push_back(sf::Vector2i(maxX + 2, minY - 1));
          if (minY > 1)
            hitLocation1.push_back(sf::Vector2i(maxX + 3, minY - 2));
          // up
          if (minY < 5)
          {
            hitLocation2.push_back(sf::Vector2i(minX - 1, minY));
            hitLocation2.push_back(sf::Vector2i(minX - 2, minY));
          }
          break;
        case 2:
          // down
          if (minY > 0)
          {
            hitLocation1.push_back(sf::Vector2i(maxX + 1, minY - 1));
            hitLocation1.push_back(sf::Vector2i(maxX + 2, minY - 2));
            if (minY > 1)
              hitLocation1.push_back(sf::Vector2i(maxX + 3, minY - 3));
          }
          // up
          if (minY < 6)
          {
            hitLocation2.push_back(sf::Vector2i(minX - 1, minY));
            hitLocation2.push_back(sf::Vector2i(minX - 2, minY));
            if (minY < 5)
              hitLocation2.push_back(sf::Vector2i(minX - 3, minY));
          }
          break;
        //==================================================================
        // Bottom part with 3
        //==================================================================
        case 4 :
          if (minY > 0)
          {
            // up
            hitLocation1.push_back(sf::Vector2i(minX - 1, minY - 1));
            hitLocation1.push_back(sf::Vector2i(minX - 2, minY - 2));
            if (minY > 1)
              hitLocation1.push_back(sf::Vector2i(minX - 3, minY - 3));
          }
          // down
          if (minY < 6)
          {
            hitLocation2.push_back(sf::Vector2i(maxX + 1, minY));
            hitLocation2.push_back(sf::Vector2i(maxX + 2, minY));
            if (minY < 5)
              hitLocation2.push_back(sf::Vector2i(maxX + 3, minY));
          }
          break;
        case 5 :
          // up
          hitLocation1.push_back(sf::Vector2i(minX - 1, minY));
          hitLocation1.push_back(sf::Vector2i(minX - 2, minY - 1));
          if (minY > 1)
            hitLocation1.push_back(sf::Vector2i(minX - 3, minY - 2));
          // down
          if (minY < 5)
          {
            hitLocation2.push_back(sf::Vector2i(maxX + 1, minY));
            hitLocation2.push_back(sf::Vector2i(maxX + 2, minY));
          }
          break;
        case 6 :
          // down
          hitLocation2.push_back(sf::Vector2i(minX - 1, minY));
          hitLocation2.push_back(sf::Vector2i(minX - 2, minY));
          hitLocation2.push_back(sf::Vector2i(minX - 3, minY - 1));
          break;
      }
    }
    else //if (_clickedPawns.size() == 2)
    {
      switch (minX)
      {
        //==================================================================
        // Top part with 2
        //==================================================================
        case 0 :
          // down
          hitLocation1.push_back(sf::Vector2i(maxX + 1, minY));
          hitLocation1.push_back(sf::Vector2i(maxX + 2, minY));
          break;
        case 1 :
          // down
          hitLocation1.push_back(sf::Vector2i(maxX + 1, minY));
          hitLocation1.push_back(sf::Vector2i(maxX + 2, minY));
          // up
          if (minY < 5)
          {
            hitLocation2.push_back(sf::Vector2i(minX - 1, minY));
            hitLocation2.push_back(sf::Vector2i(minX - 2, minY));
          }
          break;
        case 2 :
          // down
          hitLocation1.push_back(sf::Vector2i(maxX + 1, minY));
          hitLocation1.push_back(sf::Vector2i(maxX + 2, minY - 1));
          // up
          if (minY < 6)
          {
            hitLocation2.push_back(sf::Vector2i(minX - 1, minY));
            hitLocation2.push_back(sf::Vector2i(minX - 2, minY));
          }
          break;
        case 3 :
          // down
          if (minY > 0)
          {
            hitLocation1.push_back(sf::Vector2i(maxX + 1, minY - 1));
            hitLocation1.push_back(sf::Vector2i(maxX + 2, minY - 2));
          }
          // up
          if (minY < 7)
          {
            hitLocation2.push_back(sf::Vector2i(minX - 1, minY));
            hitLocation2.push_back(sf::Vector2i(minX - 2, minY));
          }
          break;
        //==================================================================
        // Bottom part with 2
        //==================================================================
        case 4 :
          if (minY > 0)
          {
            // up
            hitLocation1.push_back(sf::Vector2i(minX - 1, minY - 1));
            hitLocation1.push_back(sf::Vector2i(minX - 2, minY - 2));
          }
          // down
          if (minY < 7)
          {
            hitLocation2.push_back(sf::Vector2i(maxX + 1, minY));
            hitLocation2.push_back(sf::Vector2i(maxX + 2, minY));
          }
          break;
        case 5 :
          // up
          hitLocation1.push_back(sf::Vector2i(minX - 1, minY));
          hitLocation1.push_back(sf::Vector2i(minX - 2, minY - 1));
          // down
          if (minY < 6)
          {
            hitLocation2.push_back(sf::Vector2i(maxX + 1, minY));
            hitLocation2.push_back(sf::Vector2i(maxX + 2, minY));
          }
          break;
        case 6 :
          // up
          hitLocation1.push_back(sf::Vector2i(minX - 1, minY));
          hitLocation1.push_back(sf::Vector2i(minX - 2, minY));
          // down
          if (minY < 5)
          {
            hitLocation2.push_back(sf::Vector2i(maxX + 1, minY));
            hitLocation2.push_back(sf::Vector2i(maxX + 2, minY));
          }
          break;
        case 7 :
          // up
          hitLocation1.push_back(sf::Vector2i(minX - 1, minY));
          hitLocation1.push_back(sf::Vector2i(minX - 2, minY));
          break;
      }
    }
  }
  //==================================================================
  // If in different column
  //==================================================================
  else
  {
    if (_clickedPawns.size() == 3)
    {
      sort(_clickedPawns.begin(), _clickedPawns.end(), sortClickedByPosition);
      int maxXRowLastElement = _board[maxX].size() - 1;
      int minXRowLastElement = _board[minX].size() - 1;
      switch (minX)
      {
        //==================================================================
        // Top part
        //==================================================================
        case 0 :
          // down
          hitLocation1.push_back(sf::Vector2i(maxX + 1, maxY + 1));
          hitLocation1.push_back(sf::Vector2i(maxX + 2, maxY + 2));
          hitLocation1.push_back(sf::Vector2i(maxX + 3, maxY + 2));
          break;
        case 1 :
          // down
          hitLocation1.push_back(sf::Vector2i(maxX + 1, maxY + 1));
          hitLocation1.push_back(sf::Vector2i(maxX + 2, maxY + 1));
          if (maxY < maxXRowLastElement)
            hitLocation1.push_back(sf::Vector2i(maxX + 3, maxY + 1));
          // up
          if (minY > 0)
          {
            hitLocation2.push_back(sf::Vector2i(minX - 1, minY - 1));
            hitLocation2.push_back(sf::Vector2i(minX - 2, minY - 2));
          }
          break;
        case 2 :
          // down
          if (maxY < maxXRowLastElement)
          {
            hitLocation1.push_back(sf::Vector2i(maxX + 1, maxY));
            hitLocation1.push_back(sf::Vector2i(maxX + 2, maxY));
            if (maxY < maxXRowLastElement - 1)
              hitLocation1.push_back(sf::Vector2i(maxX + 3, maxY));              
          }
          // up
          if (minY > 0)
          {
            hitLocation2.push_back(sf::Vector2i(minX - 1, minY - 1));
            hitLocation2.push_back(sf::Vector2i(minX - 2, minY - 2));
            if (minY > 1)
              hitLocation2.push_back(sf::Vector2i(minX - 3, minY - 3));
          }
          break;
        //==================================================================
        // Middle part
        //==================================================================
        case 3 :
          // If maxY is in maxX row
          if (maxY == _clickedPawns[2].pos.y)
          {
            // down
            if (maxY < maxXRowLastElement)
            {
              hitLocation1.push_back(sf::Vector2i(maxX + 1, maxY));
              hitLocation1.push_back(sf::Vector2i(maxX + 2, maxY));
              if (maxY < maxXRowLastElement - 1)
                hitLocation1.push_back(sf::Vector2i(maxX + 3, maxY));
            }
            // up
            if (minY > 0)
            {
              hitLocation2.push_back(sf::Vector2i(minX - 1, minY - 1));
              hitLocation2.push_back(sf::Vector2i(minX - 2, minY - 2));
              if (minY > 1)
                hitLocation2.push_back(sf::Vector2i(minX - 3, minY - 3));
            }
          }
          // If maxY is in minX row
          else if (maxY == _clickedPawns[0].pos.y)
          {
            // up
            if (maxY < minXRowLastElement)
            {
              hitLocation1.push_back(sf::Vector2i(minX - 1, maxY));
              hitLocation1.push_back(sf::Vector2i(minX - 2, maxY));
              if (maxY < maxXRowLastElement - 1)
                hitLocation1.push_back(sf::Vector2i(minX - 3, maxY));
            }
            // down
            if (minY > 0)
            {
              hitLocation2.push_back(sf::Vector2i(maxX + 1, minY - 1));
              hitLocation2.push_back(sf::Vector2i(maxX + 2, minY - 2));
              if (minY > 1)
                hitLocation2.push_back(sf::Vector2i(maxX + 3, minY - 3));
            }
          }
          break;
        //==================================================================
        // Bottom part
        //==================================================================
        case 4 :
          // up
          if (maxY < minXRowLastElement)
          {
            hitLocation1.push_back(sf::Vector2i(minX - 1, maxY));
            hitLocation1.push_back(sf::Vector2i(minX - 2, maxY));
            if (maxY < maxXRowLastElement - 1)
              hitLocation1.push_back(sf::Vector2i(minX - 3, maxY));
          }
          // down
          if (minY > 0)
          {
            hitLocation2.push_back(sf::Vector2i(maxX + 1, minY - 1));
            hitLocation2.push_back(sf::Vector2i(maxX + 2, minY - 2));
            if (minY > 1)
              hitLocation2.push_back(sf::Vector2i(maxX + 3, minY - 3));
          }
          break;
        case 5 :
          // up
          hitLocation1.push_back(sf::Vector2i(minX - 1, maxY + 1));
          hitLocation1.push_back(sf::Vector2i(minX - 2, maxY + 1));
          if (maxY < maxXRowLastElement)
            hitLocation1.push_back(sf::Vector2i(minX - 3, maxY + 1));
          // down
          if (minY > 0)
          {
            hitLocation2.push_back(sf::Vector2i(maxX + 1, minY - 1));
            hitLocation2.push_back(sf::Vector2i(maxX + 2, minY - 2));
          }
          break;
        case 6 :
          // up
          hitLocation1.push_back(sf::Vector2i(minX - 1, maxY + 1));
          hitLocation1.push_back(sf::Vector2i(minX - 2, maxY + 2));
          hitLocation1.push_back(sf::Vector2i(minX - 3, maxY + 2));
          break;
      }
    }
    else //if (_clickedPawns.size() == 2)
    {
      int maxXRowLastElement = _board[maxX].size() - 1;
      int minXRowLastElement = _board[minX].size() - 1;
      switch (minX)
      {
      //==================================================================
      // Top part
      //==================================================================
      case 0:
        // down
        hitLocation1.push_back(sf::Vector2i(maxX + 1, maxY + 1));
        hitLocation1.push_back(sf::Vector2i(maxX + 2, maxY + 2));
        break;
      case 1:
        // down
        hitLocation1.push_back(sf::Vector2i(maxX + 1, maxY + 1));
        hitLocation1.push_back(sf::Vector2i(maxX + 2, maxY + 2));
        // up
        if (minY > 0)
        {
          hitLocation2.push_back(sf::Vector2i(minX - 1, minY - 1));
          hitLocation2.push_back(sf::Vector2i(minX - 2, minY - 2));
        }
        break;
      case 2:
        // down
        hitLocation1.push_back(sf::Vector2i(maxX + 1, maxY + 1));
        hitLocation1.push_back(sf::Vector2i(maxX + 2, maxY + 1));
        // up
        if (minY > 0)
        {
          hitLocation2.push_back(sf::Vector2i(minX - 1, minY - 1));
          hitLocation2.push_back(sf::Vector2i(minX - 2, minY - 2));
        }
        break;
      case 3:
        // down
        if (maxY < maxXRowLastElement)
        {
          hitLocation1.push_back(sf::Vector2i(maxX + 1, maxY));
          hitLocation1.push_back(sf::Vector2i(maxX + 2, maxY));
        }
        // up
        if (minY > 0)
        {
          hitLocation2.push_back(sf::Vector2i(minX - 1, minY - 1));
          hitLocation2.push_back(sf::Vector2i(minX - 2, minY - 2));
        }
        break;
      //==================================================================
      // Bottom part
      //==================================================================
      case 4:
        // up
        if (maxY < minXRowLastElement)
        {
          hitLocation1.push_back(sf::Vector2i(minX - 1, maxY));
          hitLocation1.push_back(sf::Vector2i(minX - 2, maxY));
        }
        // down
        if (minY > 0)
        {
          hitLocation2.push_back(sf::Vector2i(maxX + 1, minY - 1));
          hitLocation2.push_back(sf::Vector2i(maxX + 2, minY - 2));
        }
        break;
      case 5:
        // up
        hitLocation1.push_back(sf::Vector2i(minX - 1, maxY + 1));
        hitLocation1.push_back(sf::Vector2i(minX - 2, maxY + 1));
        // down
        if (minY > 0)
        {
          hitLocation2.push_back(sf::Vector2i(maxX + 1, minY - 1));
          hitLocation2.push_back(sf::Vector2i(maxX + 2, minY - 2));
        }
        break;
      case 6:
        // up
        hitLocation1.push_back(sf::Vector2i(minX - 1, maxY + 1));
        hitLocation1.push_back(sf::Vector2i(minX - 2, maxY + 2));
        // down
        if (minY > 0)
        {
          hitLocation2.push_back(sf::Vector2i(maxX + 1, minY - 1));
          hitLocation2.push_back(sf::Vector2i(maxX + 2, minY - 2));
        }
        break;
      case 7:
        // up
        hitLocation1.push_back(sf::Vector2i(minX - 1, maxY + 1));
        hitLocation1.push_back(sf::Vector2i(minX - 2, maxY + 2));
        break;
      }
    }
  }

  //==================================================================
  // Fill positions to _can_hit vector
  //==================================================================
  std::vector<PawnPos> hitLoc;
  for (size_t i = 0; i < hitLocation1.size(); i++)
  {
    PawnPos pawnPos_tmp;
    pawnPos_tmp.pos = hitLocation1[i];
    hitLoc.push_back(pawnPos_tmp);
  }
  if (hitLoc.size() > 0)
  {
    _can_hit.push_back(hitLoc);
    hitLoc.clear();
  }

  for (size_t i = 0; i < hitLocation2.size(); i++)
  {
    PawnPos pawnPos_tmp;
    pawnPos_tmp.pos = hitLocation2[i];
    hitLoc.push_back(pawnPos_tmp);
  }
  if (hitLoc.size() > 0)
  {
    _can_hit.push_back(hitLoc);
    hitLoc.clear();
  }

  //==================================================================
  // Set circleshape to hit possibility
  //==================================================================
  for (size_t i = 0; i < _can_hit.size(); i++)
  {
    bool canHit = false;
    bool secondEmpty = false;
    for (size_t j = 0; j < _can_hit[i].size(); j++)
    {
      PawnPos &whereToHit = _can_hit[i][j];
      // Last element is position where to move enemy pawn
      if (j == _can_hit[i].size() - 1)
      {
        // If in board
        if (whereToHit.pos.x >= 0 &&
          whereToHit.pos.x <= 8 &&
          whereToHit.pos.y >= 0 &&
          whereToHit.pos.y <= (float)_board[whereToHit.pos.x].size() - 1)
        {
          if (_board[whereToHit.pos.x][whereToHit.pos.y] != 0)
            canHit = false;
        }
        else
        {
          // If enemy is moved out of board
          if (canHit)
            whereToHit.index = 666;
        }
        break;
      }
      // If first is enemy pawn
      else if (j == 0)
      {
        bool enemy = (_whiteTurn) ? _board[_can_hit[i][0].pos.x][_can_hit[i][0].pos.y] < 0 : _board[_can_hit[i][0].pos.x][_can_hit[i][0].pos.y] > 0;
        if (enemy)
          canHit = true;
        else
          break;
      }
        

      if (canHit)
      {
        int index = _board[whereToHit.pos.x][whereToHit.pos.y];
        bool enemy = (_whiteTurn) ? index < 0 : index > 0;
        // If is enemy
        if (enemy)
        {
          sf::CircleShape triangle;
          triangle.setPointCount(3);
          triangle.setRadius(16);
          triangle.setFillColor(sf::Color::Transparent);
          sf::Vector2f pawnPlace_tmp = _pawnPlace[whereToHit.pos.x][whereToHit.pos.y].getPosition();
          triangle.setPosition(pawnPlace_tmp.x - 2, pawnPlace_tmp.y - 2);

          whereToHit.moveCircle = triangle;
        }
        // If is empty
        else if (index == 0)
        {
          _can_hit[i].pop_back();
        }
        // If is same color
        else
        {
          canHit = false;
          break;
        }
      }
    }

    // If first isnt enemy pawn or last place is filled then delete possibility
    if (!canHit)
    {
      _can_hit.erase(_can_hit.begin() + i);
      i--;
    }
  }
  
}


void GamePlay::changeTurn()
{
  _whiteTurn = !_whiteTurn;
  // White turn
  if (_whiteTurn)
  {
    _currentTurn.setString("Turn: White");
  }
  // Black turn
  else
  {
    _currentTurn.setString("Turn: Black");
  }
  _deadWhite.setString("White: " + std::to_string(_numberDeadWhite));
  _deadBlack.setString("Black: " + std::to_string(_numberDeadBlack));
}


void GamePlay::initialize()
{
  std::cout << "GamePlay init()" << std::endl;

  //==================================================================
  // Initialize tekst with what turn is now
  //==================================================================
  _font.loadFromFile("arial.ttf");
  _currentTurn.setFont(_font);
  _currentTurn.setCharacterSize(30);
  _currentTurn.setStyle(sf::Text::Bold);
  _currentTurn.setColor(sf::Color::Red);
  _currentTurn.setPosition(sf::Vector2f(400, 100));
  if (_whiteTurn)
    _currentTurn.setString("Turn: White");
  else
    _currentTurn.setString("Turn: Black");

  _deadWhite.setFont(_font);
  _deadWhite.setCharacterSize(30);
  _deadWhite.setStyle(sf::Text::Bold);
  _deadWhite.setColor(sf::Color::Blue);
  _deadWhite.setPosition(sf::Vector2f(400, 200));
  _deadWhite.setString("White: " + std::to_string(_numberDeadWhite));

  _deadBlack.setFont(_font);
  _deadBlack.setCharacterSize(30);
  _deadBlack.setStyle(sf::Text::Bold);
  _deadBlack.setColor(sf::Color::Blue);
  _deadBlack.setPosition(sf::Vector2f(400, 300));
  _deadBlack.setString("Black " + std::to_string(_numberDeadBlack));

  //==================================================================
  // Initialize board and pawns
  //==================================================================
  int floor = 5;
  for (int i = 0; i < 9; i++)
  {
    std::vector<int> intVec_tmp;
    for (int j = 0; j < floor; j++)
    {
      intVec_tmp.push_back(0);
    }
    _board.push_back(intVec_tmp);

    if (i < 4)
      floor++;
    else
      floor--;
  }

  float init_x = 100;
  float init_y = 100;
  float pawn_x = init_x;
  float pawn_y = init_y;
  float pawn_radius = 12;
  float colStart = 0;
  float colDist = 1.5;

  _boardRect.setPosition(0, 0);
  _boardRect.setSize(sf::Vector2f(800, 800));
  _boardRect.setFillColor(sf::Color::Color(192, 192, 192));

  //==================================================================
  // Initialize place for pawns
  //==================================================================
  for (size_t i = 0; i < _board.size(); i++)
  {
    std::vector<sf::CircleShape> pawnPlace_row;
    for (size_t j = 0; j < _board[i].size(); j++)
    {
      int boardFieldPawn = 0;
      // Add place for pawn
      sf::CircleShape pawnPlaceCircle;
      pawnPlaceCircle.setPosition(pawn_x, pawn_y);
      pawnPlaceCircle.setRadius(pawn_radius);
      pawnPlaceCircle.setFillColor(sf::Color::Red);
      pawnPlace_row.push_back(pawnPlaceCircle);

      //==================================================================
      // Add pawn
      //==================================================================
      if (i < 2 || i == 2 && j > 1 && j < 5)
      {
        Pawn newPawn(sf::Color::White, sf::Vector2f(pawn_x - 2, pawn_y - 2), sf::Vector2i(i, j));
        _whitePawns.push_back(newPawn);
        boardFieldPawn = _whitePawns.size();
      }
      else if (i > 6 || i == 6 && j > 1 && j < 5)
      {
        Pawn newPawn(sf::Color::Black, sf::Vector2f(pawn_x - 2, pawn_y - 2), sf::Vector2i(i, j));
        _blackPawns.push_back(newPawn);
        boardFieldPawn = _blackPawns.size() * -1;
      }

      _board[i][j] = boardFieldPawn;

      pawn_x += pawn_radius * 3;
    }
    _pawnPlace.push_back(pawnPlace_row);

    if (i < 4)
      colStart++;
    else
      colStart--;

    pawn_x = init_x - pawn_radius * colDist * colStart;
    pawn_y += pawn_radius * 3;
  }
}



bool GamePlay::isActive()
{
  return _is_active;
}



void GamePlay::setActive(bool status)
{
  _is_active = status;
}



void GamePlay::pawnClicked(PawnPos pawnPos)
{
  bool white;
  int &pawnNumber = pawnPos.index;

  if (pawnNumber != 0)
  {
    Pawn pawn;
    // White pawn
    if (pawnNumber > 0)
    {
      white = true;
      pawnNumber--;
      pawn = _whitePawns[pawnNumber];
    }
    // Black pawn 
    else
    {
      white = false;
      pawnNumber *= -1;
      pawnNumber--;
      pawn = _blackPawns[pawnNumber];
    }

    // If is dead
    if (pawn.isDead())
      return;

    //==================================================================
    // Unclick pawn
    //==================================================================
    if (pawn.isClicked())
    {
      bool can_delete = false;

      if (_clickedPawns.size() == 3)
      {
        std::vector<sf::Vector2i> clickedPawnsPos;
        sf::Vector2i nowClickedPos = pawnPos.pos;
        sf::Vector2i pawnPos1 = _clickedPawns[0].pos;
        sf::Vector2i pawnPos2 = _clickedPawns[1].pos;
        sf::Vector2i pawnPos3 = _clickedPawns[2].pos;

        int midX = mid(pawnPos1.x, pawnPos2.x, pawnPos3.x);
        int midY = mid(pawnPos1.y, pawnPos2.y, pawnPos3.y);

        if (!(nowClickedPos.x == midX && nowClickedPos.y == midY))
          can_delete = true;

      }
      else
        can_delete = true;

      //==================================================================
      // Delete clicked
      //==================================================================
      if (can_delete)
      {
        for (size_t i = 0; i < _clickedPawns.size(); i++)
        {
          if (_clickedPawns[i].index == pawnNumber)
          {
            _clickedPawns.erase(_clickedPawns.begin() + i);
            calculateCanClick();
            break;
          }
        }

        if (white)
          _whitePawns[pawnNumber].setClicked(false);
        else
          _blackPawns[pawnNumber].setClicked(false);
      }
    }
    //==================================================================
    // Clicking pawns 
    //==================================================================
    else
    {
      // Check if can click next pawn
      if (_clickedPawns.size() == 0)
      {
        _clickedPawns.push_back(pawnPos);
        calculateCanClick();

        if (white)
          _whitePawns[pawnNumber].setClicked(true);
        else
          _blackPawns[pawnNumber].setClicked(true);
      }
      // If clicked more than 1
      else if (checkClickAvailable(pawnPos, white))
      {
        _clickedPawns.push_back(pawnPos);
        calculateCanClick();
        if (white)
          _whitePawns[pawnNumber].setClicked(true);
        else
          _blackPawns[pawnNumber].setClicked(true);
      }
    }
  }
}



void GamePlay::calculateCanClick()
{
  // Clear vector of which pawns can be clicked
  _can_click.clear();

  switch (_clickedPawns.size())
  {
    // 1 clicked
  case 1:
  {
    sf::Vector2i pawnPos1 = _clickedPawns[0].pos;

    _can_click.push_back(sf::Vector2i(pawnPos1.x, pawnPos1.y - 1));
    _can_click.push_back(sf::Vector2i(pawnPos1.x, pawnPos1.y + 1));
    _can_click.push_back(sf::Vector2i(pawnPos1.x - 1, pawnPos1.y));
    _can_click.push_back(sf::Vector2i(pawnPos1.x + 1, pawnPos1.y));

    // Not middle row
    if (pawnPos1.x != 4)
    {
      int changeAxis = (pawnPos1.x < 4) ? 1 : -1;

      _can_click.push_back(sf::Vector2i(pawnPos1.x - 1, pawnPos1.y - changeAxis));
      _can_click.push_back(sf::Vector2i(pawnPos1.x + 1, pawnPos1.y + changeAxis));
    }
    // Middle row
    else
    {
      _can_click.push_back(sf::Vector2i(pawnPos1.x - 1, pawnPos1.y - 1));
      _can_click.push_back(sf::Vector2i(pawnPos1.x + 1, pawnPos1.y - 1));
    }
    break;
  }
    // 2 clicked
  case 2:
  {
    sf::Vector2i pawnPos1 = _clickedPawns[0].pos;
    sf::Vector2i pawnPos2 = _clickedPawns[1].pos;
    int maxX = max(pawnPos1.x, pawnPos2.x);
    int minX = min(pawnPos1.x, pawnPos2.x);
    int maxY = max(pawnPos1.y, pawnPos2.y);
    int minY = min(pawnPos1.y, pawnPos2.y);

    // If in same row
    if (minX == maxX)
    {
      _can_click.push_back(sf::Vector2i(maxX, minY - 1));
      _can_click.push_back(sf::Vector2i(maxX, maxY + 1));
    }
    // If in same column and different row
    else if (minY == maxY)
    {
      if (maxX == 4)
      {
        _can_click.push_back(sf::Vector2i(minX - 1, minY));
        _can_click.push_back(sf::Vector2i(maxX + 1, minY - 1));
      }
      else if (minX == 4)
      {
        _can_click.push_back(sf::Vector2i(minX - 1, minY - 1));
        _can_click.push_back(sf::Vector2i(maxX + 1, minY));
      }
      else
      {
        _can_click.push_back(sf::Vector2i(minX - 1, minY));
        _can_click.push_back(sf::Vector2i(maxX + 1, minY));
      }
    }
    // If in different row and column
    else
    {
      if (maxX <= 4)
      {
        _can_click.push_back(sf::Vector2i(minX - 1, minY - 1));
        if (maxX == 4)
          _can_click.push_back(sf::Vector2i(maxX + 1, maxY));
        else
          _can_click.push_back(sf::Vector2i(maxX + 1, maxY + 1));
      }
      else if (minX >= 4)
      {
        _can_click.push_back(sf::Vector2i(maxX + 1, minY - 1));
        if (minX == 4)
          _can_click.push_back(sf::Vector2i(minX - 1, maxY));
        else
          _can_click.push_back(sf::Vector2i(minX - 1, maxY + 1));
      }
    }
    break;
  }
    // 3 clicked
  case 3:
  {
    break;
  }
  }

  // Checking where can move
  calculateCanMove();
  // Checking where can hit
  calculateCanHit();

}



bool GamePlay::checkClickAvailable(PawnPos pawnPos, bool white)
{
  sf::Vector2i newPos = pawnPos.pos;
  sf::Vector2i lastPos = _clickedPawns[0].pos;

  // Checking if pawn of same color is clicked
  if (_board[lastPos.x][lastPos.y] < 0)
    white = false;

  if (_board[newPos.x][newPos.y] > 0 && !white)
  {
    return false;
  }
  else if (_board[newPos.x][newPos.y] < 0 && white)
    return false;

  // Check if can be clicked
  for (size_t i = 0; i < _can_click.size(); i++)
  {
    if (newPos == _can_click[i])
      return true;
  }

  return false;
}




void GamePlay::lightMove(sf::Vector2i mousePos)
{
  //==================================================================
  // Hightlight possible move
  //==================================================================
  int containsMove = -1;
  for (size_t i = 0; i < _can_move.size(); i++)
  {
    for (size_t j = 0; j < _can_move[i].size(); j++)
    {
      _can_move[i][j].moveCircle.setFillColor(sf::Color::Transparent);
      if (containsMove == -1)
        // If mouse under possible move
        if (_can_move[i][j].moveCircle.getGlobalBounds().contains(sf::Vector2f((float)mousePos.x, (float)mousePos.y)))
          containsMove = i;
    }
  }
  //  Hightlight indicated possible move
  if (containsMove > -1)
  {
    for (size_t i = 0; i < _can_move[containsMove].size(); i++)
    {
      _can_move[containsMove][i].moveCircle.setFillColor(sf::Color::Yellow);
    }
  }
  //==================================================================
  // Hightlight possible hit
  //==================================================================
  int containsHit = -1;
  for (size_t i = 0; i < _can_hit.size(); i++)
  {
    // Last element is place where to move enemy
    for (size_t j = 0; j < _can_hit[i].size() - 1; j++)
    {
      _can_hit[i][j].moveCircle.setFillColor(sf::Color::Transparent);
      if (containsHit == -1)
        // If mouse under possible hit
        if (_can_hit[i][j].moveCircle.getGlobalBounds().contains(sf::Vector2f((float)mousePos.x, (float)mousePos.y)))
          containsHit = i;
    }
  }
  // Hightlight indicated possible hit
  if (containsHit > -1)
  {
    for (size_t i = 0; i < _can_hit[containsHit].size(); i++)
    {
      _can_hit[containsHit][i].moveCircle.setFillColor(sf::Color::Blue);
    }
  }
}




std::vector<PawnPos> GamePlay::calculateMoveRightLeft(PawnPos pawnPos)
{
  bool pawnSortInvert = false; // Invert sort
  bool addPlus = false; // Adding where to move pawn from 0
  bool addMinus = false; // Adding where to move pawn from size()
  sf::CircleShape circleShape_tmp;
  std::vector<PawnPos> pawnMovePos;
  pawnMovePos.push_back(pawnPos);
  PawnPos firstClickedPos = _clickedPawns[0];

  // Moving left and right in same row
  if (pawnPos.pos.x == firstClickedPos.pos.x)
  {
    // LEFT
    if (pawnPos.pos.y < firstClickedPos.pos.y)
      addPlus = true;
    // RIGHT
    else
      addMinus = true;
  }
  // Moving up and down in same column
  else
  {
    // UP
    if (pawnPos.pos.x < firstClickedPos.pos.x)
      addPlus = true;
    // DOWN
    else
      addMinus = true;
  }

  if (addPlus)
  {
    for (size_t i = 0; i < _clickedPawns.size() - 1; i++)
    {
      sf::Vector2f pawnPlace_tmp = _pawnPlace[_clickedPawns[i].pos.x][_clickedPawns[i].pos.y].getPosition();
      circleShape_tmp.setPosition(pawnPlace_tmp.x - 2, pawnPlace_tmp.y - 2);
      PawnPos pawnPos_tmp;
      pawnPos_tmp.moveCircle = circleShape_tmp;
      pawnPos_tmp.pos = _clickedPawns[i].pos;
      pawnMovePos.push_back(pawnPos_tmp);
    }
  }
  else if (addMinus)
  {
    for (size_t i = _clickedPawns.size() - 1; i > 0; i--)
    {
      sf::Vector2f pawnPlace_tmp = _pawnPlace[_clickedPawns[i].pos.x][_clickedPawns[i].pos.y].getPosition();
      circleShape_tmp.setPosition(pawnPlace_tmp.x - 2, pawnPlace_tmp.y - 2);
      PawnPos pawnPos_tmp;
      pawnPos_tmp.moveCircle = circleShape_tmp;
      pawnPos_tmp.pos = _clickedPawns[i].pos;
      pawnMovePos.push_back(pawnPos_tmp);
    }
    pawnSortInvert = true;
  }

  // Sort clicked and move vector
  if (pawnSortInvert)
  {
    std::sort(_clickedPawns.begin(), _clickedPawns.end(), sortClickedByPositionInvert);
    std::sort(pawnMovePos.begin(), pawnMovePos.end(), sortClickedByPositionInvert);
  }
  else
  {
    std::sort(pawnMovePos.begin(), pawnMovePos.end(), sortClickedByPosition);
  }
  return pawnMovePos;
}



void GamePlay::makeHit(int pawnHitNumber)
{
  std::vector<PawnPos> pawnHitPos;
  for (size_t i = 0; i < _can_hit[pawnHitNumber].size(); i++)
  {
    pawnHitPos.push_back(_can_hit[pawnHitNumber][i]);
  }

  //DEBUG
  system("cls");
  debugWriteBoard();
  //DEBUG

  // Sort
  std::sort(_clickedPawns.begin(), _clickedPawns.end(), sortClickedByPosition);
  if (pawnHitPos[0].pos.x > _clickedPawns[0].pos.x)
    std::sort(_clickedPawns.begin(), _clickedPawns.end(), sortClickedByPositionInvert);
  else if (pawnHitPos[0].pos.x == _clickedPawns[0].pos.x && pawnHitPos[0].pos.y > _clickedPawns[0].pos.y)
         std::sort(_clickedPawns.begin(), _clickedPawns.end(), sortClickedByPositionInvert);
  //==================================================================
  // Moving enemy pawns
  //==================================================================
  for (int i = pawnHitPos.size() - 1; i > 0; i--)
  {
    // If moving out of board
    if (pawnHitPos[i].index == 666)
    {
      int index = _board[pawnHitPos[i - 1].pos.x][pawnHitPos[i - 1].pos.y];
      if (index < 0)
        index *= -1;
      index--;

      if (_whiteTurn)
      {
        _blackPawns[index].die();
        _board[pawnHitPos[i - 1].pos.x][pawnHitPos[i - 1].pos.y] = 0;
        _numberDeadBlack++;
      }
      else
      {
        _whitePawns[index].die();
        _board[pawnHitPos[i - 1].pos.x][pawnHitPos[i - 1].pos.y] = 0;
        _numberDeadWhite++;
      }
      
    }
    // Moving pawns
    else
    {
      int index = _board[pawnHitPos[i - 1].pos.x][pawnHitPos[i - 1].pos.y];

      _board[pawnHitPos[i].pos.x][pawnHitPos[i].pos.y] = index;
      _board[pawnHitPos[i - 1].pos.x][pawnHitPos[i - 1].pos.y] = 0;
      sf::Vector2f posAbsolute_tmp(_pawnPlace[pawnHitPos[i].pos.x][pawnHitPos[i].pos.y].getPosition());
      posAbsolute_tmp.x -= 2;
      posAbsolute_tmp.y -= 2;
      if (index < 0)
        index *= -1;
      index--;

      if (_whiteTurn)
        _blackPawns[index].move(pawnHitPos[i].pos, posAbsolute_tmp);
      else
        _whitePawns[index].move(pawnHitPos[i].pos, posAbsolute_tmp);
    }
  }

  //==================================================================
  // Moving ally pawns
  //==================================================================
  for (size_t i = 0; i < _clickedPawns.size(); i++)
  {
    sf::Vector2i movePos;
    if (i == 0)
      movePos = pawnHitPos.front().pos;
    else
      movePos = _clickedPawns[i - 1].pos;

    int index = _board[_clickedPawns[i].pos.x][_clickedPawns[i].pos.y];
    _board[movePos.x][movePos.y] = index;
    _board[_clickedPawns[i].pos.x][_clickedPawns[i].pos.y] = 0;
    sf::Vector2f posAbsolute_tmp(_pawnPlace[movePos.x][movePos.y].getPosition());
    posAbsolute_tmp.x -= 2;
    posAbsolute_tmp.y -= 2;

    if (index < 0)
      index *= -1;
    index--;
    if (_whiteTurn)
      _whitePawns[index].move(movePos, posAbsolute_tmp);
    else
      _blackPawns[index].move(movePos, posAbsolute_tmp);
  }

  //DEBUG
  debugWriteBoard();
  //DEBUG

  // Change turn
  changeTurn();

  _clickedPawns.clear();
  calculateCanClick();
}



void GamePlay::makeMove(int pawnMoveNumber)
{
  std::vector<PawnPos> pawnMovePos;
  for (size_t i = 0; i < _can_move[pawnMoveNumber].size(); i++)
  {
    pawnMovePos.push_back(_can_move[pawnMoveNumber][i]);
  }

  //DEBUG
  system("cls");
  debugWriteBoard();
  //DEBUG

  // Sort
  std::sort(_clickedPawns.begin(), _clickedPawns.end(), sortClickedByPosition);
  std::sort(pawnMovePos.begin(), pawnMovePos.end(), sortClickedByPosition);

  // Moving when 1 place is highlighted
  if (_clickedPawns.size() > 1 && pawnMovePos.size() == 1)
  {
    pawnMovePos = calculateMoveRightLeft(pawnMovePos[0]);
  }

  //==================================================================
  // Moving pawns
  //==================================================================
  for (size_t i = 0; i < _clickedPawns.size(); i++)
  {
    _board[_clickedPawns[i].pos.x][_clickedPawns[i].pos.y] = 0;
    sf::Vector2f posAbsolute_tmp(pawnMovePos[i].moveCircle.getPosition());
    if (_whiteTurn)
    {
      _board[pawnMovePos[i].pos.x][pawnMovePos[i].pos.y] = _clickedPawns[i].index + 1;
      _whitePawns[_clickedPawns[i].index].move(pawnMovePos[i].pos, posAbsolute_tmp);
    }
    else
    {
      _board[pawnMovePos[i].pos.x][pawnMovePos[i].pos.y] = (_clickedPawns[i].index + 1) * -1;
      _blackPawns[_clickedPawns[i].index].move(pawnMovePos[i].pos, posAbsolute_tmp);
    }
   
  }

  //DEBUG
  debugWriteBoard();
  //DEBUG

  // Change turn
  changeTurn();

  _clickedPawns.clear();
  calculateCanClick();
}



void GamePlay::clickEvent(sf::Event event)
{
  PawnPos pawnPos;
  sf::Vector2f local_click;

  local_click.x = (float)event.mouseButton.x;
  local_click.y = (float)event.mouseButton.y;
  if (_whiteTurn)
  {
    //==================================================================
    // If white pawn clicked
    //==================================================================
    for (size_t i = 0; i < _whitePawns.size(); i++)
    {
      if (_whitePawns[i].getPawn().getGlobalBounds().contains(local_click))
      {
        pawnPos.index = i + 1;
        pawnPos.pos = _whitePawns[i].getPosition();
        pawnClicked(pawnPos);
        return;
      }
    }
  }
  else
  {
    //==================================================================
    // If white pawn clicked
    //==================================================================
    for (size_t i = 0; i < _blackPawns.size(); i++)
    {
      if (_blackPawns[i].getPawn().getGlobalBounds().contains(local_click))
      {
        pawnPos.index = (i + 1) * -1;
        pawnPos.pos = _blackPawns[i].getPosition();
        pawnClicked(pawnPos);
        return;
      }
    }
  }
  
  //==================================================================
  // If move clicked
  //==================================================================
  for (size_t i = 0; i < _can_move.size(); i++)
  {
    for (size_t j = 0; j < _can_move[i].size(); j++)
    {
      if (_can_move[i][j].moveCircle.getGlobalBounds().contains(local_click))
      {
        makeMove(i);
        return;
      }
    }
  }
  //==================================================================
  // If hit clicked
  //==================================================================
  for (size_t i = 0; i < _can_hit.size(); i++)
  {
    for (size_t j = 0; j < _can_hit[i].size(); j++)
    {
      if (_can_hit[i][j].moveCircle.getGlobalBounds().contains(local_click))
      {
        makeHit(i);
        return;
      }
    }
  }
}




int GamePlay::max(int a, int b)
{
  return (a<b) ? b : a;
}

int GamePlay::min(int a, int b)
{
  return (a>b) ? b : a;
}

int GamePlay::mid(int a, int b, int c)
{
  if (b <= a && a <= c || c <= a && a <= b)
    return a;
  else if (a <= b && b <= c || c <= b && b <= a)
    return b;
  else //if (a <= c && c <= b || b <= c && c <= a)
    return c;
}



void GamePlay::calculateCanMove()
{
  // Clear move vector
  _can_move.clear();

  std::vector<std::vector<sf::Vector2i>> moveCircle_vec;
  sf::CircleShape moveCircle_tmp;
  moveCircle_tmp.setFillColor(sf::Color::Transparent);
  moveCircle_tmp.setRadius(16);

  switch (_clickedPawns.size())
  {
  case 0:
    return;
  //==================================================================
  // Hightlight when 1 pawn is clicked
  //==================================================================
  case 1:
  {
    sf::Vector2i pawnPos1 = _clickedPawns[0].pos;
    std::vector<sf::Vector2i> pawnPosOnBoard;

    pawnPosOnBoard.clear();
    pawnPosOnBoard.push_back(sf::Vector2i(pawnPos1.x, pawnPos1.y - 1));
    moveCircle_vec.push_back(pawnPosOnBoard);

    pawnPosOnBoard.clear();
    pawnPosOnBoard.push_back(sf::Vector2i(pawnPos1.x, pawnPos1.y + 1));
    moveCircle_vec.push_back(pawnPosOnBoard);

    pawnPosOnBoard.clear();
    pawnPosOnBoard.push_back(sf::Vector2i(pawnPos1.x - 1, pawnPos1.y));
    moveCircle_vec.push_back(pawnPosOnBoard);

    pawnPosOnBoard.clear();
    pawnPosOnBoard.push_back(sf::Vector2i(pawnPos1.x + 1, pawnPos1.y));
    moveCircle_vec.push_back(pawnPosOnBoard);

    if (pawnPos1.x != 4)
    {
      int changeAxis = (pawnPos1.x < 4) ? 1 : -1;

      pawnPosOnBoard.clear();
      pawnPosOnBoard.push_back(sf::Vector2i(pawnPos1.x - 1, pawnPos1.y - changeAxis));
      moveCircle_vec.push_back(pawnPosOnBoard);

      pawnPosOnBoard.clear();
      pawnPosOnBoard.push_back(sf::Vector2i(pawnPos1.x + 1, pawnPos1.y + changeAxis));
      moveCircle_vec.push_back(pawnPosOnBoard);
    }
    else
    {
      pawnPosOnBoard.clear();
      pawnPosOnBoard.push_back(sf::Vector2i(pawnPos1.x - 1, pawnPos1.y - 1));
      moveCircle_vec.push_back(pawnPosOnBoard);

      pawnPosOnBoard.clear();
      pawnPosOnBoard.push_back(sf::Vector2i(pawnPos1.x + 1, pawnPos1.y - 1));
      moveCircle_vec.push_back(pawnPosOnBoard);
    }

    break;
  }
  //==================================================================
  // Highlight when 2 pawns are clicked
  //==================================================================
  case 2:
  {
    std::vector<sf::Vector2i> pawnPosOnBoard;

    sf::Vector2i pawnPos1 = _clickedPawns[0].pos;
    sf::Vector2i pawnPos2 = _clickedPawns[1].pos;
    int maxX = max(pawnPos1.x, pawnPos2.x);
    int minX = min(pawnPos1.x, pawnPos2.x);
    int maxY = max(pawnPos1.y, pawnPos2.y);
    int minY = min(pawnPos1.y, pawnPos2.y);

    // If in same row
    if (minX == maxX)
    {
      pawnPosOnBoard.clear();
      pawnPosOnBoard.push_back(sf::Vector2i(minX, minY - 1));
      moveCircle_vec.push_back(pawnPosOnBoard);

      pawnPosOnBoard.clear();
      pawnPosOnBoard.push_back(sf::Vector2i(maxX, maxY + 1));
      moveCircle_vec.push_back(pawnPosOnBoard);

      pawnPosOnBoard.clear();
      pawnPosOnBoard.push_back(sf::Vector2i(minX + 1, minY));
      pawnPosOnBoard.push_back(sf::Vector2i(maxX + 1, maxY));
      moveCircle_vec.push_back(pawnPosOnBoard);

      pawnPosOnBoard.clear();
      pawnPosOnBoard.push_back(sf::Vector2i(minX - 1, minY));
      pawnPosOnBoard.push_back(sf::Vector2i(maxX - 1, maxY));
      moveCircle_vec.push_back(pawnPosOnBoard);

      if (maxX < 4 || minX > 4)
      {
        int changeAxis = (maxX < 4) ? 1 : -1;
        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX - changeAxis, maxY - 1));
        pawnPosOnBoard.push_back(sf::Vector2i(minX - changeAxis, minY - 1));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX + changeAxis, maxY + 1));
        pawnPosOnBoard.push_back(sf::Vector2i(minX + changeAxis, minY + 1));
        moveCircle_vec.push_back(pawnPosOnBoard);
      }
      else if (maxX == 4)
      {
        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX + 1, maxY - 1));
        pawnPosOnBoard.push_back(sf::Vector2i(minX + 1, minY - 1));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX - 1, maxY - 1));
        pawnPosOnBoard.push_back(sf::Vector2i(minX - 1, minY - 1));
        moveCircle_vec.push_back(pawnPosOnBoard);
      }
    }
    // If in same column
    else if (minY == maxY)
    {
      pawnPosOnBoard.clear();
      pawnPosOnBoard.push_back(sf::Vector2i(maxX, maxY + 1));
      pawnPosOnBoard.push_back(sf::Vector2i(minX, minY + 1));
      moveCircle_vec.push_back(pawnPosOnBoard);

      pawnPosOnBoard.clear();
      pawnPosOnBoard.push_back(sf::Vector2i(maxX, maxY - 1));
      pawnPosOnBoard.push_back(sf::Vector2i(minX, minY - 1));
      moveCircle_vec.push_back(pawnPosOnBoard);

      if (maxX < 4 || minX > 4)
      {
        int changeAxis = (maxX < 4) ? 1 : -1;

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(minX - 1, minY));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX + 1, maxY));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX, minY + changeAxis));
        pawnPosOnBoard.push_back(sf::Vector2i(maxX + 1, minY + changeAxis));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(minX, minY - changeAxis));
        pawnPosOnBoard.push_back(sf::Vector2i(minX - 1, minY - changeAxis));
        moveCircle_vec.push_back(pawnPosOnBoard);
      }
      else if (maxX == 4 || minX == 4)
      {
        int maxAxis = (maxX == 4) ? 1 : 0;
        int minAxis = 0;

        if (minX == 4)
        {
          int swap = maxY;
          maxY = minY;
          minY = swap;
          minAxis = 1;
        }

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(minX - 1, minY - minAxis));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX + 1, maxY - maxAxis));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(minX + 1, minY + maxAxis - minAxis));
        pawnPosOnBoard.push_back(sf::Vector2i(maxX + 1, maxY - minAxis));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX - 1, maxY - maxAxis + minAxis));
        pawnPosOnBoard.push_back(sf::Vector2i(minX - 1, minY - maxAxis));
        moveCircle_vec.push_back(pawnPosOnBoard);
      }
    }
    // If in different columns
    else
    {
      if (maxX < 4 || minX > 4)
      {
        int changeAxis = 1;

        if (minX > 4)
        {
          int swap = maxY;
          maxY = minY;
          minY = swap;
          changeAxis = -1;
        }

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(minX - 1, minY - changeAxis));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX + 1, maxY + changeAxis));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(minX, minY - 1));
        pawnPosOnBoard.push_back(sf::Vector2i(maxX, maxY - 1));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(minX, minY + 1));
        pawnPosOnBoard.push_back(sf::Vector2i(maxX, maxY + 1));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX + 1, maxY));
        pawnPosOnBoard.push_back(sf::Vector2i(minX + 1, minY));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX - 1, maxY));
        pawnPosOnBoard.push_back(sf::Vector2i(minX - 1, minY));
        moveCircle_vec.push_back(pawnPosOnBoard);
      }
      else if (maxX == 4 || minX == 4)
      {
        int maxAxis = (maxX == 4) ? 1 : 0;
        int minAxis = 0;

        if (minX == 4)
        {
          int swap = maxY;
          maxY = minY;
          minY = swap;
          minAxis = 1;
        }

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(minX - 1, minY - maxAxis));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX + 1, maxY - minAxis));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(minX, minY - 1));
        pawnPosOnBoard.push_back(sf::Vector2i(maxX, maxY - 1));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(minX, minY + 1));
        pawnPosOnBoard.push_back(sf::Vector2i(maxX, maxY + 1));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX + 1, maxY - maxAxis));
        pawnPosOnBoard.push_back(sf::Vector2i(minX + 1, minY));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX - 1, maxY));
        pawnPosOnBoard.push_back(sf::Vector2i(minX - 1, minY - minAxis));
        moveCircle_vec.push_back(pawnPosOnBoard);
      }
    }

    break;
  }
  //==================================================================
  // Highlight when 3 pawns are clicked
  //==================================================================
  case 3:
  {
    std::vector<sf::Vector2i> pawnPosOnBoard;

    sf::Vector2i pawnPos1 = _clickedPawns[0].pos;
    sf::Vector2i pawnPos2 = _clickedPawns[1].pos;
    sf::Vector2i pawnPos3 = _clickedPawns[2].pos;
    int maxX = max(max(pawnPos1.x, pawnPos2.x), pawnPos3.x);
    int minX = min(min(pawnPos1.x, pawnPos2.x), pawnPos3.x);
    int maxY = max(max(pawnPos1.y, pawnPos2.y), pawnPos3.y);
    int minY = min(min(pawnPos1.y, pawnPos2.y), pawnPos3.y);
    int midX = mid(pawnPos1.x, pawnPos2.x, pawnPos3.x);
    int midY = mid(pawnPos1.y, pawnPos2.y, pawnPos3.y);

    // If in same row
    if (maxX == minX)
    {
      pawnPosOnBoard.clear();
      pawnPosOnBoard.push_back(sf::Vector2i(minX, minY - 1));
      moveCircle_vec.push_back(pawnPosOnBoard);

      pawnPosOnBoard.clear();
      pawnPosOnBoard.push_back(sf::Vector2i(maxX, maxY + 1));
      moveCircle_vec.push_back(pawnPosOnBoard);

      pawnPosOnBoard.clear();
      pawnPosOnBoard.push_back(sf::Vector2i(maxX + 1, minY));
      pawnPosOnBoard.push_back(sf::Vector2i(midX + 1, midY));
      pawnPosOnBoard.push_back(sf::Vector2i(minX + 1, maxY));
      moveCircle_vec.push_back(pawnPosOnBoard);

      pawnPosOnBoard.clear();
      pawnPosOnBoard.push_back(sf::Vector2i(maxX - 1, minY));
      pawnPosOnBoard.push_back(sf::Vector2i(midX - 1, midY));
      pawnPosOnBoard.push_back(sf::Vector2i(minX - 1, maxY));
      moveCircle_vec.push_back(pawnPosOnBoard);

      if (maxX < 4 || minX > 4)
      {
        int changeAxis = (maxX < 4) ? 1 : -1;
        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX - changeAxis, maxY - 1));
        pawnPosOnBoard.push_back(sf::Vector2i(midX - changeAxis, midY - 1));
        pawnPosOnBoard.push_back(sf::Vector2i(minX - changeAxis, minY - 1));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX + changeAxis, maxY + 1));
        pawnPosOnBoard.push_back(sf::Vector2i(midX + changeAxis, midY + 1));
        pawnPosOnBoard.push_back(sf::Vector2i(minX + changeAxis, minY + 1));
        moveCircle_vec.push_back(pawnPosOnBoard);
      }
      else if (maxX == 4)
      {
        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX + 1, maxY - 1));
        pawnPosOnBoard.push_back(sf::Vector2i(midX + 1, midY - 1));
        pawnPosOnBoard.push_back(sf::Vector2i(minX + 1, minY - 1));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX - 1, maxY - 1));
        pawnPosOnBoard.push_back(sf::Vector2i(midX - 1, midY - 1));
        pawnPosOnBoard.push_back(sf::Vector2i(minX - 1, minY - 1));
        moveCircle_vec.push_back(pawnPosOnBoard);
      }
    }
    // If in same column
    else if (minY == maxY)
    {
      pawnPosOnBoard.clear();
      pawnPosOnBoard.push_back(sf::Vector2i(maxX, maxY + 1));
      pawnPosOnBoard.push_back(sf::Vector2i(midX, midY + 1));
      pawnPosOnBoard.push_back(sf::Vector2i(minX, minY + 1));
      moveCircle_vec.push_back(pawnPosOnBoard);

      pawnPosOnBoard.clear();
      pawnPosOnBoard.push_back(sf::Vector2i(maxX, maxY - 1));
      pawnPosOnBoard.push_back(sf::Vector2i(midX, midY - 1));
      pawnPosOnBoard.push_back(sf::Vector2i(minX, minY - 1));
      moveCircle_vec.push_back(pawnPosOnBoard);

      if (maxX < 4 || minX > 4)
      {
        int changeAxis = (maxX < 4) ? 1 : -1;

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(minX - 1, minY));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX + 1, maxY));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX + 1, maxY + changeAxis));
        pawnPosOnBoard.push_back(sf::Vector2i(midX + 1, midY + changeAxis));
        pawnPosOnBoard.push_back(sf::Vector2i(minX + 1, minY + changeAxis));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX - 1, maxY - changeAxis));
        pawnPosOnBoard.push_back(sf::Vector2i(midX - 1, midY - changeAxis));
        pawnPosOnBoard.push_back(sf::Vector2i(minX - 1, minY - changeAxis));
        moveCircle_vec.push_back(pawnPosOnBoard);
      }
      else if (maxX == 4 || minX == 4)
      {
        int maxAxis = (maxX == 4) ? 1 : 0;
        int minAxis = 0;

        if (minX == 4)
        {
          int swap = maxY;
          maxY = minY;
          minY = swap;
          minAxis = 1;
        }

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(minX - 1, minY - minAxis));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX + 1, maxY - maxAxis));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX + 1, maxY - minAxis));
        pawnPosOnBoard.push_back(sf::Vector2i(midX + 1, midY + maxAxis - minAxis));
        pawnPosOnBoard.push_back(sf::Vector2i(minX + 1, minY + maxAxis - minAxis));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX - 1, maxY - maxAxis + minAxis));
        pawnPosOnBoard.push_back(sf::Vector2i(midX - 1, midY - maxAxis + minAxis));
        pawnPosOnBoard.push_back(sf::Vector2i(minX - 1, minY - maxAxis));
        moveCircle_vec.push_back(pawnPosOnBoard);
      }
    }
    // If in different columns
    else
    {
      // Middle clicked is in middle row
      if (midX == 4)
      {
        bool changeY = true;
        int minAxis = 0;

        // true -> x < midX & y == midY
        // false -> x > midX & y == midY
        for (size_t i = 0; i < _clickedPawns.size(); i++)
        {
          sf::Vector2i pos_tmp = _clickedPawns[i].pos;
          if (pos_tmp.x > midX && pos_tmp.y == midY)
          {
            int swap = maxY;
            maxY = minY;
            minY = swap;
            minAxis = 1;
            changeY = false;
            break;
          }
        }
        int maxAxis = (changeY) ? 1 : 0;

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(minX - 1, maxY - minAxis));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX + 1, minY - maxAxis));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX + 1, minY - minAxis));
        pawnPosOnBoard.push_back(sf::Vector2i(midX + 1, midY - minAxis));
        pawnPosOnBoard.push_back(sf::Vector2i(minX + 1, maxY + maxAxis));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX - 1, minY + minAxis));
        pawnPosOnBoard.push_back(sf::Vector2i(midX - 1, midY - maxAxis));
        pawnPosOnBoard.push_back(sf::Vector2i(minX - 1, maxY - maxAxis));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX, minY + 1));
        pawnPosOnBoard.push_back(sf::Vector2i(midX, midY + 1));
        pawnPosOnBoard.push_back(sf::Vector2i(minX, maxY + 1));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX, minY - 1));
        pawnPosOnBoard.push_back(sf::Vector2i(midX, midY - 1));
        pawnPosOnBoard.push_back(sf::Vector2i(minX, maxY - 1));
        moveCircle_vec.push_back(pawnPosOnBoard);
      }
      else if (maxX < 4 || minX > 4)
      {
        int changeAxis = 1;

        if (minX > 4)
        {
          int swap = maxY;
          maxY = minY;
          minY = swap;
          changeAxis = -1;
        }

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(minX - 1, minY - changeAxis));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX + 1, maxY + changeAxis));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX, maxY + 1));
        pawnPosOnBoard.push_back(sf::Vector2i(midX, midY + 1));
        pawnPosOnBoard.push_back(sf::Vector2i(minX, minY + 1));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX, maxY - 1));
        pawnPosOnBoard.push_back(sf::Vector2i(midX, midY - 1));
        pawnPosOnBoard.push_back(sf::Vector2i(minX, minY - 1));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX + 1, maxY));
        pawnPosOnBoard.push_back(sf::Vector2i(midX + 1, midY));
        pawnPosOnBoard.push_back(sf::Vector2i(minX + 1, minY));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX - 1, maxY));
        pawnPosOnBoard.push_back(sf::Vector2i(midX - 1, midY));
        pawnPosOnBoard.push_back(sf::Vector2i(minX - 1, minY));
        moveCircle_vec.push_back(pawnPosOnBoard);
      }
      else if (maxX == 4 || minX == 4)
      {
        int maxAxis = (maxX == 4) ? 1 : 0;
        int minAxis = 0;

        if (minX == 4)
        {
          int swap = maxY;
          maxY = minY;
          minY = swap;
          minAxis = 1;
        }

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(minX - 1, minY - maxAxis));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX + 1, maxY - minAxis));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX, maxY + 1));
        pawnPosOnBoard.push_back(sf::Vector2i(midX, midY + 1));
        pawnPosOnBoard.push_back(sf::Vector2i(minX, minY + 1));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX, maxY - 1));
        pawnPosOnBoard.push_back(sf::Vector2i(midX, midY - 1));
        pawnPosOnBoard.push_back(sf::Vector2i(minX, minY - 1));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX + 1, maxY - maxAxis));
        pawnPosOnBoard.push_back(sf::Vector2i(midX + 1, midY));
        pawnPosOnBoard.push_back(sf::Vector2i(minX + 1, minY));
        moveCircle_vec.push_back(pawnPosOnBoard);

        pawnPosOnBoard.clear();
        pawnPosOnBoard.push_back(sf::Vector2i(maxX - 1, maxY));
        pawnPosOnBoard.push_back(sf::Vector2i(midX - 1, midY));
        pawnPosOnBoard.push_back(sf::Vector2i(minX - 1, minY - minAxis));
        moveCircle_vec.push_back(pawnPosOnBoard);
      }
    }

    break;
  }
  default:
    break;
  }

  for (size_t i = 0; i < moveCircle_vec.size(); i++)
  {
    bool empty = true; // If field is empty
    bool inBoard = true; // If all fields are on board
    std::vector<PawnPos> tmp_vec;
    for (size_t j = 0; j < moveCircle_vec[i].size(); j++)
    {
      sf::Vector2i posOnBoard_tmp = moveCircle_vec[i][j];
      // If on the board
      if (posOnBoard_tmp.x >= 0 && posOnBoard_tmp.y >= 0 && posOnBoard_tmp.x < 9 && posOnBoard_tmp.y < (int)_board[posOnBoard_tmp.x].size())
      {
        // If field is empty
        if (_board[posOnBoard_tmp.x][posOnBoard_tmp.y] == 0)
        {
          PawnPos whereToMove_tmp;
          sf::Vector2f pawnPlace_tmp = _pawnPlace[posOnBoard_tmp.x][posOnBoard_tmp.y].getPosition();
          moveCircle_tmp.setPosition(pawnPlace_tmp.x - 2, pawnPlace_tmp.y - 2);

          whereToMove_tmp.pos = posOnBoard_tmp;
          whereToMove_tmp.moveCircle = moveCircle_tmp;
          tmp_vec.push_back(whereToMove_tmp);
        }
        else
          empty = false;
      }
      else
        inBoard = false;
    }

    if (empty && inBoard)
    {
      _can_move.push_back(tmp_vec);
    }
  }

}



void GamePlay::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
  target.draw(_boardRect);
  // Place for pawns
  for (size_t i = 0; i < _pawnPlace.size(); i++)
  {
    for (size_t j = 0; j < _pawnPlace[i].size(); j++)
    {
      target.draw(_pawnPlace[i][j]);
    }
  }
  // White pawns
  for (size_t i = 0; i < _whitePawns.size(); i++)
  {
    target.draw(_whitePawns[i].getPawn());
  }
  // Black pawns
  for (size_t i = 0; i < _blackPawns.size(); i++)
  {
    target.draw(_blackPawns[i].getPawn());
  }
  // Move possibilities
  for (size_t i = 0; i < _can_move.size(); i++)
  {
    for (size_t j = 0; j < _can_move[i].size(); j++)
    {
      target.draw(_can_move[i][j].moveCircle);
    }
  }
  // Hit possibilities
  for (size_t i = 0; i < _can_hit.size(); i++)
  {
    for (size_t j = 0; j < _can_hit[i].size() - 1; j++)
    {
      target.draw(_can_hit[i][j].moveCircle);
    }
  }
  // Textes
  target.draw(_currentTurn);
  target.draw(_deadWhite);
  target.draw(_deadBlack);
}



//bool sortClickedByPosition(const PawnPos &lhs, const PawnPos &rhs)
//{
//  if (lhs.pos.x == rhs.pos.x)
//    return lhs.pos.y < rhs.pos.y;
//  else
//    return lhs.pos.x < rhs.pos.x;
//}
//
//bool sortClickedByPositionInvert(const PawnPos &lhs, const PawnPos &rhs)
//{
//  if (lhs.pos.x == rhs.pos.x)
//    return lhs.pos.y > rhs.pos.y;
//  else
//    return lhs.pos.x > rhs.pos.x;
//}