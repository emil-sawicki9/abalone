#ifndef APPLICATION_H
#define APPLICATION_H

#include "UIResource.h"

#include "ResourceHolder.h"
#include "ResourceIdentifiers.h"
#include "StateIdentifiers.h"
#include "State.h"
#include "StateStack.h"

#include "MenuState.h"
#include "GameState.h"
#include "PauseState.h"
#include "WinState.h"
#include "ChooseColorState.h"
#include "ChooseAiOptionsState.h"
#include "HelpKeyState.h"

#include <SFML/System/Time.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Text.hpp>

#include <Windows.h>

namespace abalone
{
  class Abalone
  {
  public:
    Abalone();
    ~Abalone();

    void					run();

  private:
    void          initialize();

    void					processInput();
    void					update(sf::Time dt);
    void					render();

    void					updateStatistics(sf::Time dt);
    void					registerStates();

    static const sf::Time	              TimePerFrame;

    float                               _WIDTH_;
    float                               _HEIGHT_;
    float                               _ASPECT_RATIO_;
    std::shared_ptr<sf::RenderWindow>   _mainWindow;
    std::shared_ptr<sf::View>           _gameView;
    std::shared_ptr<TextureHolder>			_textures;
    std::shared_ptr<FontHolder>         _fonts;
    sf::Color                           _startingColor;

    state::StateStack                   _stateStack;

    sf::Text				                    _fpsText;
    sf::Time				                    _fps;
    std::size_t			                    _secPerFrame;

    std::unique_ptr<sf::Texture>        LoadTextureFromResource(const std::string& name);

    sf::Vector2f                        calculateAspectRatio(const sf::Vector2f in, const sf::Vector2f clip);
  };
}
#endif // APPLICATION_H
