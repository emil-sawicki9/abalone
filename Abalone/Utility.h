#ifndef UTILITY_H
#define UTILITY_H

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>
#include <sfml/Graphics/CircleShape.hpp>

#include <sstream>
#include <cmath>

namespace sf
{
  class Sprite;
  class Text;
}

namespace utility
{
  void centerOrigin(sf::Sprite &sprite);
  void centerOrigin(sf::CircleShape &shape);
  void centerOrigin(sf::Text &text);
}

#endif // UTILITY_H
