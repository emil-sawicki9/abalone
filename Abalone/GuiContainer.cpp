#include "GuiContainer.h"

#include "Button.h"

#include <iostream>

GUI::GuiContainer::GuiContainer()
  : _children()
  , _selectedChild(-1)
{
}

GUI::GuiContainer::GuiContainer(bool horizontalEvent)
  : _children()
  , _selectedChild(-1)
  , _horizontalEvents(horizontalEvent)
{
}

void GUI::GuiContainer::push(Component::Ptr component)
{
  _children.push_back(component);

  if (!hasSelection() && component->isSelectable())
    select(_children.size() - 1);
}

bool GUI::GuiContainer::isSelectable() const
{
  return false;
}

void GUI::GuiContainer::handleEvent(const sf::Event &event)
{
  // If we have selected a child then give it events
  if (hasSelection() && _children[_selectedChild]->isActive())
  {
    _children[_selectedChild]->handleEvent(event);
  }
  else if (event.type == sf::Event::KeyReleased)
  {
    if (_horizontalEvents)
    {
      if (event.key.code == sf::Keyboard::A || event.key.code == sf::Keyboard::Left)
      {
        selectPrevious();
      }
      else if (event.key.code == sf::Keyboard::D || event.key.code == sf::Keyboard::Right)
      {
        selectNext();
      }
    }
    else
    {
      if (event.key.code == sf::Keyboard::W || event.key.code == sf::Keyboard::Up)
      {
        selectPrevious();
      }
      else if (event.key.code == sf::Keyboard::S || event.key.code == sf::Keyboard::Down)
      {
        selectNext();
      }
    }
    if (event.key.code == sf::Keyboard::Return || event.key.code == sf::Keyboard::Space)
    {
      if (hasSelection())
        _children[_selectedChild]->activate();
    }
  }
  else if (event.type == sf::Event::MouseWheelMoved)
  {
    if (event.mouseWheel.delta > 0)
    {
      selectPrevious();
    }
    else if (event.mouseWheel.delta < 0)
    {
      selectNext();
    }
  }
  else if (event.type == sf::Event::MouseMoved)
  {
    for (size_t i = 0; i < _children.size(); i++)
    {
      Button::Ptr but = std::dynamic_pointer_cast<Button>(_children[i]);
      if (but)
      {
        if (but->check_contains(sf::Vector2f((float)event.mouseMove.x, (float)event.mouseMove.y)))
        {
          select(i);
          break;
        }
      }
    }
  }
  else if (event.type == sf::Event::MouseButtonPressed)
  {
    for (size_t i = 0; i < _children.size(); i++)
    {
      Button::Ptr but = std::dynamic_pointer_cast<Button>(_children[i]);
      if (but)
      {
        if (but->check_contains(sf::Vector2f((float)event.mouseButton.x, (float)event.mouseButton.y)))
        {
          but->activate();
          break;
        }
      }
    }
  }
}

void GUI::GuiContainer::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
  states.transform *= getTransform();

  for(const Component::Ptr &child : _children)
    target.draw(*child, states);
}

bool GUI::GuiContainer::hasSelection() const
{
  return _selectedChild >= 0;
}

void GUI::GuiContainer::select(std::size_t index)
{
  if (_children[index]->isSelectable())
  {
    if (hasSelection())
      _children[_selectedChild]->deselect();

    _children[index]->select();
    _selectedChild = index;
  }
}

void GUI::GuiContainer::selectNext()
{
  if (!hasSelection())
    return;

  // Search next component that is selectable, wrap around if necessary
  int next = _selectedChild;
  do
    next = (next + 1) % _children.size();
  while (!_children[next]->isSelectable());

  // Select that component
  select(next);
}

void GUI::GuiContainer::selectPrevious()
{
  if (!hasSelection())
    return;

  // Search previous component that is selectable, wrap around if necessary
  int prev = _selectedChild;
  do
    prev = (prev + _children.size() - 1) % _children.size();
  while (!_children[prev]->isSelectable());

  // Select that component
  select(prev);
}


