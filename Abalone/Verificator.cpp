#include "Verificator.h"

static bool compareClickedByPosition(const sf::Vector2i &lhs, const sf::Vector2i &rhs)
{
  if (lhs.x == rhs.x)
    return lhs.y < rhs.y;
  else
    return lhs.x < rhs.x;
}

void abalone::Verificator::sortClicked(pointVecShr &clicked)
{
  std::sort(clicked->begin(), clicked->end(), compareClickedByPosition);
}

sf::Vector2i abalone::Verificator::calculateNewPos(const sf::Vector2i pos, const int angle)
{
  sf::Vector2i newPos;
  switch (angle)
  {
  case MOVE_ANGLE_0:
    newPos.x = pos.x;
    newPos.y = pos.y + 1;
    break;
  case MOVE_ANGLE_60:
    newPos.x = pos.x - 1;
    newPos.y = pos.y;
    break;
  case MOVE_ANGLE_120:
    newPos.x = pos.x - 1;
    newPos.y = pos.y - 1;
    break;
  case MOVE_ANGLE_180:
    newPos.x = pos.x;
    newPos.y = pos.y - 1;
    break;
  case MOVE_ANGLE_240:
    newPos.x = pos.x + 1;
    newPos.y = pos.y;
    break;
  case MOVE_ANGLE_300:
    newPos.x = pos.x + 1;
    newPos.y = pos.y + 1;
    break;
  }

  return newPos;
}

//==================================================================
// RIGHT  &LEFT
//==================================================================

bool abalone::Verificator::verificate_R_L(pointVecShr &clicked, int angle, abalone::GameData::Ptr &data, pointVecShr &moveVec)
{
  bool resultVerification = false;
  switch ((MoveAngle)angle)
  {
  case MOVE_ANGLE_0:
    resultVerification = verificate_0_R_L(clicked, data, moveVec);
    break;
  case MOVE_ANGLE_60:
    resultVerification = verificate_60_R_L(clicked, data);
    break;
  case MOVE_ANGLE_120:
    resultVerification = verificate_120_R_L(clicked, data);
    break;
  case MOVE_ANGLE_180:
    resultVerification = verificate_180_R_L(clicked, data, moveVec);
    break;
  case MOVE_ANGLE_240:
    resultVerification = verificate_240_R_L(clicked, data);
    break;
  case MOVE_ANGLE_300:
    resultVerification = verificate_300_R_L(clicked, data);
    break;
  }

  if (moveVec->size() > 0)
    sortClicked(moveVec);

  return resultVerification;
}

bool abalone::Verificator::verificate_0_R_L(pointVecShr &clicked, abalone::GameData::Ptr &data, pointVecShr &moveVec)
{
  bool result = false;
  int size = clicked->size();
  int x = clicked->back().x;
  int y = clicked->back().y;
  int color = data->getColor(x, y);

  int color_1 = data->getColor(x, y + 1);
  int color_2 = data->getColor(x, y + 2);
  int color_3 = data->getColor(x, y + 3);

  if (color_1 == PAWN_EMPTY)
    result = true;
  else if ((size == 2 || size == 3) && 
           (color_2 == PAWN_EMPTY || color_2 == PAWN_OUTSIDE) && 
           ((color  &PAWN_BLACK && color_1  &PAWN_WHITE) ||
           (color  &PAWN_WHITE && color_1  &PAWN_BLACK)))  
    {
      moveVec->push_back(sf::Vector2i(x, y + 1));
      result = true;
    }
  else if ((size == 3) && 
          (color_3 == PAWN_EMPTY || color_3 == PAWN_OUTSIDE) && 
          ((color  &PAWN_BLACK && color_1  &PAWN_WHITE && color_2  &PAWN_WHITE) ||
          (color  &PAWN_WHITE && color_1  &PAWN_BLACK && color_2  &PAWN_BLACK)))
    {
      moveVec->push_back(sf::Vector2i(x, y + 1));
      moveVec->push_back(sf::Vector2i(x, y + 2));
      result = true;
    }

  return result;
}

bool abalone::Verificator::verificate_60_R_L(pointVecShr &clicked, abalone::GameData::Ptr &data)
{
  bool result = false;
  int size = clicked->size();
  int x = clicked->front().x;
  int y = clicked->front().y;
  int color = data->getColor(x, y);

  bool everyPawn = true;
  for (size_t i = 0; i < clicked->size() && everyPawn == true; i++)
  {
    if (data->getColor(x - 1, y + i) != PAWN_EMPTY)
      everyPawn = false;
  }
  result = everyPawn;

  return result;
}

bool abalone::Verificator::verificate_120_R_L(pointVecShr &clicked, abalone::GameData::Ptr &data)
{
  bool result = false;
  int size = clicked->size();
  int x = clicked->front().x;
  int y = clicked->front().y;
  int color = data->getColor(x, y);

  bool everyPawn = true;
  for (size_t i = 0; i < clicked->size() && everyPawn == true; i++)
  {
    if (data->getColor(x - 1, y - 1 + i) != PAWN_EMPTY)
      everyPawn = false;
  }
  result = everyPawn;

  return result;
}

bool abalone::Verificator::verificate_180_R_L(pointVecShr &clicked, abalone::GameData::Ptr &data, pointVecShr &moveVec)
{
  bool result = false;
  int size = clicked->size();
  int x = clicked->front().x;
  int y = clicked->front().y;
  int color = data->getColor(x, y);

  int color_1 = data->getColor(x, y - 1);
  int color_2 = data->getColor(x, y - 2);
  int color_3 = data->getColor(x, y - 3);

  if (color_1 == PAWN_EMPTY)
    result = true;
  else if ((size == 2 || size == 3) && 
          (color_2 == PAWN_EMPTY || color_2 == PAWN_OUTSIDE) && 
          ((color  &PAWN_BLACK && color_1  &PAWN_WHITE) ||
          (color  &PAWN_WHITE && color_1  &PAWN_BLACK)))
    {
      moveVec->push_back(sf::Vector2i(x, y - 1));
      result = true;
    }
  else if ((size == 3) && 
          (color_3 == PAWN_EMPTY || color_3 == PAWN_OUTSIDE) && 
          ((color  &PAWN_BLACK && color_1  &PAWN_WHITE && color_2  &PAWN_WHITE) ||
          (color  &PAWN_WHITE && color_1  &PAWN_BLACK && color_2  &PAWN_BLACK)))
    {
      moveVec->push_back(sf::Vector2i(x, y - 1));
      moveVec->push_back(sf::Vector2i(x, y - 2));
      result = true;
    }


  return result;
}

bool abalone::Verificator::verificate_240_R_L(pointVecShr &clicked, abalone::GameData::Ptr &data)
{
  bool result = false;
  int size = clicked->size();
  int x = clicked->front().x;
  int y = clicked->front().y;
  int color = data->getColor(x, y);

  bool everyPawn = true;
  for (size_t i = 0; i < clicked->size() && everyPawn == true; i++)
  {
    if (data->getColor(x + 1, y + i) != PAWN_EMPTY)
      everyPawn = false;
  }
  result = everyPawn;

  return result;
}

bool abalone::Verificator::verificate_300_R_L(pointVecShr &clicked, abalone::GameData::Ptr &data)
{
  bool result = false;
  int size = clicked->size();
  int x = clicked->front().x;
  int y = clicked->front().y;
  int color = data->getColor(x, y);

  bool everyPawn = true;
  for (size_t i = 0; i < clicked->size() && everyPawn == true; i++)
  {
    if (data->getColor(x + 1, y + 1 + i) != PAWN_EMPTY)
      everyPawn = false;
  }
  result = everyPawn;

  return result;
}

//==================================================================
// DOWN  &UP
//==================================================================

bool abalone::Verificator::verificate_D_U(pointVecShr &clicked, int angle, abalone::GameData::Ptr &data, pointVecShr &moveVec)
{
  bool resultVerification = false;
  switch ((MoveAngle)angle)
  {
  case MOVE_ANGLE_0:
    resultVerification = verificate_0_D_U(clicked, data);
    break;
  case MOVE_ANGLE_60:
    resultVerification = verificate_60_D_U(clicked, data, moveVec);
    break;
  case MOVE_ANGLE_120:
    resultVerification = verificate_120_D_U(clicked, data);
    break;
  case MOVE_ANGLE_180:
    resultVerification = verificate_180_D_U(clicked, data);
    break;
  case MOVE_ANGLE_240:
    resultVerification = verificate_240_D_U(clicked, data, moveVec);
    break;
  case MOVE_ANGLE_300:
    resultVerification = verificate_300_D_U(clicked, data);
    break;
  }

  if (moveVec->size() > 0)
    sortClicked(moveVec);

  return resultVerification;
}

bool abalone::Verificator::verificate_0_D_U(pointVecShr &clicked, abalone::GameData::Ptr &data)
{
  bool result = false;
  int size = clicked->size();
  int x = clicked->front().x;
  int y = clicked->front().y;
  int color = data->getColor(x, y);

  bool everyPawn = true;
  for (size_t i = 0; i < clicked->size() && everyPawn == true; i++)
  {
    if (data->getColor(x + i, y + 1 ) != PAWN_EMPTY)
      everyPawn = false;
  }
  result = everyPawn;

  return result;
}

bool abalone::Verificator::verificate_60_D_U(pointVecShr &clicked, abalone::GameData::Ptr &data, pointVecShr &moveVec)
{
  bool result = false;
  int size = clicked->size();
  int x = clicked->front().x;
  int y = clicked->front().y;
  int color = data->getColor(x, y);

  int color_1 = data->getColor(x - 1, y);
  int color_2 = data->getColor(x - 2, y);
  int color_3 = data->getColor(x - 3, y);

  if (color_1 == PAWN_EMPTY)
    result = true;
  else if ((size == 2 || size == 3) && 
          (color_2 == PAWN_EMPTY || color_2 == PAWN_OUTSIDE) && 
          ((color  &PAWN_BLACK && color_1  &PAWN_WHITE) ||
          (color  &PAWN_WHITE && color_1  &PAWN_BLACK)))
    {
      moveVec->push_back(sf::Vector2i(x - 1, y));
      result = true;
    }
  else if (moveVec != NULL && 
          (size == 3) && 
          (color_3 == PAWN_EMPTY || color_3 == PAWN_OUTSIDE) && 
          ((color  &PAWN_BLACK && color_1  &PAWN_WHITE && color_2  &PAWN_WHITE) ||
          (color  &PAWN_WHITE && color_1  &PAWN_BLACK && color_2  &PAWN_BLACK)))
    {
      moveVec->push_back(sf::Vector2i(x - 1, y));
      moveVec->push_back(sf::Vector2i(x - 2, y));
      result = true;
    }

  return result;
}

bool abalone::Verificator::verificate_120_D_U(pointVecShr &clicked, abalone::GameData::Ptr &data)
{
  bool result = false;
  int size = clicked->size();
  int x = clicked->front().x;
  int y = clicked->front().y;
  int color = data->getColor(x, y);

  bool everyPawn = true;
  for (size_t i = 0; i < clicked->size() && everyPawn == true; i++)
  {
    if (data->getColor(x + i - 1, y - 1) != PAWN_EMPTY)
      everyPawn = false;
  }
  result = everyPawn;

  return result;
}

bool abalone::Verificator::verificate_180_D_U(pointVecShr &clicked, abalone::GameData::Ptr &data)
{
  bool result = false;
  int size = clicked->size();
  int x = clicked->front().x;
  int y = clicked->front().y;
  int color = data->getColor(x, y);

  bool everyPawn = true;
  for (size_t i = 0; i < clicked->size() && everyPawn == true; i++)
  {
    if (data->getColor(x + i, y - 1) != PAWN_EMPTY)
      everyPawn = false;
  }
  result = everyPawn;

  return result;
}

bool abalone::Verificator::verificate_240_D_U(pointVecShr &clicked, abalone::GameData::Ptr &data, pointVecShr &moveVec)
{
  bool result = false;
  int size = clicked->size();
  int x = clicked->back().x;
  int y = clicked->back().y;
  int color = data->getColor(x, y);

  int color_1 = data->getColor(x + 1, y);
  int color_2 = data->getColor(x + 2, y);
  int color_3 = data->getColor(x + 3, y);

  if (color_1 == PAWN_EMPTY)
    result = true;
  else if ((size == 2 || size == 3) && 
          (color_2 == PAWN_EMPTY || color_2 == PAWN_OUTSIDE) && 
          ((color  &PAWN_BLACK && color_1  &PAWN_WHITE) ||
          (color  &PAWN_WHITE && color_1  &PAWN_BLACK)))
    {
      moveVec->push_back(sf::Vector2i(x + 1, y));
      result = true;
    }
  else if ((size == 3) && 
          (color_3 == PAWN_EMPTY || color_3 == PAWN_OUTSIDE) && 
          ((color  &PAWN_BLACK && color_1  &PAWN_WHITE && color_2  &PAWN_WHITE) ||
          (color  &PAWN_WHITE && color_1  &PAWN_BLACK && color_2  &PAWN_BLACK)))
    {
      moveVec->push_back(sf::Vector2i(x + 1, y));
      moveVec->push_back(sf::Vector2i(x + 2, y));
      result = true;
    }

  return result;
}

bool abalone::Verificator::verificate_300_D_U(pointVecShr &clicked, abalone::GameData::Ptr &data)
{
  bool result = false;
  int size = clicked->size();
  int x = clicked->front().x;
  int y = clicked->front().y;
  int color = data->getColor(x, y);

  bool everyPawn = true;
  for (size_t i = 0; i < clicked->size() && everyPawn == true; i++)
  {
    if (data->getColor(x + i + 1, y + 1) != PAWN_EMPTY)
      everyPawn = false;
  }
  result = everyPawn;

  return result;
}

//==================================================================
// DOWN  &RIGHT
//==================================================================

bool abalone::Verificator::verificate_D_R(pointVecShr &clicked, int angle, abalone::GameData::Ptr &data, pointVecShr &moveVec)
{
  bool resultVerification = false;
  switch ((MoveAngle)angle)
  {
  case MOVE_ANGLE_0:
    resultVerification = verificate_0_D_R(clicked, data);
    break;
  case MOVE_ANGLE_60:
    resultVerification = verificate_60_D_R(clicked, data);
    break;
  case MOVE_ANGLE_120:
    resultVerification = verificate_120_D_R(clicked, data, moveVec);
    break;
  case MOVE_ANGLE_180:
    resultVerification = verificate_180_D_R(clicked, data);
    break;
  case MOVE_ANGLE_240:
    resultVerification = verificate_240_D_R(clicked, data);
    break;
  case MOVE_ANGLE_300:
    resultVerification = verificate_300_D_R(clicked, data, moveVec);
    break;
  }

  if (moveVec->size() > 0)
    sortClicked(moveVec);

  return resultVerification;
}

bool abalone::Verificator::verificate_0_D_R(pointVecShr &clicked, abalone::GameData::Ptr &data)
{
  bool result = false;
  int size = clicked->size();
  int x = clicked->front().x;
  int y = clicked->front().y;
  int color = data->getColor(x, y);

  bool everyPawn = true;
  for (size_t i = 0; i < clicked->size() && everyPawn == true; i++)
  {
    if (data->getColor(x + i, y + i + 1) != PAWN_EMPTY)
      everyPawn = false;
  }
  result = everyPawn;

  return result;
}

bool abalone::Verificator::verificate_60_D_R(pointVecShr &clicked, abalone::GameData::Ptr &data)
{
  bool result = false;
  int size = clicked->size();
  int x = clicked->front().x;
  int y = clicked->front().y;
  int color = data->getColor(x, y);

  bool everyPawn = true;
  for (size_t i = 0; i < clicked->size() && everyPawn == true; i++)
  {
    if (data->getColor(x + i - 1, y + i) != PAWN_EMPTY)
      everyPawn = false;
  }
  result = everyPawn;

  return result;
}

bool abalone::Verificator::verificate_120_D_R(pointVecShr &clicked, abalone::GameData::Ptr &data, pointVecShr &moveVec)
{
  bool result = false;
  int size = clicked->size();
  int x = clicked->front().x;
  int y = clicked->front().y;
  int color = data->getColor(x, y);

  int color_1 = data->getColor(x - 1, y - 1);
  int color_2 = data->getColor(x - 2, y - 2);
  int color_3 = data->getColor(x - 3, y - 3);

  if (color_1 == PAWN_EMPTY)
    result = true;
  else if ((size == 2 || size == 3) && 
          (color_2 == PAWN_EMPTY || color_2 == PAWN_OUTSIDE) && 
          ((color  &PAWN_BLACK && color_1  &PAWN_WHITE) ||
          (color  &PAWN_WHITE && color_1  &PAWN_BLACK)))
    {
      moveVec->push_back(sf::Vector2i(x - 1, y - 1));
      result = true;
    }
  else if ((size == 3) && 
          (color_3 == PAWN_EMPTY || color_3 == PAWN_OUTSIDE) && 
          ((color  &PAWN_BLACK && color_1  &PAWN_WHITE && color_2  &PAWN_WHITE) ||
          (color  &PAWN_WHITE && color_1  &PAWN_BLACK && color_2  &PAWN_BLACK)))
    {
      moveVec->push_back(sf::Vector2i(x - 1, y - 1));
      moveVec->push_back(sf::Vector2i(x - 2, y - 2));
      result = true;
    }

  return result;
}

bool abalone::Verificator::verificate_180_D_R(pointVecShr &clicked, abalone::GameData::Ptr &data)
{
  bool result = false;
  int size = clicked->size();
  int x = clicked->front().x;
  int y = clicked->front().y;
  int color = data->getColor(x, y);

  bool everyPawn = true;
  for (size_t i = 0; i < clicked->size() && everyPawn == true; i++)
  {
    if (data->getColor(x + i, y + i - 1) != PAWN_EMPTY)
      everyPawn = false;
  }
  result = everyPawn;

  return result;
}

bool abalone::Verificator::verificate_240_D_R(pointVecShr &clicked, abalone::GameData::Ptr &data)
{
  bool result = false;
  int size = clicked->size();
  int x = clicked->front().x;
  int y = clicked->front().y;
  int color = data->getColor(x, y);

  bool everyPawn = true;
  for (size_t i = 0; i < clicked->size() && everyPawn == true; i++)
  {
    if (data->getColor(x + i + 1, y + i) != PAWN_EMPTY)
      everyPawn = false;
  }
  result = everyPawn;

  return result;
}

bool abalone::Verificator::verificate_300_D_R(pointVecShr &clicked, abalone::GameData::Ptr &data, pointVecShr &moveVec)
{
  bool result = false;
  int size = clicked->size();
  int x = clicked->back().x;
  int y = clicked->back().y;
  int color = data->getColor(x, y);

  int color_1 = data->getColor(x + 1, y + 1);
  int color_2 = data->getColor(x + 2, y + 2);
  int color_3 = data->getColor(x + 3, y + 3);

  if (color_1 == PAWN_EMPTY)
    result = true;
  else if ((size == 2 || size == 3) && 
          (color_2 == PAWN_EMPTY || color_2 == PAWN_OUTSIDE) && 
          ((color  &PAWN_BLACK && color_1  &PAWN_WHITE) ||
          (color  &PAWN_WHITE && color_1  &PAWN_BLACK)))
    {
      moveVec->push_back(sf::Vector2i(x + 1, y + 1));
      result = true;
    }
  else if ((size == 3) && 
          (color_3 == PAWN_EMPTY || color_3 == PAWN_OUTSIDE) && 
          ((color  &PAWN_BLACK && color_1  &PAWN_WHITE && color_2  &PAWN_WHITE) ||
          (color  &PAWN_WHITE && color_1  &PAWN_BLACK && color_2  &PAWN_BLACK)))
    {
      moveVec->push_back(sf::Vector2i(x + 1, y + 1));
      moveVec->push_back(sf::Vector2i(x + 2, y + 2));
      result = true;
    }

  return result;
}