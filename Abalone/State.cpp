#include "State.h"
#include "StateStack.h"

state::State::State(state::StateStack &stack, state::State::Context::Ptr &context)
  : _stack(&stack)
  , _context(context)
{
}

state::State::~State()
{
}

void state::State::requestStackPush(state::ID stateID)
{
  _stack->pushState(stateID);
}

void state::State::requestStackPop()
{
  _stack->popState();
}

void state::State::requestStateClear()
{
  _stack->clearStates();
}

state::State::Context::Ptr state::State::getContext() const
{
  return _context;
}
