#ifndef GAMEMENU_H
#define GAMEMENU_H

#include <SFML/Graphics.hpp>
//#include <SFML/Audio.hpp>

#include <iostream>
#include <vector>

class GameMenu : public sf::Drawable
{
public:
  //sf::Sound sound;

  GameMenu() :_is_active(true)
  { 
	initialize(); 
  }
  ~GameMenu() {};

  void initialize();

  bool isActive();
  void setActive(bool);

  int clickOption(sf::Event);

private:
  bool _is_active = true;

  //sf::SoundBuffer soundbuffer;
  //sf::Music music;

  sf::RectangleShape _bigWindow;
  std::vector<sf::RectangleShape> _button;
  sf::Text _logo, _start, _options, _exit;
  sf::Font _font;


  virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

};

#endif // GAMEMENU_H