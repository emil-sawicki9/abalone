#ifndef LABEL_H
#define LABEL_H

#include "Component.h"
#include "Utility.h"
#include "ResourceIdentifiers.h"
#include "ResourceHolder.h"

#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Text.hpp>

namespace GUI
{
  class Label : public Component
  {
  public:
    typedef std::shared_ptr<Label> Ptr;
    
  public:
    Label(const std::string &text, const FontHolder &fonts);

    virtual bool		isSelectable() const;
    void				    setText(const std::string &text);
    void            setSize(const unsigned int &size);
    void            setColor(const sf::Color &color);
    void            setStyle(const sf::Text::Style &style);
    void            center();
    void            setVisible(bool visible);
    sf::Text        getText() const;

    virtual void		handleEvent(const sf::Event &event);
    
  private:
    void				    draw(sf::RenderTarget &target, sf::RenderStates states) const;
    
  private:
    bool            _visible;
    sf::Text			  _text;
  };

}

#endif // LABEL_H
