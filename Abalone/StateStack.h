#ifndef STATESTACK_H
#define STATESTACK_H

#include "State.h"

#include <SFML/System/NonCopyable.hpp>

#include <vector>
#include <utility>
#include <functional>
#include <map>
#include <cassert>

namespace sf
{
  class Event;
  class RenderWindow;
}

namespace state
{
  class StateStack : private sf::NonCopyable
  {
  public:
    enum Action
    {
      Push,
      Pop,
      Clear,
    };

    explicit StateStack(State::Context::Ptr &context);

    template <typename T>
    void				registerState(state::ID stateID);

    void				update(sf::Time dt);
    void				draw();
    void				handleEvent(const sf::Event &event);

    void				pushState(state::ID stateID);
    void				popState();
    void				clearStates();

    bool				isEmpty() const;

  private:
    State::Ptr	createState(state::ID stateID);
    void				applyPendingChanges();

    struct PendingChange
    {
      explicit	PendingChange(Action action, state::ID stateID = state::None);

      Action		action;
      state::ID	stateID;
    };

    std::vector<State::Ptr>							_stack;
    std::vector<PendingChange>				  _pendingList;

    State::Context::Ptr									    _context;
    std::map<state::ID, std::function<State::Ptr()>>	_factories;
  };

  template <typename T>
  void StateStack::registerState(state::ID stateID)
  {
    _factories[stateID] = [this]()
    {
      return State::Ptr(new T(*this, _context));
    };
  }
}
#endif // STATESTACK_H
