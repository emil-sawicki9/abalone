#include "Node.h"

ai::Node::Node(const abalone::GameData::Ptr &data,const Node::Ptr parentNode)
  :_parent(parentNode),
  _data(std::make_unique<abalone::GameData>(*data)),
  _hitMove(false),
  _bestChild(0)
{
  setMoveData(std::make_shared<pointVector>(), MOVE_ANGLE_0, MOVE_DIR_R_L);
}

ai::Node::~Node()
{
}

bool ai::Node::isHitMove() const
{
  return _hitMove;
}

void ai::Node::clearChildren()
{
  _children.clear();
}

int ai::Node::getChildrenSize() const
{
  return _children.size();
}

ai::Node::Ptr ai::Node::getChild(int index) const
{
  return _children.at(index);
}

const ai::Node::Ptr ai::Node::getBestChild() const
{
  return _children.size() ? _children.at(_bestChild) : nullptr;
}

const abalone::GameData::Ptr ai::Node::getGameData() const
{
  return std::make_unique<abalone::GameData>(*_data);
}

void ai::Node::setMoveData(abalone::MoveData moveData)
{
  setMoveData(moveData.clickedPawns, moveData.angle, moveData.direction);
}

void ai::Node::setMoveData(std::shared_ptr<pointVector> &clickPawns, int angle, int dir)
{
  _moveData.clickedPawns = std::make_shared<pointVector>(*clickPawns);
  _moveData.angle = (MoveAngle)angle;
  _moveData.direction = (MoveDirection)dir;
}

abalone::MoveData ai::Node::getMoveData() const
{
  return _moveData;
}

abalone::MoveData ai::Node::getBestMove() const
{
  Node::Ptr tmp = getBestChild();
  if (tmp)
    return tmp->getMoveData();
  else
    return _moveData;
}

std::shared_ptr<pointVector> ai::Node::getClicked(int dir, int step, sf::Vector2i pawnPos)
{
  std::shared_ptr<pointVector> clicked = std::make_shared<pointVector>();
  sf::Vector2i nextPawn;

  int x = pawnPos.x;
  int y = pawnPos.y;
  clicked->push_back(pawnPos);
  switch (dir)
  {
  case MOVE_DIR_R_L :
    for (int i = 1; i <= step; i++)
    {
      nextPawn.x = x;
      nextPawn.y = y + i;
      clicked->push_back(nextPawn);
    }
    break;
  case MOVE_DIR_D_U :
    for (int i = 1; i <= step; i++)
    {
      nextPawn.x = x + i;
      nextPawn.y = y;
      clicked->push_back(nextPawn);
    }
    break;
  case MOVE_DIR_D_R :
    for (int i = 1; i <= step; i++)
    {
      nextPawn.x = x + i;
      nextPawn.y = y + i;
      clicked->push_back(nextPawn);
    }
    break;
  };
  return clicked;
}

bool ai::Node::checkClickedColor(std::shared_ptr<pointVector> &clicked, abalone::GameData::Ptr &data)
{
  bool resultStatus = true;
  sf::Vector2i pawnPos = clicked->front();
  int color = data->getColor(pawnPos.x, pawnPos.y);	

  for (size_t i = 1; i < clicked->size(); i++)
  {
    if (data->getColor(clicked->at(i)) != (color  &(~PAWN_CLICKED)))
      resultStatus = false;
  }
  return resultStatus;
}

bool ai::Node::checkMove(std::shared_ptr<pointVector> &clicked, int dir, int angle, abalone::GameData::Ptr &data)
{
  bool resultStatus = false;
  std::shared_ptr<pointVector> hitPos = std::make_shared<pointVector>();
  switch ((MoveDirection)dir)
  {
    case MOVE_DIR_R_L :
      resultStatus = abalone::Verificator::verificate_R_L(clicked, (MoveAngle)angle, data, hitPos);
      break;
    case MOVE_DIR_D_U :
      resultStatus = abalone::Verificator::verificate_D_U(clicked, (MoveAngle)angle, data, hitPos);
      break;
    case MOVE_DIR_D_R :
      resultStatus = abalone::Verificator::verificate_D_R(clicked, (MoveAngle)angle, data, hitPos);
      break;
  };

  if (hitPos->size() > 0)
  {
    _hitMove = true;
    clicked->insert(clicked->end(), hitPos->begin(), hitPos->end());
  }

  return resultStatus;
}

int ai::Node::calculateChildren(int color)
{
  _children.reserve(150);

  for (size_t i = 0; i < BOARD_SIZE; i++)
  {
    for (size_t j = 0; j < BOARD_SIZE; j++)
    {
      if (getGameData()->getColor(i, j) == color)
      {
        addChildFor(sf::Vector2i(i, j));
      }
    }
  }

  _children.shrink_to_fit();
  return _children.size();
}

void ai::Node::setBestChild(int i)
{
  _bestChild = i;
}

void ai::Node::addChildFor(const sf::Vector2i pawnPos)
{
  abalone::GameData::Ptr currentData = std::make_unique<abalone::GameData>(*_data);
  int direction, angle, step;

  std::shared_ptr<pointVector> clicked = std::make_shared<pointVector>();

  for (direction = MOVE_DIR_R_L; direction <= MOVE_DIR_D_R; direction++)
  {
    // if true -> 1 pawn is clicked
    step = (direction == MOVE_DIR_R_L) ? 0 : 1;
    for (; step < 3; step++)
    {
      for (angle = MOVE_ANGLE_0; angle <= MOVE_ANGLE_300; angle++)
      {      
        clicked = getClicked((MoveDirection)direction, step, pawnPos);

        if (checkClickedColor(clicked, currentData))
        {
          if (checkMove(clicked, (MoveDirection)direction, (MoveAngle)angle, currentData))
          {		
            Node::Ptr nextNode = std::make_shared<Node>(currentData, shared_from_this());
            if (nextNode != NULL)
            {
              abalone::Verificator::sortClicked(clicked);
              nextNode->_data->makeMove(clicked, (MoveAngle)angle, (MoveDirection)direction);
              nextNode->setMoveData(clicked, (MoveAngle)angle, (MoveDirection)direction);

              if (clicked->size() > 1)
                _children.insert(_children.begin(), nextNode);
              else
                _children.push_back(nextNode);
            }
            else
              std::cout << "out of memory - node creation" << std::endl;
          }
          else
            clicked->clear();
        }
      }
    }
  }
}