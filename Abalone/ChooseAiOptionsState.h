#ifndef CHOOSEAIOPTIONSTATE_H
#define CHOOSEAIOPTIONSTATE_H

#include "State.h"
#include "GuiContainer.h"
#include "Utility.h"
#include "ResourceHolder.h"
#include "Button.h"
#include "Label.h"
#include "Globals.h"

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Text.hpp>

namespace state
{
  class ChooseAiOptionsState : public State
  {
  public:
    ChooseAiOptionsState(StateStack &stack, Context::Ptr &context);
    ~ChooseAiOptionsState();

    virtual void		draw();
    virtual bool		update(sf::Time dt);
    virtual bool		handleEvent(const sf::Event &event);

  private:
    void            initialize(Context::Ptr &context);
    void            setAiLevel(int aiLevel);

    int                   _aiColor;
    sf::RectangleShape		_backgroundSprite;
    GUI::GuiContainer     _guiContainer;
  };
}
#endif //CHOOSEAIOPTIONSTATE_H