#ifndef AI_H
#define AI_H

#include "Node.h"

#include <SFML/System/Lock.hpp>
#include <SFML/System/Clock.hpp>

#include <mutex>
#include <thread>
#include <map>
#include <string>

namespace ai
{
  typedef std::vector<std::vector<unsigned long long>> zobristVectors;

  class AI
  {
  public:
    
    AI(int aiLevel, int aiType, bool mainAI);
    ~AI();
    // debug
    void                printTTSize();
    void                printAiTypeLevel();
    void                printMove(const abalone::MoveData &move);
    // Ai func
    abalone::MoveData   getMoveData();
    int                 getAiType();
    void                setAiType(int aiType);
    void                setAiLevel(int aiLevel);
    void                setZobristKeyTable(std::shared_ptr<zobristVectors> &zobristKey);
    void                initTT();
    // Thread funcs
    void                execute(const abalone::GameData::Ptr &data, int color);
    bool                isFinished();
    bool                isRunning();
    void                stopThread();
    sf::Int32           getElapsedTime();

  private:

    struct TableEntry
    {
      typedef std::shared_ptr<TableEntry> Ptr;
      unsigned long long hash;
      int value;
      int depth;
      int type;
      abalone::MoveData bestMove;
    };

    enum ValueType
    {
      VALUE_EXACT,
      VALUE_LOWERBOUND,
      VALUE_UPPERBOUND
    };

    void                calculateMove();
    int                 isTerminal(const abalone::GameData::Ptr &data);
    unsigned long long  calculateZobristHash(const abalone::GameData::Ptr &data, const int color);
    void                storeTTEntry(unsigned long long hash, int value, int valueType, int depth, abalone::MoveData moveData, TableEntry::Ptr &tableEntry);
    TableEntry::Ptr     getTTEntry(unsigned long long hash);
    int                 evaluate(const abalone::GameData::Ptr &data, const int playerColor, int aiLevel);
    int                 evaluateNormal(const abalone::GameData::Ptr &data, const int playerColor);
    int                 evaluateExtended(const abalone::GameData::Ptr &data, const int playerColor);
    int                 getPawnNumberCount(const abalone::GameData::Ptr &data, const int playerColor) const;
    std::vector<int>    calculateEvalFuncNormal(const abalone::GameData::Ptr &data, const int playerColor) const;
    std::vector<int>    calculateEvalFuncExtended(const abalone::GameData::Ptr &data, const int playerColor) const;
    std::vector<float>  getWeights(const int distance, const int cohesion) const;
    int                 miniMax(Node::Ptr &node, int depth, int color); 
    int                 alphaBeta(Node::Ptr &node, int depth, int alpha, int beta, int color); 
    int                 alphaBetaWithMemory(Node::Ptr &node, int depth, int alpha, int beta, int color); 
    int                 iterativeDeepeningMTD(Node::Ptr &node, int depth);
    int                 MTDF(Node::Ptr &node, int depth, int firstGuess);

    std::vector<std::vector<TableEntry::Ptr>> _transpositionTable;
    std::shared_ptr<zobristVectors>           _zobristKeyTable;
    abalone::GameData::Ptr                    _gameData;
    sf::Clock                                 _elapsedTime;
    sf::Int32                                 _calculationTime;
    AiType                                    _aiType;
    AiLevel                                   _aiLevel;
    int                                       _playerColor, _maxDepth;
    std::shared_ptr<pointVector>              _clickedPawns;
    abalone::MoveData                         _moveData;
    std::thread                               _thread;
    std::mutex                                _mutex;
    bool                                      _threadFinished, _threadRunning;
    bool                                      _mainAI;
    int node_count; // debug
    int evaluate_count; //debug
    int node_checked_count; //debug
  };
}
#endif // AI_H