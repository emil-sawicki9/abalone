#include "StateStack.h"

state::StateStack::StateStack(State::Context::Ptr &context) 
  : _stack()
  , _pendingList()
  , _context(context)
  , _factories()
{
}

state::StateStack::PendingChange::PendingChange(Action action, state::ID stateID)
  : action(action)
  , stateID(stateID)
{
}

void state::StateStack::update(sf::Time dt)
{
  // Iterate from top to bottom, stop as soon as update() returns false
  for (auto itr = _stack.rbegin(); itr != _stack.rend(); ++itr)
  {
    if (!(*itr)->update(dt))
      break;
  }

  applyPendingChanges();
}

void state::StateStack::draw()
{
  // Draw all active states from bottom to top
  for(State::Ptr &state : _stack)
    state->draw();
}

void state::StateStack::handleEvent(const sf::Event &event)
{    
  // Iterate from top to bottom, stop as soon as handleEvent() returns false
  for (auto itr = _stack.rbegin(); itr != _stack.rend(); ++itr)
  {
    if (!(*itr)->handleEvent(event))
      break;
  }

  applyPendingChanges();
}

void state::StateStack::pushState(state::ID stateID)
{
  _pendingList.push_back(PendingChange(Push, stateID));
}

void state::StateStack::popState()
{
  _pendingList.push_back(PendingChange(Pop));
}

void state::StateStack::clearStates()
{
  _pendingList.push_back(PendingChange(Clear));
}

bool state::StateStack::isEmpty() const
{
  return _stack.empty();
}

state::State::Ptr state::StateStack::createState(state::ID stateID)
{
  auto found = _factories.find(stateID);
  assert(found != _factories.end());

  return found->second();
}

void state::StateStack::applyPendingChanges()
{
  for(PendingChange change : _pendingList)
  {
    switch (change.action)
    {
    case Push:
      _stack.push_back(createState(change.stateID));
      break;

    case Pop:
      _stack.pop_back();
      break;

    case Clear:
      _stack.clear();
      break;
    }
  }

  _pendingList.clear();
}