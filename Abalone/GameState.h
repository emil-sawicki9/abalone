#ifndef GAMESTATE_H
#define GAMESTATE_H

#include "GameData.h"
#include "Pawn.h"
#include "State.h"
#include "AI.h"
#include "ResourceHolder.h"
#include "Utility.h"
#include "GuiContainer.h"
#include "Label.h"

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/System/Clock.hpp>

#include <random>

namespace abalone
{
  struct MoveShape
  {
    abalone::MoveData moveData;
    std::vector<sf::Sprite> shapeContainer;
  };
}

namespace state
{
  class GameState : public State
  {
  public:
    GameState(StateStack &stack, Context::Ptr &context);
    ~GameState();

    virtual void		draw();
    virtual bool		update(sf::Time dt);
    virtual bool		handleEvent(const sf::Event &event);

  private:
    abalone::GameData::Ptr                      _gameData;
    int                                         _currentTurn, _aiCalcUpdate;
    bool                                        _isDebug, _aiAuto;
    std::unique_ptr<ai::AI>                     _aiMain, _aiMain2;// , _ai_minimax, _ai_ab, _ai_abTt, _ai_mtdf;
    std::unique_ptr<sf::Texture>                _pawnTexture;
    GUI::GuiContainer                           _guiContainer;
    GUI::Label::Ptr                             _aiMainTime, _aiMain2Time;// , _aiMiniMaxTime, _aiAbTime, _aiAbTtTime, _aiMTDfTime;
    GUI::Label::Ptr                             _currentTurnText, _deadWhite, _deadBlack, _aiCalc;
    std::vector<abalone::Pawn>                  _pawnShapeContainer;
    std::vector <abalone::MoveShape>            _moveContainer;
    std::vector <abalone::MoveShape>            _hitContainer;
    std::shared_ptr<pointVector>                _clickedPawns; // Vector2i -> based on 9x9 board
    sf::CircleShape                             _boardBackground;
    sf::RectangleShape                          _boardRect, _deadPawnRectW, _deadPawnRectB;
    std::vector<std::vector<sf::Sprite>>        _pawnPlace;
    std::vector<std::vector<sf::Sprite>>        _deadPawnPlace;
    sf::Sprite                                  _aiCalculation;

    void                          initialize(Context::Ptr &context);

    sf::Vector2f                  getPawnPlacePosition(sf::Vector2i pos) const;
    void                          movePawns(abalone::MoveData &moveData);
    void                          makeMove(abalone::MoveData &moveData);
    void                          makeAiMove();
    void                          calculateMoves();
    int                           checkClickedDirection() const;
    void                          clearSelection();
    void                          switchTurn();
    std::vector<sf::Sprite>       createHoverPawns(const std::shared_ptr<pointVector> &clickedPos, const int angle, const int dir, const bool hit) const;
    void                          updateAiCalc();

    // Events Handlers
    void                          pawnSelectedEvent(const sf::Vector2i pawnPos, const int index);
    void                          mouseHoverEvent(const sf::Vector2f mousePos);

    // Verificators
    bool                          verificateSelect(const sf::Vector2i selectPos) const;
    bool                          verificateUnselect(const sf::Vector2i unselectPos) const;
  };
}
#endif //GAMESTATE_H