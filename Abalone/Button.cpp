#include "Button.h"

#include <iostream>

GUI::Button::Button(const FontHolder &fonts, const TextureHolder &textures)
  : _callback()
  , _normalTexture(textures.get(textures::ButtonNormal))
  , _selectedTexture(textures.get(textures::ButtonSelected))
  , _pressedTexture(textures.get(textures::ButtonPressed))
  , _sprite()
  , _text("", fonts.get(fonts::Main), 16)
  , _isToggle(false)
{
  _sprite.setTexture(_normalTexture);
  utility::centerOrigin(_sprite);

  sf::Vector2f spritePos = _sprite.getPosition();
  _text.setPosition(spritePos);
}

void GUI::Button::setCallback(Callback callback)
{
  _callback = std::move(callback);
}

void GUI::Button::setText(const std::string &text)
{
  _text.setString(text);
  utility::centerOrigin(_text);
}

void GUI::Button::setSize(const unsigned int size)
{
  _text.setCharacterSize(size);
  utility::centerOrigin(_text);
}

void GUI::Button::setToggle(bool flag)
{
  _isToggle = flag;
}

bool GUI::Button::isSelectable() const
{
  return true;
}

void GUI::Button::select()
{
  Component::select();

  _sprite.setTexture(_selectedTexture);
}

void GUI::Button::deselect()
{
  Component::deselect();

  _sprite.setTexture(_normalTexture);
}

void GUI::Button::activate()
{
  Component::activate();

  // If we are toggle then we should show that the button is pressed and thus "toggled".
  if (_isToggle)
    _sprite.setTexture(_pressedTexture);

  if (_callback)
    _callback();

  // If we are not a toggle then deactivate the button since we are just momentarily activated.
  if (!_isToggle)
    deactivate();
}

void GUI::Button::deactivate()
{
  Component::deactivate();

  if (_isToggle)
  {
    // Reset texture to right one depending on if we are selected or not.
    if (isSelected())
      _sprite.setTexture(_selectedTexture);
    else
      _sprite.setTexture(_normalTexture);
  }
}

bool GUI::Button::check_contains(sf::Vector2f mouse_pos)
{
  sf::Vector2f size(_sprite.getGlobalBounds().width, _sprite.getGlobalBounds().height);
  sf::FloatRect globBounds(this->getPosition() - size/2.0f, size);
  if (globBounds.contains(mouse_pos))
    return true;
  else
    return false;
}

void GUI::Button::handleEvent(const sf::Event &event)
{

}

void GUI::Button::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
  states.transform *= getTransform();
  target.draw(_sprite, states);
  target.draw(_text, states);
}


