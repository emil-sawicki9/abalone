#include "AI.h"
#include <fstream>

ai::AI::AI(int aiLevel, int aiType, bool mainAI)
  : _mainAI(mainAI)
  , _threadFinished(false)
  , _threadRunning(false)
  , _aiLevel((AiLevel)aiLevel)
  , _aiType((AiType)aiType)
  , _clickedPawns(std::make_shared<pointVector>())
  , _gameData(std::make_unique<abalone::GameData>())
{
  initTT();
}

ai::AI::~AI()
{
  if (isRunning())
    _thread.join();
}

void ai::AI::initTT()
{
  _transpositionTable.clear();
  if (_aiType == AiType::AI_TYPE_ALPHABETA_TT || _aiType == AiType::AI_TYPE_MTDF)
  {
    unsigned int size = 1000000;
    _transpositionTable.reserve(size);
    for (size_t i = 0; i < size; i++)
    {
      _transpositionTable.push_back(std::vector<TableEntry::Ptr>());
    }
    _transpositionTable.shrink_to_fit();
  }
}

int ai::AI::iterativeDeepeningMTD(Node::Ptr &node, int maxDepth) 
{
  //function iterative_deepening(root : node_type) : integer;
  //  firstguess: = 0;
  //  for d = 1 to MAX_SEARCH_DEPTH do
  //    firstguess : = MTDF(root, firstguess, d);
  //  return firstguess;
  // -----------------------------------------

  int firstGuess = 0;
  for (int depth = 1; depth <= maxDepth; depth++)
  {
    firstGuess = MTDF(node, depth, firstGuess);
  }
  return firstGuess;
}

int ai::AI::MTDF(Node::Ptr &node, int depth, int firstGuess)
{
  //function MTDF(root : node_type; f: integer; d: integer) : integer;
  //  g: = f;
  //  upperbound: = +INFINITY;
  //  lowerbound: = -INFINITY;
  //  repeat
  //    if g == lowerbound then beta : = g + 1 else beta : = g;
  //    g: = AlphaBetaWithMemory(root, beta � 1, beta, d);
  //    if g < beta then upperbound : = g else lowerbound : = g;
  //  until lowerbound >= upperbound;
  //  return g;
  // -----------------------------------------

  int result = firstGuess;
  int beta = 0;
  int upperbound = INT_MAX;
  int lowerbound = INT_MIN;
  while (lowerbound < upperbound)
  {
    beta = (result == lowerbound) ? result + 1 : result;
    result = alphaBetaWithMemory(node, depth, beta - 1, beta, _playerColor);
    if (result < beta)
      upperbound = result;
    else
      lowerbound = result;
  }
  return result;
}

int ai::AI::alphaBetaWithMemory(Node::Ptr &node, int depth, int alpha, int beta, int color)
{
  //function AlphaBetaWithMemory(n : node_type; alpha, beta, d : integer) : integer;
  //  if retrieve(n) == OK then /* Transposition table lookup */
  //    if n.lowerbound >= beta then return n.lowerbound;
  //    if n.upperbound <= alpha then return n.upperbound;
  //    alpha: = max(alpha, n.lowerbound);
  //    beta: = min(beta, n.upperbound);
  //  if d == 0 then g : = evaluate(n); /* leaf node */
  //  else if n == MAXNODE then
  //    g : = -INFINITY; a: = alpha; /* save original alpha value */
  //    c: = firstchild(n);
  //    while (g < beta) and (c != NOCHILD) do
  //      g : = max(g, AlphaBetaWithMemory(c, a, beta, d � 1));
  //      a: = max(a, g);
  //      c: = nextbrother(c);
  //  else /* n is a MINNODE */
  //    g : = +INFINITY; b: = beta; /* save original beta value */
  //    c: = firstchild(n);
  //    while (g > alpha) and (c != NOCHILD) do
  //      g : = min(g, AlphaBetaWithMemory(c, alpha, b, d � 1));
  //      b: = min(b, g);
  //      c: = nextbrother(c);
  //  /* Traditional transposition table storing of bounds */
  //  /* Fail low result implies an upper bound */
  //  if g <= alpha then n.upperbound : = g; store n.upperbound;
  //  /* Found an accurate minimax value � will not occur if called with zero window */
  //  if g > alpha and g < beta then n.lowerbound : = g; n.upperbound : = g; store n.lowerbound, n.upperbound;
  //  /* Fail high result implies a lower bound */
  //  if g >= beta then n.lowerbound : = g; store n.lowerbound;
  //  return g;
  // -----------------------------------------

  node_checked_count++;
  unsigned long long currentHash = calculateZobristHash(node->getGameData(), color);
  bool createNewEntry = true;
  TableEntry::Ptr tableEntry = getTTEntry(currentHash);
  if (tableEntry != nullptr && tableEntry->depth >= depth)
  {
    createNewEntry = false;
    if (tableEntry->type == VALUE_EXACT)
    {
      if (depth == _maxDepth)
      {
        std::cout << "EXACT _maxDepth\n";
        node->setMoveData(tableEntry->bestMove);
      }
      if (depth != _maxDepth)
        node->clearChildren();
      return tableEntry->value;
    }
    else if (tableEntry->type == VALUE_LOWERBOUND && tableEntry->value >= beta)
      alpha = tableEntry->value;
    else if (tableEntry->type == VALUE_UPPERBOUND && tableEntry->value <= alpha)
      beta = tableEntry->value;
    if (alpha >= beta)
    {
      if (depth == _maxDepth)
      {
        std::cout << "A >= B " << alpha << " >= " << beta << "\n";
        node->setMoveData(tableEntry->bestMove);
      }
      if (depth != _maxDepth)
        node->clearChildren();
      return tableEntry->value;
    }
  }

  int bestValue = 0;
  int terminalNode = isTerminal(node->getGameData());
  if (depth == 0 || terminalNode != 0)
  {
    if (terminalNode != 0)
    {
      createNewEntry = true;
      bestValue = terminalNode;
    }
    else
      bestValue = evaluate(node->getGameData(), _playerColor, _aiLevel);
    if (createNewEntry)
    {
      if (bestValue <= alpha)
        storeTTEntry(currentHash, bestValue, VALUE_UPPERBOUND, depth, node->getBestMove(), tableEntry);
      else if (bestValue >= beta)
        storeTTEntry(currentHash, bestValue, VALUE_LOWERBOUND, depth, node->getBestMove(), tableEntry);
      else
        storeTTEntry(currentHash, bestValue, VALUE_EXACT, depth, node->getBestMove(), tableEntry);
    }
    return bestValue;
  }

  if (node->getChildrenSize() == 0)
  {
    node_count += node->calculateChildren(color);
  }
  
  if (color == _playerColor)
  {
    int a = alpha;
    bestValue = INT_MIN;
    for (int i = 0; i < node->getChildrenSize(); i++)
    {
      int result = alphaBetaWithMemory(node->getChild(i), depth - 1, a, beta, (color % 2) + 1);
      if (result > bestValue)
        bestValue = result;
      if (bestValue > a)
      {
        a = bestValue;
        node->setBestChild(i);
      }
      if (bestValue >= beta)
        break;
    }
  }
  else //if (color == enemyColor)
  {
    int b = beta;
    bestValue = INT_MAX;
    for (int i = 0; i < node->getChildrenSize(); i++)
    {
      int result = alphaBetaWithMemory(node->getChild(i), depth - 1, alpha, b, (color % 2) + 1);
      if (result < bestValue)
        bestValue = result;
      if (bestValue < b)
      {
        b = bestValue;
        node->setBestChild(i);
      }
      if (bestValue <= alpha)
        break;
    }
  }

  if (createNewEntry)
  {
    if (bestValue <= alpha)
      storeTTEntry(currentHash, bestValue, VALUE_UPPERBOUND, depth, node->getBestMove(), tableEntry);
    else if (bestValue >= beta)
      storeTTEntry(currentHash, bestValue, VALUE_LOWERBOUND, depth, node->getBestMove(), tableEntry);
    else
      storeTTEntry(currentHash, bestValue, VALUE_EXACT, depth, node->getBestMove(), tableEntry);
  }

  if (depth != _maxDepth)
    node->clearChildren();
  return bestValue;
}

void ai::AI::printMove(const abalone::MoveData &move)
{
  using namespace std;
  cout << "ANGLE=" << move.angle << " DIR=" << move.direction << " MOVE=";
  for (size_t i = 0; i < move.clickedPawns->size(); i++)
  {
    sf::Vector2i pos = move.clickedPawns->at(i);
    cout << pos.x << pos.y << " ";
  }
  cout << "\n";
}

int ai::AI::isTerminal(const abalone::GameData::Ptr &data)
{
  // LOSS
  if (PAWN_MAX_NUMBER - data->getNumberOfPawns(_playerColor) >= 6)
    return INT_MIN;
  // WIN
  else if (PAWN_MAX_NUMBER - data->getNumberOfPawns((_playerColor % 2) + 1) >= 6)
    return INT_MAX;
  else
    return 0;
}

int ai::AI::alphaBeta(Node::Ptr &node, int depth, int alpha, int beta, int color) 
{
  //function alphabeta(node, depth, alpha, beta, maximizingPlayer)
  //      if depth = 0 or node is a terminal node
  //          return the heuristic value of node
  //      if maximizingPlayer
  //          v : = -?
  //          for each child of node
  //              v : = max(v, alphabeta(child, depth - 1, ?, ?, FALSE))
  //              ? : = max(?, v)
  //              if ? ? ?
  //                  break (*? cut - off *)
  //          return v
  //      else
  //          v : = ?
  //          for each child of node
  //              v : = min(v, alphabeta(child, depth - 1, ?, ?, TRUE))
  //              ? : = min(?, v)
  //              if ? ? ?
  //                  break (*? cut - off *)
  //          return v
  // -----------------------------------------

  node_checked_count++;
  int terminal = isTerminal(node->getGameData());
  if (terminal != 0)
    return terminal;
  else if (depth == 0)
    return evaluate(node->getGameData(), _playerColor, _aiLevel);

  node_count += node->calculateChildren(color);

  int bestValue = 0;
  if (color == _playerColor)
  {
    bestValue = INT_MIN;
    for (int i = 0; i < node->getChildrenSize(); i++)
    {
      int result = alphaBeta(node->getChild(i), depth - 1, alpha, beta, (color % 2) + 1);
      if (result > bestValue)
        bestValue = result;
      if (bestValue > alpha)
      {
        alpha = bestValue;
        node->setBestChild(i);
      }
      if (alpha >= beta)
        break;
    }
  }
  else //if (color == enemyColor)
  {
    bestValue = INT_MAX;
    for (int i = 0; i < node->getChildrenSize(); i++)
    {
      int result = alphaBeta(node->getChild(i), depth - 1, alpha, beta, (color % 2) + 1);
      if (result < bestValue)
        bestValue = result;
      if (bestValue < beta)
      {
        beta = bestValue;
        node->setBestChild(i);
      }
      if (alpha >= beta)
        break;
    }
  }
  if (depth != _maxDepth)
    node->clearChildren();
  return bestValue;
}

int ai::AI::miniMax(Node::Ptr &node, int depth, int color) 
{
  //function minimax(node, depth, maximizingPlayer)
  //  if depth = 0 or node is a terminal node
  //    return the heuristic value of node
  //    if maximizingPlayer
  //      bestValue : = -?
  //      for each child of node
  //        val : = minimax(child, depth - 1, FALSE)
  //        bestValue : = max(bestValue, val)
  //      return bestValue
  //    else
  //        bestValue : = +?
  //      for each child of node
  //        val : = minimax(child, depth - 1, TRUE)
  //        bestValue : = min(bestValue, val)
  //      return bestValue
  // -----------------------------------------

  node_checked_count++;
  int terminal = isTerminal(node->getGameData());
  if (terminal != 0)
    return terminal;
  else if (depth == 0)
  {
    return evaluate(node->getGameData(), _playerColor, _aiLevel);
  }

  node_count += node->calculateChildren(color);

  int bestValue = 0;
  if (color == _playerColor)
  {
    bestValue = INT_MIN;
    for (int i = 0; i < node->getChildrenSize() ; i++)
    {
      int result = miniMax(node->getChild(i), depth - 1, (color % 2) + 1);
      if (bestValue < result)
      {
        bestValue = result;
        node->setBestChild(i);
      }
    }
  }
  else // if(color == enemyColor)
  {
    bestValue = INT_MAX;
    for (int i = 0; i < node->getChildrenSize(); i++)
    {
      int result = miniMax(node->getChild(i), depth - 1, (color % 2) + 1);
      if (bestValue > result)
      {
        bestValue = result;
        node->setBestChild(i);
      }
    }
  }
  if (depth != _maxDepth)
    node->clearChildren();
  return bestValue;
}

int ai::AI::evaluate(const abalone::GameData::Ptr &data, const int playerColor, int aiLevel) 
{
  evaluate_count++;
  if (aiLevel != AI_LEVEL_EASY)
    return evaluateExtended(data, playerColor);
  else
    return evaluateNormal(data, playerColor);
}

int ai::AI::evaluateNormal(const abalone::GameData::Ptr &data, const int playerColor)
{
  int result = 0;
  std::vector<int> resultEval = calculateEvalFuncNormal(data, playerColor);

  for (size_t i = 0; i < resultEval.size(); i++)
  {
    result += resultEval.at(i);
  }

  return result;
}

int ai::AI::evaluateExtended(const abalone::GameData::Ptr &data, const int playerColor)
{
  int result = 0;
  std::vector<int> resultEval = calculateEvalFuncExtended(data, playerColor);
  std::vector<float> weights = getWeights(resultEval.at(0), resultEval.at(1));

  for (size_t i = 0; i < resultEval.size(); i++)
  {
    result += (int)((float)resultEval.at(i) * weights.at(i));
  }

  return result;
}

std::vector<int> ai::AI::calculateEvalFuncNormal(const abalone::GameData::Ptr &data, const int playerColor) const
{
  std::vector<int> resultEval(4);
  int playerCohesionCount = 0;
  int enemyCohesionCount = 0;
  int playerDistCount = 0;
  int enemyDistCount = 0;
  // axis x -> 1st row of table
  // axis y -> 1st column of table
  sf::Vector2i center = data->getCenter();
  center.y *= -1; // (4, -4)

  int enemyColor = (playerColor % 2) + 1;

  for (size_t i = 0; i < BOARD_SIZE; i++)
  {
    for (size_t j = 0; j < BOARD_SIZE; j++)
    {
      int color = data->getColor(i, j);
      bool playerCond = color == playerColor;
      bool enemyCond = color == enemyColor;
      if (playerCond || enemyCond)
      {
        // Dist eval
        int x = j;
        int y = i * -1;
        int distance = 0;
        int dx = abs(x - center.x);
        int dy = abs(y - center.y);
        if ((x >= 4 && y >= -4) || (x <= 4 && y <= -4))
          distance = dx + dy;
        else
          distance = [](const int &a, const int &b)->const int {return a > b ? a : b; }(dx, dy);
        if (playerCond)
          playerDistCount += distance;
        else if (enemyCond)
          enemyDistCount += distance;

        // angle = [0, 60, 120, 180, 240, 300]
        for (size_t angle = MOVE_ANGLE_0; angle <= MOVE_ANGLE_300; angle++)
        {
          sf::Vector2i checkPos1 = abalone::Verificator::calculateNewPos(sf::Vector2i(i, j), angle);
          int checkColor1 = data->getColor(checkPos1);

          // Cohesion eval
          if (playerCond && checkColor1 == playerColor)
            playerCohesionCount++;
          else if (enemyCond && checkColor1 == enemyColor)
            enemyCohesionCount++;
        }
      }
    }
  }

  resultEval.at(0) = enemyDistCount - playerDistCount;
  resultEval.at(1) = playerCohesionCount - enemyCohesionCount;
  int kills = getPawnNumberCount(data, (playerColor % 2) + 1);
  resultEval.at(2) = kills * 100;
  int losses = getPawnNumberCount(data, playerColor);
  resultEval.at(3) = -losses * 10;

  return resultEval;
}

std::vector<int> ai::AI::calculateEvalFuncExtended(const abalone::GameData::Ptr &data, const int playerColor) const
{
  std::vector<int> resultEval(6);
  int strongGroupCount = 0;
  int playerBreakGroupCount = 0;
  int enemyBreakGroupCount = 0;
  int playerCohesionCount = 0;
  int enemyCohesionCount = 0;
  int playerDistCount = 0;
  int enemyDistCount = 0;
  // axis x -> 1st row of table
  // axis y -> 1st column of table
  sf::Vector2i center = data->getCenter();
  center.y *= -1; // (4, -4)

  int enemyColor = (playerColor % 2) + 1;

  for (size_t i = 0; i < BOARD_SIZE; i++)
  {
    for (size_t j = 0; j < BOARD_SIZE; j++)
    {
      int color = data->getColor(i, j);
      bool playerCond = color == playerColor;
      bool enemyCond = color == enemyColor;
      if (playerCond || enemyCond)
      {
        // Dist eval
        int x = j;
        int y = i * -1;
        int distance = 0;
        int dx = abs(x - center.x);
        int dy = abs(y - center.y);
        if ((x >= 4 && y >= -4) || (x <= 4 && y <= -4))
          distance = dx + dy;
        else
          distance = [](const int &a, const int &b)->const int {return a > b ? a : b; }(dx, dy);
        if (playerCond)
          playerDistCount += distance;
        else if (enemyCond)
          enemyDistCount += distance;

        // angle = [0, 60, 120, 180, 240, 300]
        for (size_t angle = MOVE_ANGLE_0; angle <= MOVE_ANGLE_300; angle++)
        {
          sf::Vector2i checkPos1 = abalone::Verificator::calculateNewPos(sf::Vector2i(i, j), angle);
          int checkColor1 = data->getColor(checkPos1);

          // Cohesion eval
          if (playerCond && checkColor1 == playerColor)
            playerCohesionCount++;
          else if (enemyCond && checkColor1 == enemyColor)
            enemyCohesionCount++;

          // Strong group eval + Break group eval
          if (angle <= MOVE_ANGLE_120)
          {
            sf::Vector2i checkPos2 = abalone::Verificator::calculateNewPos(sf::Vector2i(i, j), angle + 3);
            int checkColor2 = data->getColor(checkPos2);
            bool checkGroupCond1 = checkColor1 == PAWN_WHITE || checkColor1 == PAWN_BLACK;
            bool checkGroupCond2 = checkColor2 == PAWN_WHITE || checkColor2 == PAWN_BLACK;
            bool breakGroupCond = checkColor1 == checkColor2;
            bool strongGroupCond = checkColor1 != checkColor2;
            if (checkGroupCond1 && checkGroupCond2)
            {
              // Strong group
              if (strongGroupCond && playerCond)
                strongGroupCount++;
              // Break group
              if (breakGroupCond)
              {
                if (playerCond && checkColor2 == enemyColor)
                  playerBreakGroupCount++;
                else if (enemyCond && checkColor2 == playerColor)
                  enemyBreakGroupCount++;
              }
            }
          }
        }
      }
    }
  }

  resultEval.at(0) = enemyDistCount - playerDistCount;
  resultEval.at(1) = playerCohesionCount - enemyCohesionCount;
  resultEval.at(2) = playerBreakGroupCount - enemyBreakGroupCount;
  resultEval.at(3) = strongGroupCount;
  int kills = getPawnNumberCount(data, (playerColor % 2) + 1);
  resultEval.at(4) = kills * 1000;
  int losses = getPawnNumberCount(data, playerColor);
  resultEval.at(5) = -losses * 10000;

  return resultEval;
}

int ai::AI::getPawnNumberCount(const abalone::GameData::Ptr &data, const int pawnColor) const
{
  return _gameData->getNumberOfPawns(pawnColor) - data->getNumberOfPawns(pawnColor);
}

std::vector<float> ai::AI::getWeights(const int distance, const int cohesion) const
{
  std::vector<float> resultWeights;

  if (distance < 0)
  {
    resultWeights.push_back(3);
    resultWeights.push_back(2);
    resultWeights.push_back(6);
    resultWeights.push_back(1.8f);
    resultWeights.push_back(1);
  }
  else if (distance < 5)
  {
    resultWeights.push_back(3.3f);
    resultWeights.push_back(2);
    resultWeights.push_back(6);
    resultWeights.push_back(1.8f);
    resultWeights.push_back(35);
  }
  else if (distance >= 5)
  {
    if (cohesion < 4)  // x < 4
    {
      resultWeights.push_back(2.9f);
      resultWeights.push_back(2);
      resultWeights.push_back(15);
      resultWeights.push_back(3);
      resultWeights.push_back(4);
    }
    else if (cohesion < 10) // 4 <= x < 10
    {
      resultWeights.push_back(2.9f);
      resultWeights.push_back(2);
      resultWeights.push_back(15);
      resultWeights.push_back(3);
      resultWeights.push_back(15);
    }
    else if (cohesion < 16) // 10 <= x < 16
    {
      resultWeights.push_back(2.8f);
      resultWeights.push_back(2.3f);
      resultWeights.push_back(25);
      resultWeights.push_back(3);
      resultWeights.push_back(15);
    }
    else if (cohesion < 22) // 16 <= x < 22
    {
      resultWeights.push_back(2.8f);
      resultWeights.push_back(2.1f);
      resultWeights.push_back(25);
      resultWeights.push_back(3);
      resultWeights.push_back(25);
    }
    else if (cohesion < 28) // 22 <= x < 28
    {
      resultWeights.push_back(2.7f);
      resultWeights.push_back(2.3f);
      resultWeights.push_back(25);
      resultWeights.push_back(3);
      resultWeights.push_back(30);
    }
    else if (cohesion < 34) // 28 <= x < 34
    {
      resultWeights.push_back(2.4f);
      resultWeights.push_back(2.3f);
      resultWeights.push_back(25);
      resultWeights.push_back(3);
      resultWeights.push_back(35);
    }
    else // x >= 34
    {
      resultWeights.push_back(2.2f);
      resultWeights.push_back(2.3f);
      resultWeights.push_back(25);
      resultWeights.push_back(3);
      resultWeights.push_back(40);
    }
  }

  try {
    resultWeights.push_back(50 * resultWeights.at(4));
  }
  catch (std::exception &e)
  {
    e.what();
    std::cout << "Exception (AI::getWeights), out of vector, vSize = " << resultWeights.size() << std::endl;
    resultWeights.clear();
    resultWeights = std::vector<float>(6);
  }

  return resultWeights;
}

unsigned long long ai::AI::calculateZobristHash(const abalone::GameData::Ptr &data, const int color)
{
  unsigned long long zobristKey = 0;
  int field = 0;
  for (size_t i = 0; i < BOARD_SIZE; i++)
  {
    for (size_t j = 0; j < BOARD_SIZE; j++)
    {
      int color = data->getColor(i, j);
      if (color == PAWN_WHITE || color == PAWN_BLACK)
      {
        zobristKey ^= _zobristKeyTable->at(color - 1).at(field);
        field++;
      }
      else if (color == PAWN_EMPTY)
        field++;
    }
  }
  zobristKey ^= _zobristKeyTable->at(color - 1).at(61);
  return zobristKey;
}

void ai::AI::setZobristKeyTable(std::shared_ptr<zobristVectors> &zobristKeyTable)
{
  _zobristKeyTable = zobristKeyTable;
}

ai::AI::TableEntry::Ptr ai::AI::getTTEntry(unsigned long long hash)
{
  unsigned int index = hash % _transpositionTable.size();
  for (size_t i = 0; i < _transpositionTable.at(index).size(); i++)
  {
    if (_transpositionTable.at(index).at(i)->hash == hash)
      return _transpositionTable.at(index).at(i);
  }
  return nullptr;
}

void ai::AI::printTTSize()
{
  int max = 0;
  int total = 0;
  for (size_t i = 0; i < _transpositionTable.size(); i++)
  {
    int size = _transpositionTable.at(i).size();
    total += size;
    if (size > max)
      max = size;
  }
  std::cout << "TT stats: Total = " << total << " Max = " << max;
  printAiTypeLevel();
}

void ai::AI::printAiTypeLevel()
{
  if (_mainAI)
    std::cout << " MAIN_AI";
  if (_playerColor == PAWN_WHITE)
    std::cout << "-WHITE ";
  else
    std::cout << "-BLACK ";
  switch (_aiType)
  {
  case AI_TYPE_MINIMAX:
    std::cout << "ai:MM  ";
    break;
  case AI_TYPE_ALPHABETA:
    std::cout << "ai:AB  ";
    break;
  case AI_TYPE_ALPHABETA_TT:
    std::cout << "ai:AB_TT  ";
    break;
  case AI_TYPE_MTDF:
    std::cout << "ai:MTD-f  ";
    break;
  }

  switch (_aiLevel)
  {
  case AI_LEVEL_EASY:
    std::cout << "lvl:EASY\n";
    break;
  case AI_LEVEL_NORMAL:
    std::cout << "lvl:MEDIUM\n";
    break;
  case AI_LEVEL_HARD:
    std::cout << "lvl:HARD\n";
    break;
  default:
    std::cout << "lvl:NONE\n";
    break;
  }
}

void ai::AI::storeTTEntry(unsigned long long hash, int value, int valueType, int depth, abalone::MoveData moveData, AI::TableEntry::Ptr &tableEntry)
{
  if (tableEntry == nullptr)
  {
    TableEntry::Ptr newEntry = std::make_shared<TableEntry>();
    newEntry->hash = hash;
    newEntry->depth = depth;
    newEntry->value = value;
    newEntry->type = valueType;
    newEntry->bestMove = moveData;
    _transpositionTable.at(hash % _transpositionTable.size()).push_back(newEntry);
  }
  else
  {
    tableEntry->value = value;
    tableEntry->depth = depth;
    tableEntry->type = valueType;
    tableEntry->bestMove = moveData;
  }
}

bool ai::AI::isFinished()
{
  return _threadFinished;
}

void ai::AI::stopThread()
{
  _threadRunning = false;
  _threadFinished = true;
  _thread.join();
}

bool ai::AI::isRunning()
{
  return _threadRunning;
}

sf::Int32 ai::AI::getElapsedTime()
{
  if (_calculationTime)
    return _calculationTime;
  else
    return _elapsedTime.getElapsedTime().asMilliseconds();
}

abalone::MoveData ai::AI::getMoveData()
{
  return _moveData;
}

int ai::AI::getAiType()
{
  return _aiType;
}

void ai::AI::setAiLevel(int aiLevel)
{
  _aiLevel = (AiLevel)aiLevel;
}

void ai::AI::setAiType(int aiType)
{
  _aiType = (AiType)aiType;
  initTT();
}

void ai::AI::execute(const abalone::GameData::Ptr &data, int color)
{
  if (_threadRunning)
  {
    //std::cout << "\nTHREAD ALREADY RUNNING!\n";
    return;
  }

  _gameData = std::make_unique<abalone::GameData>(*data);
  _moveData = abalone::MoveData();
  _playerColor = color;
  node_count = 0;
  node_checked_count = 0;
  evaluate_count = 0;
  _threadFinished = false;
  _threadRunning = true;
  _calculationTime = 0;
  _thread = std::thread(&ai::AI::calculateMove, this);
}

void ai::AI::calculateMove()
{
  std::cout << "----------------\nCALCULATION START... ";
  printAiTypeLevel();

  Node::Ptr root = std::make_shared<Node>(_gameData, nullptr);
  
  int maxDepth = 2;
  switch (_aiLevel)
  {
  case AI_LEVEL_EASY:
    maxDepth = 2;
    break;
  case AI_LEVEL_NORMAL:
    maxDepth = 3;
    break;
  case AI_LEVEL_HARD:
    maxDepth = 4;
    break;
  }
  
  _maxDepth = maxDepth;
  //for (size_t i = 1; i <= 8; i++)
  //{
  //  root = std::make_shared<Node>(_gameData, nullptr);
  //  initTT();
  //  _maxDepth = i;
  //  maxDepth = i;
  //  node_checked_count = 0;
  //  node_count = 0;
  //  evaluate_count = 0;
    _elapsedTime.restart();
    switch (_aiType)
    {
    case AI_TYPE_MINIMAX:
      miniMax(root, maxDepth, _playerColor);
      break;
    case AI_TYPE_ALPHABETA:
      alphaBeta(root, maxDepth, INT_MIN, INT_MAX, _playerColor);
      break;
    case AI_TYPE_ALPHABETA_TT:
      alphaBetaWithMemory(root, maxDepth, INT_MIN, INT_MAX, _playerColor);
      break;
    case AI_TYPE_MTDF:
      iterativeDeepeningMTD(root, maxDepth);
      break;
    }
  //  std::cout << "calculation nr " << i << ":\n";
  //  std::cout << "time=" << std::to_string(_elapsedTime.getElapsedTime().asMilliseconds());
  //  std::cout << " nodes_check=" << std::to_string(node_checked_count);
  //  std::cout << " nodes_creat=" << std::to_string(node_count);
  //  std::cout << " nodes_eval=" << std::to_string(evaluate_count);
  //  std::cout << "\n";
  //}
  _calculationTime = _elapsedTime.getElapsedTime().asMilliseconds();

  _moveData = root->getBestMove();
  _threadFinished = true;

  std::cout << "nodes_count = " << node_count << std::endl;
  std::cout << "nodes_checked = " << node_checked_count << std::endl;
  std::cout << "eval_count = " << evaluate_count << std::endl;
  std::cout << "CALCULATING END...\n";
}