#include "HelpKeyState.h"

state::HelpKeyState::HelpKeyState(StateStack &stack, Context::Ptr &context)
  : State(stack, context)
  , _backgroundSprite()
{
  initialize(context);
}

state::HelpKeyState::~HelpKeyState()
{
}

void state::HelpKeyState::initialize(Context::Ptr &context)
{
  sf::Font &font = context->fonts->get(fonts::Main);

  _backgroundSprite.setFillColor(sf::Color(0, 0, 0, 200));

  int charSize = 30;
  float y_diff = (float)(charSize + 10);
  sf::Vector2f pos(50.0f, 50.0f);
  auto label_f1 = std::make_shared<GUI::Label>("F1 - key guide", *context->fonts);
  label_f1->setPosition(pos);
  label_f1->setSize(charSize);
  _guiContainer.push(label_f1);

  pos += sf::Vector2f(0, y_diff);
  auto label_f10 = std::make_shared<GUI::Label>("F10 - AI calculation time count", *context->fonts);
  label_f10->setPosition(pos);
  label_f10->setSize(charSize);
  _guiContainer.push(label_f10);

  if (context->gameType == GAME_AI)
  {
    pos += sf::Vector2f(0, y_diff);
    auto label_f5 = std::make_shared<GUI::Label>("F5 - AI auto turn switch", *context->fonts);
    label_f5->setPosition(pos);
    label_f5->setSize(charSize);
    _guiContainer.push(label_f5);

    pos += sf::Vector2f(0, y_diff);
    auto label_enter = std::make_shared<GUI::Label>("ENTER - switch AI's turn", *context->fonts);
    label_enter->setPosition(pos);
    label_enter->setSize(charSize);
    _guiContainer.push(label_enter);
  }
  
  pos += sf::Vector2f(0, y_diff);
  auto label_esc = std::make_shared<GUI::Label>("ESC - Pause menu", *context->fonts);
  label_esc->setPosition(pos);
  label_esc->setSize(charSize);
  _guiContainer.push(label_esc);

  pos += sf::Vector2f(0, y_diff);
  auto label_back = std::make_shared<GUI::Label>("BACKSPACE - in Pause menu, return to Main menu", *context->fonts);
  label_back->setPosition(pos);
  label_back->setSize(charSize);
  _guiContainer.push(label_back);

  auto label = std::make_shared<GUI::Label>("Click or press any key to return", *context->fonts);
  label->setPosition(sf::Vector2f((float)(context->window->getSize().x/2), pos.y + y_diff*5));
  label->setStyle(sf::Text::Underlined);
  label->setSize(charSize);
  label->center();
  _guiContainer.push(label);
}

void state::HelpKeyState::draw()
{
  sf::RenderWindow &window = *State::getContext()->window;
  window.setView(*getContext()->gameView);

  _backgroundSprite.setSize(window.getView().getSize());

  window.draw(_backgroundSprite);
  window.draw(_guiContainer);
}

bool state::HelpKeyState::update(sf::Time)
{
  return false;
}

bool state::HelpKeyState::handleEvent(const sf::Event &event)
{
  _guiContainer.handleEvent(event);

  switch (event.type)
  {
  case sf::Event::MouseButtonPressed:
  case sf::Event::KeyPressed:
    requestStackPop();
    break;
  default:
    break;
  }

  return false;
}