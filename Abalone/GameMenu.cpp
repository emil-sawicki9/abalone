#include "GameMenu.h"

void GameMenu::initialize()
{
  
  << "MENU init() " << std::endl;

  int windowX = 200;
  int windowY = 70;
  int windowWidth = 400;
  int windowHeight = 440;
  int buttonDistance = 1;

  //music.openFromFile("ambient.ogg");
  //music.setLoop(true);
  //music.play();
  //soundbuffer.loadFromFile("mouseclick.wav");
  //sound.setBuffer(soundbuffer);

  _bigWindow.setPosition((float)windowX, (float)windowY);
  _bigWindow.setSize(sf::Vector2f((float)windowWidth, (float)windowHeight));
  _bigWindow.setFillColor(sf::Color::Color(50, 40, 20, 255));
  _bigWindow.setOutlineColor(sf::Color::Color(90, 80, 60, 255));
  _bigWindow.setOutlineThickness(3.0f);


  _font.loadFromFile("arial.ttf");

  sf::RectangleShape rectShape_tmp;
  sf::FloatRect floatRect_tmp;

  _logo.setString("Abalone");
  _logo.setFont(_font);
  _logo.setCharacterSize(40);
  floatRect_tmp = _logo.getLocalBounds();
  _logo.setPosition(sf::Vector2f(windowWidth - floatRect_tmp.width / 2, windowY + 50 * buttonDistance - floatRect_tmp.height / 2));

  buttonDistance += 2;
  _start.setString("Start game!");
  _start.setFont(_font);
  _start.setCharacterSize(30);
  floatRect_tmp = _start.getLocalBounds();
  _start.setPosition(sf::Vector2f(windowWidth - floatRect_tmp.width / 2, windowY + 50 * buttonDistance - floatRect_tmp.height / 2));
  _button.push_back(rectShape_tmp);

  buttonDistance += 2;
  _options.setString("Options");
  _options.setFont(_font);
  _options.setCharacterSize(30);
  floatRect_tmp = _options.getLocalBounds();
  _options.setPosition(sf::Vector2f(windowWidth - floatRect_tmp.width / 2, windowY + 50 * buttonDistance - floatRect_tmp.height / 2));
  _button.push_back(rectShape_tmp);

  buttonDistance += 2;
  _exit.setString("Exit game!");
  _exit.setFont(_font);
  _exit.setCharacterSize(30);
  floatRect_tmp = _exit.getLocalBounds();
  _exit.setPosition(sf::Vector2f(windowWidth - floatRect_tmp.width / 2, windowY + 50 * buttonDistance - floatRect_tmp.height / 2));
  _button.push_back(rectShape_tmp);
  
  for (size_t i = 0; i < _button.size(); i++)
  {
	_button[i].setSize(sf::Vector2f(200.0f, 60.0f));
	_button[i].setFillColor(sf::Color::Color(90, 80, 60, 255));
	_button[i].setOutlineColor(sf::Color::Color(130, 120, 100, 255));
	_button[i].setOutlineThickness(2.0f);
	floatRect_tmp = _button[i].getLocalBounds();
	_button[i].setPosition( (float)(windowWidth - floatRect_tmp.width / 2), (float)(windowY + 50 + 100 * (i + 1) - floatRect_tmp.height*0.35) );
  }

}

bool GameMenu::isActive()
{
  return _is_active;
}
void GameMenu::setActive(bool status)
{
  _is_active = status;
  //if (status) music.play();
  //else music.stop();
}

int GameMenu::clickOption(sf::Event event)
{
  sf::Vector2f local_click;

  local_click.x = (float)event.mouseButton.x;
  local_click.y = (float)event.mouseButton.y;
  for (size_t i = 0; i < _button.size(); i++)
  {
	if (_button[i].getGlobalBounds().contains(local_click))
	{
	  //sound.play();
	  return i;
	}
  }
  return -1;

}

void GameMenu::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
  target.draw(_bigWindow);
  for (size_t i = 0; i < _button.size(); i++)
    target.draw(_button[i]);
  target.draw(_logo);
  target.draw(_start);
  target.draw(_options);
  target.draw(_exit);

}