#include "PauseState.h"

state::PauseState::PauseState(StateStack &stack, Context::Ptr &context)
  : State(stack, context)
  , _backgroundSprite()
  , _pausedText()
  , _instructionText()
{
  initlialize(context);
}

void state::PauseState::initlialize(Context::Ptr &context)
{
  sf::Font &font = context->fonts->get(fonts::Main);
  sf::Vector2f viewSize = context->window->getView().getSize();

  _pausedText.setFont(font);
  _pausedText.setString("Game Paused");
  _pausedText.setCharacterSize(70);
  utility::centerOrigin(_pausedText);
  _pausedText.setPosition(0.5f * viewSize.x, 0.4f * viewSize.y);

  _instructionText.setFont(font);
  _instructionText.setString("Press ESCAPE to return to the game");
  utility::centerOrigin(_instructionText);
  _instructionText.setPosition(0.5f * viewSize.x, 0.6f * viewSize.y);
  
  _instructionText2.setFont(font);
  _instructionText2.setString("Press BACKSPACE to return to the main menu");
  utility::centerOrigin(_instructionText2);
  _instructionText2.setPosition(0.5f * viewSize.x, 0.6f * viewSize.y + _instructionText.getCharacterSize());
}

void state::PauseState::draw()
{
  sf::RenderWindow &window = *getContext()->window;
  window.setView(*getContext()->gameView);

  sf::RectangleShape backgroundShape;
  backgroundShape.setFillColor(sf::Color(0, 0, 0, 150));
  backgroundShape.setSize(window.getView().getSize());

  window.draw(backgroundShape);
  window.draw(_pausedText);
  window.draw(_instructionText);
  window.draw(_instructionText2);
}

bool state::PauseState::update(sf::Time)
{
  return false;
}

bool state::PauseState::handleEvent(const sf::Event &event)
{
  if (event.type != sf::Event::KeyPressed)
    return false;

  if (event.key.code == sf::Keyboard::Escape)
  {
    // Escape pressed, remove itself to return to the game
    requestStackPop();
  }

  if (event.key.code == sf::Keyboard::BackSpace)
  {
    // Escape pressed, remove itself to return to the game
    requestStateClear();
    requestStackPush(state::Menu);
  }

  return false;
}