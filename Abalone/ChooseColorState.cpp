#include "ChooseColorState.h"

state::ChooseColorState::ChooseColorState(StateStack &stack, Context::Ptr &context)
  : State(stack, context)
  , _backgroundSprite()
  , _instructionText()
{
  initialize(context);
}

state::ChooseColorState::~ChooseColorState()
{
}

void state::ChooseColorState::initialize(Context::Ptr &context)
{
  sf::Font &font = context->fonts->get(fonts::Main);
  sf::Vector2f viewSize = context->window->getView().getSize();

  _instructionText.setFont(font);
  _instructionText.setString("Choose your starting color");
  utility::centerOrigin(_instructionText);
  _instructionText.setPosition(0.5f * viewSize.x, 0.4f * viewSize.y);

  _backgroundSprite.setFillColor(sf::Color(0, 0, 0, 150));

  sf::Vector2f buttonPos = viewSize / 2.f;
  auto whiteButton = std::make_shared<GUI::Button>(*context->fonts, *context->textures);
  whiteButton->setPosition(buttonPos);
  whiteButton->setText("White");
  whiteButton->setSize(30);
  whiteButton->setCallback([this]()
  {
    setStartingColor(PAWN_WHITE);
    requestStackPop();
    requestStackPush(state::ChooseAiOptions);
  });

  auto blackButton = std::make_shared<GUI::Button>(*context->fonts, *context->textures);
  blackButton->setPosition(buttonPos + sf::Vector2f(0.0f, 70.0f));
  blackButton->setText("Black");
  blackButton->setSize(30);
  blackButton->setCallback([this]()
  {
    setStartingColor(PAWN_BLACK);
    requestStackPop();
    requestStackPush(state::ChooseAiOptions);
  });

  _guiContainer.push(whiteButton);
  _guiContainer.push(blackButton);
}

void state::ChooseColorState::setStartingColor(int pawnColor)
{    
  State::getContext()->startingColor = pawnColor;
}

void state::ChooseColorState::draw()
{
  sf::RenderWindow &window = *State::getContext()->window;
  window.setView(*getContext()->gameView);

  _backgroundSprite.setSize(window.getView().getSize());

  window.draw(_backgroundSprite);
  window.draw(_guiContainer);
  window.draw(_instructionText);
}

bool state::ChooseColorState::update(sf::Time)
{
  return false;
}

bool state::ChooseColorState::handleEvent(const sf::Event &event)
{
  _guiContainer.handleEvent(event);

  return false;
}