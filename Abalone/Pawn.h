#ifndef PAWN_H
#define PAWN_H

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/Color.hpp>
#include <SFML/System/Vector2.hpp>

#include <memory>

namespace abalone
{
  class Pawn
  {
  public:
    Pawn(sf::Sprite sprite, sf::Vector2f position, sf::Vector2i position_on_board);
    ~Pawn();

    sf::Sprite        getPawn() const;
    void              setClicked(bool);
    bool              isClicked();
    bool              isDead();
    sf::Vector2i      getPosition() const; // Get position on board
    void              move(sf::Vector2i, sf::Vector2f); // moving pawn on board and in window
    void              die(sf::Vector2f deadPos); // Kill pawn

  private:
    bool              _on_board;
    bool              _is_clicked;
    bool              _isDead;
    sf::Vector2f      _absolutePos; // texture position in window
    sf::Vector2i      _positionOnBoard; // position on board
    sf::Sprite        _pawnSprite;
    sf::Color         _mainColor;

    void initialize();

  };
}
#endif // PAWN_H