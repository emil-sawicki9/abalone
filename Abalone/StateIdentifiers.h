#ifndef STATEIDENTIFIERS_H
#define STATEIDENTIFIERS_H


namespace state
{
  enum ID
  {
    None,
    Title,
    Menu,
    Game,
    Options,
    Pause,
    Win,
    ChoosePlayerColor,
    ChooseAiOptions,
    HelpKey
  };
}

#endif // STATEIDENTIFIERS_H
