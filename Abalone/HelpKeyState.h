#ifndef HELPKEYSTATE_H
#define HELPKEYSTATE_H

#include "State.h"
#include "GuiContainer.h"
#include "Utility.h"
#include "Label.h"
#include "Globals.h"

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Text.hpp>

namespace state
{
  class HelpKeyState : public State
  {
  public:
    HelpKeyState(StateStack &stack, Context::Ptr &context);
    ~HelpKeyState();

    virtual void		draw();
    virtual bool		update(sf::Time dt);
    virtual bool		handleEvent(const sf::Event &event);

  private:
    void            initialize(Context::Ptr &context);

    sf::RectangleShape		_backgroundSprite;
    GUI::GuiContainer        _guiContainer;
  };
}
#endif //HELPKEYSTATE_H