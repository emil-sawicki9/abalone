#include "MenuState.h"

state::MenuState::MenuState(StateStack &stack, Context::Ptr &context)
  : State(stack, context)
  , _guiContainer()
{
  initialize(context);
}

void state::MenuState::initialize(Context::Ptr &context)
{
  // clearing context
  context->aiOptions.blackAiLevel = AI_LEVEL_NONE;
  context->aiOptions.whiteAiLevel = AI_LEVEL_NONE;
  context->startingColor = -1;

  //sf::Texture &texture = context->textures->get(textures::TitleScreen);
  sf::Font &font = context->fonts->get(fonts::Main);
  
  //mBackgroundSprite.setTexture(texture);
  sf::Vector2f labelPos = context->window->getView().getSize();
  sf::Vector2f buttonPosition = context->window->getView().getSize() / 2.f;

  auto titleLabel = std::make_shared<GUI::Label>("Abalone", *context->fonts);
  titleLabel->setPosition(labelPos.x / 2.f, labelPos.y / 4.f);
  titleLabel->setSize(50);
  titleLabel->center();
  _guiContainer.push(titleLabel);

  float height = 0, diffHeight = 65;
  auto pvpButton = std::make_shared<GUI::Button>(*context->fonts, *context->textures);
  pvpButton->setPosition(buttonPosition);
  pvpButton->setText("Player versus Player");
  pvpButton->setSize(30);
  pvpButton->setCallback([this]()
  {
    setGameType(GAME_PVP);
    requestStackPop();
    requestStackPush(state::Game);
  });
  _guiContainer.push(pvpButton);

  height += diffHeight;
  auto pveButton = std::make_shared<GUI::Button>(*context->fonts, *context->textures);
  pveButton->setPosition(buttonPosition + sf::Vector2f(0, height));
  pveButton->setText("Player versus AI");
  pveButton->setSize(30);
  pveButton->setCallback([this]()
  {
    setGameType(GAME_PVE);
    requestStackPop();
    requestStackPush(state::ChoosePlayerColor);
  });
  _guiContainer.push(pveButton);

  height += diffHeight;
  auto aiButton = std::make_shared<GUI::Button>(*context->fonts, *context->textures);
  aiButton->setPosition(buttonPosition + sf::Vector2f(0, height));
  aiButton->setText("AI versus AI");
  aiButton->setSize(30);
  aiButton->setCallback([this]()
  {
    setGameType(GAME_AI);
    requestStackPop();
    requestStackPush(state::ChooseAiOptions);
  });
  _guiContainer.push(aiButton);
  
  height += diffHeight;
  auto exitButton = std::make_shared<GUI::Button>(*context->fonts, *context->textures);
  exitButton->setPosition(buttonPosition + sf::Vector2f(0, height));
  exitButton->setText("Exit");
  exitButton->setSize(30);
  exitButton->setCallback([this]()
  {
    requestStackPop();
  });
  _guiContainer.push(exitButton);
}

void state::MenuState::setGameType(int gameType)
{
  State::getContext()->gameType = (GameType)gameType;
}

void state::MenuState::draw()
{
  sf::RenderWindow &window = *getContext()->window;

  window.setView(*getContext()->gameView);

  window.draw(_backgroundSprite);
  window.draw(_guiContainer);    
}

bool state::MenuState::update(sf::Time)
{
  return true;
}

bool state::MenuState::handleEvent(const sf::Event &event)
{
  _guiContainer.handleEvent(event);

  //bool selected = false;
  //bool mousePressed = event.type == sf::Event::MouseButtonPressed;    
  //bool mouseMoved = event.type == sf::Event::MouseMoved;
  ////////////////////
  //// Mouse events //
  ////////////////////
  //if ( mouseMoved || mousePressed)
  //{
  //  sf::Vector2f local_click;
  //  sf::RenderWindow &window = *getContext().window;
  //  window.setView(window.getDefaultView());
  //  
  //  if (mousePressed)
  //    local_click = window.mapPixelToCoords(sf::Vector2i(event.mouseButton.x, event.mouseButton.y));
  //  else
  //    local_click = window.mapPixelToCoords(sf::Vector2i(event.mouseMove.x, event.mouseMove.y));

  //  for (size_t i = 0; i < _options.size(); i++)
  //  {
  //    if (_options.at(i).rectShape.getGlobalBounds().contains(local_click))
  //    {
  //      _optionIndex = i;
  //      updateOptionText();

  //      if (mousePressed)
  //        selected = true;

  //      break;
  //    }
  //  }
  //}


  return true;
}