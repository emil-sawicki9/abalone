#include "Label.h"

GUI::Label::Label(const std::string &text, const FontHolder &fonts)
  : _text(text, fonts.get(fonts::Main), 16)
  , _visible(true)
{
}

sf::Text GUI::Label::getText() const
{
  return _text;
}

bool GUI::Label::isSelectable() const
{
  return false;
}

void GUI::Label::handleEvent(const sf::Event&)
{
}

void GUI::Label::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
  if (_visible)
  {
    states.transform *= getTransform();
    target.draw(_text, states);
  }
}

void GUI::Label::setText(const std::string &text)
{
  _text.setString(text);
}

void GUI::Label::setSize(const unsigned int &size)
{
  _text.setCharacterSize(size);
}

void GUI::Label::center()
{
  utility::centerOrigin(_text);
}

void GUI::Label::setColor(const sf::Color &color)
{
  _text.setColor(color);
}

void GUI::Label::setStyle(const sf::Text::Style &style)
{
  _text.setStyle(style);
}

void GUI::Label::setVisible(bool visible)
{
  _visible = visible;
}