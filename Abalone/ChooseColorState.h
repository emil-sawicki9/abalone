#ifndef CHOOSECOLORSTATE_H
#define CHOOSECOLORSTATE_H

#include "State.h"
#include "GuiContainer.h"
#include "Utility.h"
#include "ResourceHolder.h"
#include "Button.h"
#include "Globals.h"

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Text.hpp>

namespace state
{
  class ChooseColorState : public State
  {
  public:
    ChooseColorState(StateStack &stack, Context::Ptr &context);
    ~ChooseColorState();

    virtual void		draw();
    virtual bool		update(sf::Time dt);
    virtual bool		handleEvent(const sf::Event &event);

  private:
    void            initialize(Context::Ptr &context);
    void            setStartingColor(int pawnColor);

    enum OptionNames
    {
      White,
      Black
    };

    sf::RectangleShape		_backgroundSprite;
    sf::Text			        _instructionText;
    GUI::GuiContainer        _guiContainer;
  };
}
#endif //CHOOSECOLORSTATE_H