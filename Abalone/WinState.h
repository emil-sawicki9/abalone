#ifndef WINSTATE_H
#define WINSTATE_H

#include "State.h"
#include "Utility.h"
#include "ResourceHolder.h"
#include "Globals.h"

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Text.hpp>

namespace state
{
  class WinState : public State
  {
  public:
    WinState(StateStack &stack, Context::Ptr &context);
    ~WinState();

    virtual void		      draw();
    virtual bool		      update(sf::Time dt);
    virtual bool		      handleEvent(const sf::Event &event);


  private:
    void                  initialize(Context::Ptr &context);

    sf::RectangleShape		_backgroundSprite;
    sf::Text			        _winText;
    sf::Text			        _instructionText;
  };
}
#endif // WINSTATE_H