#include "GameData.h"

abalone::GameData::GameData()
{
  initialize();
}

abalone::GameData::GameData(const GameData &otherData)
{
  for (size_t i = 0; i < BOARD_SIZE; i++)
  {
    for (size_t j = 0; j < BOARD_SIZE; j++)
    {
      _board[i][j] = otherData._board[i][j];
    }
  }

  _numberOfBlackPawns = 0;
  _numberOfWhitePawns = 0;
  countNumberOfPawns();
}

abalone::GameData::~GameData()
{
}

void abalone::GameData::writeBoard()
{
  std::cout << "\n";
  for (size_t i = 0; i < BOARD_SIZE; i++)
  {
    for (size_t j = 0; j < BOARD_SIZE; j++)
    {
      int color = getColor(i, j);
      std::cout << color << " ";
      if (color < 10)
        std::cout << " ";
    }
    std::cout << "\n";
  }
  std::cout << "\n";
}

int abalone::GameData::calculateFieldNumber(sf::Vector2i pos)
{
  int floor = 5;
  int field = 0;
  for (int i = 0; i < pos.x; i++)
  {
    if (i < 4)
      field += floor++;
    else
      field += floor--;
  }
  if (pos.x < 5)
    field += pos.y;
  else
    field += pos.y - (pos.x % 5 + 1);

  return field;
}

void abalone::GameData::initialize()
{
  size_t floor = 5;
  for (size_t i = 0; i < BOARD_SIZE; i++)
  {
    for (size_t j = 0; j < BOARD_SIZE; j++)
    {
      _board[i][j] = PAWN_OUTSIDE;
      if (i < 2)
      {
        if (j < floor)
          _board[i][j] = PAWN_WHITE;
      }
      else if (i == 2)
      {
        if (j < floor)
        {
          if (j < 2 || j>4)
            _board[i][j] = PAWN_EMPTY;
          else
            _board[i][j] = PAWN_WHITE;
        }
      }
      else if (i > 2 && i < 5)
      {
        if (j < floor)
          _board[i][j] = PAWN_EMPTY;
      }
      else if (i > 4 && i < 6)
      {
        if (j >= BOARD_SIZE - floor)
          _board[i][j] = PAWN_EMPTY;
      }
      else if (i == 6)
      {
        if (j >= BOARD_SIZE - floor)
        {
          if (j < BOARD_SIZE - floor + 2 || j >= BOARD_SIZE - 2)
            _board[i][j] = PAWN_EMPTY;
          else
            _board[i][j] = PAWN_BLACK;
        }
      }
      else if (i > 6)
      {
        if (j >= BOARD_SIZE - floor)
          _board[i][j] = PAWN_BLACK;
      }
    }

    if (i < 4)
      floor++;
    else
      floor--;
  }

  _numberOfBlackPawns = 14;
  _numberOfWhitePawns = 14;
}

sf::Vector2i abalone::GameData::getCenter() const
{
  return sf::Vector2i((BOARD_SIZE - 1) / 2, (BOARD_SIZE - 1) / 2);
}

int abalone::GameData::getColor(const int x, const int y) const
{
  if (x < BOARD_SIZE && x >= 0 && y >= 0 && y < BOARD_SIZE)
    return _board[x][y];
  else
    return PAWN_OUTSIDE;
}

int abalone::GameData::getColor(const sf::Vector2i pos) const
{
  return getColor(pos.x, pos.y);
}

int abalone::GameData::getNumberOfPawns(const int color) const
{
  if (color == PAWN_WHITE)
    return _numberOfWhitePawns;
  else
    return _numberOfBlackPawns;
}

void abalone::GameData::countNumberOfPawns()
{
  int blackPawns = 0;
  int whitePawns = 0;

  for (size_t i = 0; i < BOARD_SIZE; i++)
  {
    for (size_t j = 0; j < BOARD_SIZE; j++)
    {
      if (_board[i][j] == PAWN_BLACK || _board[i][j] == (PAWN_BLACK ^ PAWN_CLICKED))
        blackPawns++;
      if (_board[i][j] == PAWN_WHITE || _board[i][j] == (PAWN_WHITE ^ PAWN_CLICKED))
        whitePawns++;
    }
  }

  _numberOfBlackPawns = blackPawns;
  _numberOfWhitePawns = whitePawns;
}

void abalone::GameData::cleanOutside()
{
  for (int i = 0; i < 4; i++)
  {
    for (int j = 0; j < 4 - i; j++)
    {
      _board[i][BOARD_SIZE - 1 - j] = PAWN_OUTSIDE;
      _board[BOARD_SIZE - 1 - i][j] = PAWN_OUTSIDE;
    }
  }
}

void abalone::GameData::setColor(const int x, const  int y, const  int color)
{
  if (x >= 0 && x < BOARD_SIZE && y >= 0 && y < BOARD_SIZE)
    _board[x][y] = color;
}

void abalone::GameData::setColor(const sf::Vector2i pos, const  int color)
{
  setColor(pos.x, pos.y, color);
}

void abalone::GameData::makeMove(std::shared_ptr<pointVector> &clickedPawns, const  int angle, const  int dir)
{
  switch (dir)
  {
  case MOVE_DIR_R_L :
    makeMove_R_L(clickedPawns, (MoveAngle)angle);
    break;
  case MOVE_DIR_D_U:
    makeMove_D_U(clickedPawns, (MoveAngle)angle);
    break;
  case MOVE_DIR_D_R:
    makeMove_D_R(clickedPawns, (MoveAngle)angle);
    break;
  }

  cleanOutside();
  countNumberOfPawns();
}

void abalone::GameData::makeMove_R_L(std::shared_ptr<pointVector> &clickedPawns, const  int angle)
{
  int i = 0, x, y;
  int clickedSize = clickedPawns->size();

  switch ((MoveAngle)angle)
  {
  case MOVE_ANGLE_0:
    x = clickedPawns->back().x;
    y = clickedPawns->back().y;
    for (i = 0; i < clickedSize; i++)
    {
      if (_board[x][y - i]  &PAWN_CLICKED)
        _board[x][y - i] &= (~PAWN_CLICKED);

      setColor(x, y + 1 - i, getColor(x, y - i));
    }
    setColor(x, y + 1 - i, PAWN_EMPTY);
    break;
  case MOVE_ANGLE_60:
    x = clickedPawns->front().x;
    y = clickedPawns->front().y;
    for (i = 0; i < clickedSize; i++)
    {
      if (_board[x][y + i]  &PAWN_CLICKED)
        _board[x][y + i] &= (~PAWN_CLICKED);

      setColor(x - 1, y + i, getColor(x, y + i));
      setColor(x, y + i, PAWN_EMPTY);
    }
    break;
  case MOVE_ANGLE_120:
    x = clickedPawns->front().x;
    y = clickedPawns->front().y;
    for (i = 0; i < clickedSize; i++)
    {
      if (_board[x][y + i]  &PAWN_CLICKED)
        _board[x][y + i] &= (~PAWN_CLICKED);

      setColor(x - 1, y - 1 + i, getColor(x, y + i));
      setColor(x, y + i, PAWN_EMPTY);
    }
    break;
  case MOVE_ANGLE_180:
    x = clickedPawns->front().x;
    y = clickedPawns->front().y;
    for (i = 0; i < clickedSize; i++)
    {
      if (_board[x][y + i]  &PAWN_CLICKED)
        _board[x][y + i] &= (~PAWN_CLICKED);

      setColor(x, y - 1 + i, getColor(x, y + i));
    }
    setColor(x, y - 1 + i, PAWN_EMPTY);
    break;
  case MOVE_ANGLE_240:
    x = clickedPawns->back().x;
    y = clickedPawns->back().y;
    for (i = 0; i < clickedSize; i++)
    {
      if (_board[x][y - i]  &PAWN_CLICKED)
        _board[x][y - i] &= (~PAWN_CLICKED);
      setColor(x + 1, y - i, getColor(x, y - i));
      setColor(x, y - i, PAWN_EMPTY);
    }
    break;
  case MOVE_ANGLE_300:
    x = clickedPawns->front().x;
    y = clickedPawns->front().y;
    for (i = 0; i < clickedSize; i++)
    {
      if (_board[x][y + i]  &PAWN_CLICKED)
        _board[x][y + i] &= (~PAWN_CLICKED);

      setColor(x + 1, y + 1 + i, getColor(x, y + i));
      setColor(x, y + i, PAWN_EMPTY);
    }
    break;
  }
}

void abalone::GameData::makeMove_D_U(std::shared_ptr<pointVector> &clickedPawns, const  int angle)
{
  int i = 0, x, y;
  int clickedSize = clickedPawns->size();
  switch ((MoveAngle)angle)
  {
  case MOVE_ANGLE_0:
    x = clickedPawns->front().x;
    y = clickedPawns->front().y;
    for (i = 0; i < clickedSize; i++)
    {
      if (_board[x + i][y]  &PAWN_CLICKED)
        _board[x + i][y] &= (~PAWN_CLICKED);

      setColor(x + i, y + 1, getColor(x + i, y));
      setColor(x + i, y, PAWN_EMPTY);
    }
    break;
  case MOVE_ANGLE_60:
    x = clickedPawns->front().x;
    y = clickedPawns->front().y;
    for (i = 0; i < clickedSize; i++)
    {
      if (_board[x + i][y]  &PAWN_CLICKED)
        _board[x + i][y] &= (~PAWN_CLICKED);

      setColor(x - 1 + i, y, getColor(x + i, y));
    }
    setColor(x - 1 + i, y, PAWN_EMPTY);
    break;
  case MOVE_ANGLE_120:
    x = clickedPawns->front().x;
    y = clickedPawns->front().y;
    for (i = 0; i < clickedSize; i++)
    {
      if (_board[x + i][y]  &PAWN_CLICKED)
        _board[x + i][y] &= (~PAWN_CLICKED);

      setColor(x - 1 + i, y - 1, getColor(x + i, y));
      setColor(x + i, y, PAWN_EMPTY);
    }
    break;
  case MOVE_ANGLE_180:
    x = clickedPawns->front().x;
    y = clickedPawns->front().y;
    for (i = 0; i < clickedSize; i++)
    {
      if (_board[x + i][y]  &PAWN_CLICKED)
        _board[x + i][y] &= (~PAWN_CLICKED);

      setColor(x + i, y - 1, getColor(x + i, y));
      setColor(x + i, y, PAWN_EMPTY);
    }
    break;
  case MOVE_ANGLE_240:
    x = clickedPawns->back().x;
    y = clickedPawns->back().y;
    for (i = 0; i < clickedSize; i++)
    {
      if (_board[x - i][y]  &PAWN_CLICKED)
        _board[x - i][y] &= (~PAWN_CLICKED);

      setColor(x + 1 - i, y, getColor(x - i, y));
    }
    setColor(x + 1 - i, y, PAWN_EMPTY);
    break;
  case MOVE_ANGLE_300:
    x = clickedPawns->front().x;
    y = clickedPawns->front().y;
    for (i = 0; i < clickedSize; i++)
    {
      if (_board[x + i][y]  &PAWN_CLICKED)
        _board[x + i][y] &= (~PAWN_CLICKED);

      setColor(x + 1 + i, y + 1, getColor(x + i, y));
      setColor(x + i, y, PAWN_EMPTY);
    }
    break;
  }
}

void abalone::GameData::makeMove_D_R(std::shared_ptr<pointVector> &clickedPawns, const  int angle)
{
  int i = 0, x, y;
  int clickedSize = clickedPawns->size();

  switch ((MoveAngle)angle)
  {
  case MOVE_ANGLE_0:
    x = clickedPawns->front().x;
    y = clickedPawns->front().y;
    for (i = 0; i < clickedSize; i++)
    {
      if (_board[x + i][y + i]  &PAWN_CLICKED)
        _board[x + i][y + i] &= (~PAWN_CLICKED);

      setColor(x + i, y + 1 + i, getColor(x + i, y + i));
      setColor(x + i, y + i, PAWN_EMPTY);
    }
    break;
  case MOVE_ANGLE_60:
    x = clickedPawns->front().x;
    y = clickedPawns->front().y;
    for (i = 0; i < clickedSize; i++)
    {
      if (_board[x + i][y + i]  &PAWN_CLICKED)
        _board[x + i][y + i] &= (~PAWN_CLICKED);

      setColor(x - 1 + i, y + i, getColor(x + i, y + i));
      setColor(x + i, y + i, PAWN_EMPTY);
    }
    break;
  case MOVE_ANGLE_120:
    x = clickedPawns->front().x;
    y = clickedPawns->front().y;
    for (i = 0; i < clickedSize; i++)
    {
      if (_board[x + i][y + i]  &PAWN_CLICKED)
        _board[x + i][y + i] &= (~PAWN_CLICKED);

      setColor(x - 1 + i, y - 1 + i, getColor(x + i, y + i));
    }
    setColor(x - 1 + i, y - 1 + i, PAWN_EMPTY);
    break;
  case MOVE_ANGLE_180:
    x = clickedPawns->front().x;
    y = clickedPawns->front().y;
    for (i = 0; i < clickedSize; i++)
    {
      if (_board[x + i][y + i]  &PAWN_CLICKED)
        _board[x + i][y + i] &= (~PAWN_CLICKED);

      setColor(x + i, y - 1 + i, getColor(x + i, y + i));
      setColor(x + i, y + i, PAWN_EMPTY);
    }
    break;
  case MOVE_ANGLE_240:
    x = clickedPawns->front().x;
    y = clickedPawns->front().y;
    for (i = 0; i < clickedSize; i++)
    {
      if (_board[x + i][y + i]  &PAWN_CLICKED)
        _board[x + i][y + i] &= (~PAWN_CLICKED);

      setColor(x + 1 + i, y + i, getColor(x + i, y + i));
      setColor(x + i, y + i, PAWN_EMPTY);
    }
    break;
  case MOVE_ANGLE_300:
    x = clickedPawns->back().x;
    y = clickedPawns->back().y;
    for (i = 0; i < clickedSize; i++)
    {
      if (_board[x - i][y - i]  &PAWN_CLICKED)
        _board[x - i][y - i] &= (~PAWN_CLICKED);

      setColor(x + 1 - i, y + 1 - i, getColor(x - i, y - i));
    }
    setColor(x + 1 - i, y + 1 - i, PAWN_EMPTY);
    break;
  }
}