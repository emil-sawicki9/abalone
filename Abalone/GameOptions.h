#ifndef GAMEOPTIONS_H
#define GAMEOPTIONS_H

#include <SFML/Graphics.hpp>

#include <iostream>
#include <vector>

class GameOptions: public sf::Drawable
{
public:
  GameOptions() : _is_active(false)
  { 
	_label_name.push_back("MinMax");
	_label_name.push_back("Alpha-Beta");
	_label_name.push_back("MD(f)");
	_difficulty = _label_name[0];
	initialize(); 
  }
  ~GameOptions() {};

  void initialize();

  bool isActive();
  void setActive(bool);

  void saveOptions();
  void loadOptions(bool);

  int clickOption(sf::Event);

private:
  
  bool _is_active;
  int _difficulty_tmp;
  std::string _difficulty;

  //sf::SoundBuffer soundbuffer;
  //sf::Music music;

  sf::RectangleShape _bigWindow;
  std::vector<sf::RectangleShape> _button;
  std::vector<sf::RectangleShape> _radioBox;
  sf::Text _logo, _save, _cancel;
  std::vector<std::string> _label_name;
  std::vector<sf::Text> _label;
  sf::Font _font;


  virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

};

#endif // GAMEOPTIONS_H

