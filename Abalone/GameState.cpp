#include "GameState.h"

state::GameState::GameState(StateStack &stack, Context::Ptr &context)
  : State(stack, context)
  , _isDebug(true)
  , _aiAuto(false)
  , _aiCalcUpdate(-1)
  , _pawnTexture(std::make_unique<sf::Texture>(context->textures->get(textures::PawnWhite)))
  , _gameData(std::make_unique<abalone::GameData>())
  , _clickedPawns(std::make_shared<pointVector>())
  , _aiMain(std::make_unique<ai::AI>(AI_LEVEL_NORMAL, AI_TYPE_MTDF, true))
  , _aiMain2(std::make_unique<ai::AI>(AI_LEVEL_NORMAL, AI_TYPE_ALPHABETA, true))
  //, _ai_minimax(std::make_unique<ai::AI>(AI_LEVEL_NORMAL, AI_TYPE_MINIMAX, false))
  //, _ai_ab(std::make_unique<ai::AI>(AI_LEVEL_HARD, AI_TYPE_ALPHABETA, false))
  //, _ai_abTt(std::make_unique<ai::AI>(AI_LEVEL_HARD, AI_TYPE_ALPHABETA_TT, false))
  //, _ai_mtdf(std::make_unique<ai::AI>(AI_LEVEL_HARD, AI_TYPE_MTDF, false))
{
  initialize(context);
}

state::GameState::~GameState()
{
}

void state::GameState::initialize(Context::Ptr &context)
{
  //==================================================================
  // Initialize text
  //==================================================================
  sf::Font &font = context->fonts->get(fonts::Main);

  sf::Vector2f turnInfo(450.0f, 100.0f);
  auto turnText = std::make_shared<GUI::Label>("Turn: ", *context->fonts);
  turnText->setSize(30);
  turnText->setStyle(sf::Text::Bold);
  turnText->setColor(sf::Color::Red);
  turnText->setPosition(turnInfo);
  _guiContainer.push(turnText);

  _currentTurnText = std::make_shared<GUI::Label>("", *context->fonts);
  _currentTurnText->setSize(30);
  _currentTurnText->setStyle(sf::Text::Bold);  
  _currentTurnText->setPosition(turnInfo.x + turnText->getText().getLocalBounds().width, turnInfo.y);
  _guiContainer.push(_currentTurnText);

  _aiMainTime = std::make_shared<GUI::Label>("AI = 0 ms", *context->fonts);
  _aiMainTime->setSize(30);
  _aiMainTime->setStyle(sf::Text::Bold);
  _aiMainTime->setColor(sf::Color::Blue);
  _aiMainTime->setPosition(sf::Vector2f(50, 490));
  _guiContainer.push(_aiMainTime);

  if (context->gameType == GameType::GAME_AI)
  {
    _aiMain2Time = std::make_shared<GUI::Label>("AI2 = 0 ms", *context->fonts);
    _aiMain2Time->setSize(30);
    _aiMain2Time->setStyle(sf::Text::Bold);
    _aiMain2Time->setColor(sf::Color::Blue);
    _aiMain2Time->setPosition(sf::Vector2f(50, 530));
    _guiContainer.push(_aiMain2Time);
  }

  _aiCalc = std::make_shared<GUI::Label>("", *context->fonts);
  _aiCalc->setSize(30);
  _aiCalc->setStyle(sf::Text::Bold);
  _aiCalc->setColor(sf::Color::Blue);
  _aiCalc->setPosition(sf::Vector2f(50, 450));
  _guiContainer.push(_aiCalc);

  //_aiMiniMaxTime = std::make_shared<GUI::Label>("MiniMax = 0 ms", *context->fonts);
  //_aiMiniMaxTime->setSize(30);
  //_aiMiniMaxTime->setStyle(sf::Text::Bold);
  //_aiMiniMaxTime->setColor(sf::Color::Blue);
  //_aiMiniMaxTime->setPosition(sf::Vector2f(50, 550));
  //_guiContainer.push(_aiMiniMaxTime);
  //
  //_aiAbTime = std::make_shared<GUI::Label>("AlphaBeta = 0 ms", *context->fonts);
  //_aiAbTime->setSize(30);
  //_aiAbTime->setStyle(sf::Text::Bold);
  //_aiAbTime->setColor(sf::Color::Blue);
  //_aiAbTime->setPosition(sf::Vector2f(50, 580));
  //_guiContainer.push(_aiAbTime);

  //_aiAbTtTime = std::make_shared<GUI::Label>("AlphaBetaTT = 0 ms", *context->fonts);
  //_aiAbTtTime->setSize(30);
  //_aiAbTtTime->setStyle(sf::Text::Bold);
  //_aiAbTtTime->setColor(sf::Color::Blue);
  //_aiAbTtTime->setPosition(sf::Vector2f(300, 550));
  //_guiContainer.push(_aiAbTtTime);

  //_aiMTDfTime = std::make_shared<GUI::Label>("MTD(f) = 0 ms", *context->fonts);
  //_aiMTDfTime->setSize(30);
  //_aiMTDfTime->setStyle(sf::Text::Bold);
  //_aiMTDfTime->setColor(sf::Color::Blue);
  //_aiMTDfTime->setPosition(sf::Vector2f(300, 580));
  //_guiContainer.push(_aiMTDfTime);

  _deadWhite = std::make_shared<GUI::Label>("Black: 0", *context->fonts);
  _deadWhite->setSize(30);
  _deadWhite->setStyle(sf::Text::Bold);
  _deadWhite->setColor(sf::Color::Black);
  _deadWhite->setPosition(sf::Vector2f(turnInfo.x, 200));
  _guiContainer.push(_deadWhite);

  _deadBlack = std::make_shared<GUI::Label>("White: 0", *context->fonts);
  _deadBlack->setSize(30);
  _deadBlack->setStyle(sf::Text::Bold);
  _deadBlack->setColor(sf::Color::White);
  _deadBlack->setPosition(sf::Vector2f(turnInfo.x, 300));
  _guiContainer.push(_deadBlack);

  auto helpInfo = std::make_shared<GUI::Label>("F1 - Help", *context->fonts);
  helpInfo->setPosition(sf::Vector2f((float)(context->window->getSize().x - 230), (float)(context->window->getSize().y - 30)));
  helpInfo->setSize(20);
  helpInfo->setStyle(sf::Text::Bold);
  helpInfo->setColor(sf::Color::Black);
  _guiContainer.push(helpInfo);

  if (context->gameType == GameType::GAME_AI)
  {
    auto autoMoveInfo = std::make_shared<GUI::Label>("F5 - AI auto move", *context->fonts);
    autoMoveInfo->setPosition(sf::Vector2f((float)(context->window->getSize().x - 230), (float)(context->window->getSize().y - 60)));
    autoMoveInfo->setSize(20);
    autoMoveInfo->setStyle(sf::Text::Bold);
    autoMoveInfo->setColor(sf::Color::Black);
    _guiContainer.push(autoMoveInfo);
  }

  //==================================================================
  // Initialize textures
  //==================================================================
  _pawnTexture->setSmooth(true);

  _aiCalculation.setTexture(context->textures->get(textures::LoadingSpin));
  _aiCalculation.setPosition(30, 470);
  utility::centerOrigin(_aiCalculation);

  //==================================================================
  // Initialize place for pawns
  //==================================================================
  float init_x = 120.0f;
  float init_y = 100.0f;
  float pawn_x = init_x;
  float pawn_y = init_y;
  float deadPawn_x = turnInfo.x;
  float deadPawn_y = 200.0f;
  sf::Vector2u pawnSize = _pawnTexture->getSize();
  float pawn_radius = (float)pawnSize.x / 2;
  float colStart = 0.0f;
  float colDist = 2.0f;
  sf::Vector2f centerOfBoard;

  sf::Sprite pawnPlaceCircle;
  pawnPlaceCircle.setTexture(*_pawnTexture);
  pawnPlaceCircle.setColor(sf::Color(0, 0, 0, 50));

  size_t floor = 5;
  for (size_t i = 0; i < BOARD_SIZE; i++)
  {
    std::vector<sf::Sprite> pawnPlace_row;
    for (size_t j = 0; j < floor; j++)
    {
      if (i == 4 && j == 4)
        centerOfBoard = sf::Vector2f(pawn_x + pawn_radius, pawn_y + pawn_radius);
      // Add place for pawn
      pawnPlaceCircle.setPosition(pawn_x, pawn_y);
      pawnPlace_row.push_back(pawnPlaceCircle);

      //==================================================================
      // Add pawn
      //==================================================================
      if (i < 2 || i == 2 && j > 1 && j < 5)
      {
        abalone::Pawn newPawn(sf::Sprite(*_pawnTexture), sf::Vector2f(pawn_x, pawn_y), sf::Vector2i(i, j));
        _pawnShapeContainer.push_back(newPawn);
      }
      else if (i > 6 || i == 6 && j > 1 && j < 5)
      {
        int j_tmp = j + BOARD_SIZE - floor;
        sf::Sprite pawnBlackSprite = pawnPlaceCircle;
        pawnBlackSprite.setColor(sf::Color(50,50,50));
        abalone::Pawn newPawn(pawnBlackSprite, sf::Vector2f(pawn_x, pawn_y), sf::Vector2i(i, j_tmp));
        _pawnShapeContainer.push_back(newPawn);
      }

      pawn_x += pawn_radius * 2 + colDist;
    }
    _pawnPlace.push_back(pawnPlace_row);

    if (i < 4)
      floor++;
    else
      floor--;

    if (i < 4)
      colStart++;
    else
      colStart--;

    pawn_x = init_x - (pawn_radius + colDist) * colStart;
    pawn_y += pawn_radius * 2 - pawn_radius / 6;
  }

  //==================================================================
  // Initialize dead pawn places
  //==================================================================
  for (size_t i = 0; i < 2; i++)
  {
    std::vector<sf::Sprite> deadPawnPlaceRow;
    float deadPawn_x_tmp = deadPawn_x;
    deadPawn_y += 40;
    for (size_t j = 0; j < 6; j++)
    {
      pawnPlaceCircle.setPosition(deadPawn_x_tmp, deadPawn_y);
      deadPawnPlaceRow.push_back(pawnPlaceCircle);
      deadPawn_x_tmp += pawn_radius * 2 + colDist;
    }
    if (i == 0)
    {
      _deadPawnRectB.setPosition(deadPawn_x - 5, deadPawn_y - 2);
      _deadPawnRectB.setSize(sf::Vector2f(deadPawn_x_tmp - deadPawn_x + 10, pawn_radius * 2 + 4));
      _deadPawnRectW.setSize(sf::Vector2f(deadPawn_x_tmp - deadPawn_x + 10, pawn_radius * 2 + 4));
    }
    else
      _deadPawnRectW.setPosition(deadPawn_x - 5, deadPawn_y - 2);
    _deadPawnPlace.push_back(deadPawnPlaceRow);
    deadPawn_y += 60;
  }

  //==================================================================
  // Initialize background
  //==================================================================
  _boardRect.setPosition(0, 0);
  _boardRect.setSize(context->window->getDefaultView().getSize());
  _boardRect.setFillColor(sf::Color(192, 192, 192));

  _boardBackground.setPosition(centerOfBoard);
  _boardBackground.setRadius(200.0f);
  _boardBackground.setPointCount(6);
  _boardBackground.rotate(30);
  _boardBackground.setFillColor(sf::Color(150, 150, 150));
  utility::centerOrigin(_boardBackground);

  _deadPawnRectB.setFillColor(_boardBackground.getFillColor());
  _deadPawnRectW.setFillColor(_boardBackground.getFillColor());

  //==================================================================
  // Initialize AI
  //==================================================================
  if (context->gameType != GAME_PVP) // debug (comment)
  {
    // init Zobrist
    std::shared_ptr<ai::zobristVectors> zobristKeyTable = std::make_shared<ai::zobristVectors>();
    std::mt19937_64 generator((unsigned long long)time(NULL));
    for (size_t i = 0; i < 2; i++)
    {
      zobristKeyTable->push_back(std::vector<unsigned long long>(62));
      for (size_t j = 0; j < 62; j++)
        zobristKeyTable->at(i).at(j) = generator();
    }

    _aiMain->setZobristKeyTable(zobristKeyTable);
    _aiMain2->setZobristKeyTable(zobristKeyTable);
    //_ai_abTt->setZobristKeyTable(zobristKeyTable);
    //_ai_mtdf->setZobristKeyTable(zobristKeyTable);
  }

  // Starting color
  _currentTurn = PAWN_WHITE;
  _currentTurnText->setText("White");
  _currentTurnText->setColor(sf::Color::White);


  if (context->gameType == GameType::GAME_PVE)
  {
    if (context->startingColor == PAWN_BLACK)
    {
      _aiMain->setAiLevel(context->aiOptions.whiteAiLevel);
      _aiMain->execute(_gameData, _currentTurn);
    }
    else
      _aiMain->setAiLevel(context->aiOptions.blackAiLevel);
  }
  else if (context->gameType == GameType::GAME_AI)
  {
    _aiMain->setAiLevel(context->aiOptions.whiteAiLevel);
    _aiMain2->setAiLevel(context->aiOptions.blackAiLevel);
  }
}

void state::GameState::draw()
{
  sf::RenderWindow &window = *State::getContext()->window;

  window.setView(window.getDefaultView());

  window.setView(*getContext()->gameView);
  window.draw(_boardRect);

  window.draw(_boardBackground);
  window.draw(_deadPawnRectB);
  window.draw(_deadPawnRectW);
  // Place for pawns
  for (size_t i = 0; i < _pawnPlace.size(); i++)
  {
    for (size_t j = 0; j < _pawnPlace.at(i).size(); j++)
    {
      window.draw(_pawnPlace.at(i).at(j));
    }
  }
  // Place for dead pawns
  for (size_t i = 0; i < _deadPawnPlace.size(); i++)
  {
    for (size_t j = 0; j < _deadPawnPlace.at(i).size(); j++)
    {
      window.draw(_deadPawnPlace.at(i).at(j));
    }
  }
  // Pawns
  for (size_t i = 0; i < _pawnShapeContainer.size(); i++)
  {
    window.draw(_pawnShapeContainer.at(i).getPawn());
  }
  // Hit possibilities
  for (size_t i = 0; i < _hitContainer.size(); i++)
  {
    for (size_t j = 0; j < _hitContainer.at(i).shapeContainer.size(); j++)
    {
      window.draw(_hitContainer.at(i).shapeContainer.at(j));
    }
  }
  // Move possibilities
  for (size_t i = 0; i < _moveContainer.size(); i++)
  {
    for (size_t j = 0; j < _moveContainer.at(i).shapeContainer.size(); j++)
    {
      window.draw(_moveContainer.at(i).shapeContainer.at(j));
    }
  }

  if (_aiMain->isRunning() || _aiMain2->isRunning())
    window.draw(_aiCalculation);
  window.draw(_guiContainer);
}

bool state::GameState::update(sf::Time dt)
{
  //if (_ai_minimax->isRunning())
  //{
  //  _aiMiniMaxTime->setText("MiniMax = " + std::to_string(_ai_minimax->getElapsedTime()) + "ms");
  //  if (_ai_minimax->isFinished())
  //  {
  //    _ai_minimax->stopThread();
  //    _ai_ab->execute(_gameData, _currentTurn);
  //  }
  //}
  //else if (_ai_ab->isRunning())
  //{
  //  _aiAbTime->setText("AlphaBeta = " + std::to_string(_ai_ab->getElapsedTime()) + "ms");
  //  if (_ai_ab->isFinished())
  //  {
  //    _ai_ab->stopThread();
  //    _ai_abTt->execute(_gameData, _currentTurn);
  //  }
  //}
  //else if (_ai_abTt->isRunning())
  //{
  //  _aiAbTtTime->setText("AlphaBetaTT = " + std::to_string(_ai_abTt->getElapsedTime()) + "ms");
  //  if (_ai_abTt->isFinished())
  //  {
  //    _ai_abTt->stopThread();
  //    //_ai_mtdf->execute(_gameData, _currentTurn);
  //  }
  //}
  //else if (_ai_mtdf->isRunning())
  //{
  //  _aiMTDfTime->setText("mtd(f) = " + std::to_string(_ai_mtdf->getElapsedTime()) + "ms");
  //  if (_ai_mtdf->isFinished())
  //  {
  //    _ai_mtdf->stopThread();
  //    _ai_abTt->execute(_gameData, _currentTurn);
  //  }
  //}
  /*else*/ if (_aiMain->isRunning())
  {
    _aiMainTime->setText("AI = " + std::to_string(_aiMain->getElapsedTime()) + " ms");
    _aiCalculation.rotate(10);
    _aiCalcUpdate++;
    if (_aiCalcUpdate > 20)
    {
      updateAiCalc();
      _aiCalcUpdate = 0;
    }
    if (_aiMain->isFinished())
    {
      _aiMain->stopThread();
      _aiCalc->setText("");
      makeMove(_aiMain->getMoveData());
    }
  }
  else if (_aiMain2->isRunning())
  {
    _aiMain2Time->setText("AI2 = " +std::to_string(_aiMain2->getElapsedTime()) + " ms");
    _aiCalculation.rotate(10);
    _aiCalcUpdate++;
    if (_aiCalcUpdate > 20)
    {
      updateAiCalc();
      _aiCalcUpdate = 0;
    }
    if (_aiMain2->isFinished())
    {
      _aiMain2->stopThread();
      _aiCalc->setText("");
      makeMove(_aiMain2->getMoveData());
    }
  }


  return true;
}

void state::GameState::updateAiCalc()
{
  int size = _aiCalc->getText().getString().getSize();
  switch (size)
  {
  case 0:
  case 14:
    _aiCalc->setText("Calculating.");
    break;
  case 12:
    _aiCalc->setText("Calculating..");
    break;
  case 13:
    _aiCalc->setText("Calculating...");
    break;
  }
}

bool state::GameState::handleEvent(const sf::Event &event)
{
  switch (event.type)
  {
    /////////////////
    // MOUSE CLICK //
    /////////////////
    case sf::Event::MouseButtonPressed:
    {
      if (State::getContext()->gameType == GameType::GAME_PVP || (State::getContext()->gameType == GAME_PVE && State::getContext()->startingColor == _currentTurn))
      {
        sf::Vector2f local_click;

        sf::RenderWindow &window = *State::getContext()->window;
        window.setView(*getContext()->gameView);
        // convert to world coordinates
        local_click = window.mapPixelToCoords(sf::Vector2i(event.mouseButton.x, event.mouseButton.y));

        if (_clickedPawns->size() > 0)
        {
          //==================================================================
          // if move clicked //
          //==================================================================
          for (size_t i = 0; i < _moveContainer.size(); i++)
          {
            for (size_t j = 0; j < _moveContainer.at(i).shapeContainer.size(); j++)
            {
              if (_moveContainer.at(i).shapeContainer.at(j).getGlobalBounds().contains(local_click))
              {
                makeMove(_moveContainer.at(i).moveData);
                return true;
              }
            }
          }
          if (_clickedPawns->size() > 1)
          {
            //==================================================================
            // if hit clicked //
            //==================================================================
            for (size_t i = 0; i < _hitContainer.size(); i++)
            {
              for (size_t j = 0; j < _hitContainer.at(i).shapeContainer.size(); j++)
              {
                if (_hitContainer.at(i).shapeContainer.at(j).getGlobalBounds().contains(local_click))
                {
                  makeMove(_hitContainer.at(i).moveData);
                  return true;
                }
              }
            }
          }
        }
        //==================================================================
        // PAWN SELECTION //
        //==================================================================
        for (size_t i = 0; i < _pawnShapeContainer.size(); i++)
        {
          if (_pawnShapeContainer.at(i).getPawn().getGlobalBounds().contains(local_click))
          {
            pawnSelectedEvent(_pawnShapeContainer.at(i).getPosition(), i);
            return true;
          }
        }
      }
      break;
    }
    /////////////////
    // MOUSE MOVED //
    /////////////////
    case sf::Event::MouseMoved:
    {
      //if (State::getContext()->gameType != GameType::GAME_AI)
      //{
        sf::RenderWindow &window = *State::getContext()->window;
        window.setView(*getContext()->gameView);
        sf::Vector2f mousePos = window.mapPixelToCoords(sf::Vector2i(event.mouseMove.x, event.mouseMove.y));
        mouseHoverEvent(mousePos);
      //}
      break;
    }
    /////////////////
    // KEY PRESSED //                   
    /////////////////
    case sf::Event::KeyPressed:
      switch (event.key.code)
      {
      case sf::Keyboard::Space:
      case sf::Keyboard::Return:
        if (!_aiAuto && State::getContext()->gameType == GAME_AI)
          makeAiMove();
        break;
      case sf::Keyboard::Escape:
        requestStackPush(state::Pause);
        break;
      case sf::Keyboard::F1:
        requestStackPush(state::HelpKey);
        break;
      //case sf::Keyboard::F2:
      //  makeAiMove();
      //  break;
      case sf::Keyboard::F5:
        if (State::getContext()->gameType == GameType::GAME_AI)
        {
          if (_aiAuto)
            _aiAuto = false;
          else
          {
            _aiAuto = true;
            makeAiMove();
          }
        }
        break;
      //case sf::Keyboard::F6:
      //  if (_currentTurn == PAWN_WHITE)
      //  {
      //    std::cout << "\n";
      //    _ai_mtdf->execute(_gameData, _currentTurn);
      //  }
      //  break;
      case sf::Keyboard::F10:
        _isDebug = !_isDebug;
        _aiMainTime->setVisible(_isDebug);
        _aiMain2Time->setVisible(_isDebug);
        //_aiMiniMaxTime->setVisible(_isDebug);
        //_aiAbTime->setVisible(_isDebug);
        //_aiAbTtTime->setVisible(_isDebug);
        //_aiMTDfTime->setVisible(_isDebug);
        break;
      }
      break;
    default:
      break;
  }

  return true;
}

void state::GameState::makeAiMove()
{
  updateAiCalc();
  if (_currentTurn == PAWN_WHITE)
    _aiMain->execute(_gameData, _currentTurn);
  else
    _aiMain2->execute(_gameData, _currentTurn);
}

void state::GameState::switchTurn()
{
  clearSelection();

  int deadWhite = PAWN_MAX_NUMBER - _gameData->getNumberOfPawns(PAWN_WHITE);
  int deadBlack = PAWN_MAX_NUMBER - _gameData->getNumberOfPawns(PAWN_BLACK);

  if (deadWhite == 6 || deadBlack == 6)
  {
    int &winColor = State::getContext()->winColor;
    if (deadBlack == 6)
      winColor = PAWN_WHITE;
    else
      winColor = PAWN_BLACK;
    
    requestStackPush(state::Win);
  }

  if (_currentTurn == PAWN_BLACK)
  {
    _currentTurn = PAWN_WHITE;
    _currentTurnText->setText("White");
    _currentTurnText->setColor(sf::Color::White);
  }
  else // if(_currentTurn == PAWN_WHITE)
  {
    _currentTurn = PAWN_BLACK;
    _currentTurnText->setText("Black");
    _currentTurnText->setColor(sf::Color::Black);
  }
  _deadWhite->setText("Black: " + std::to_string(deadWhite));
  _deadBlack->setText("White: " + std::to_string(deadBlack));


  if (State::getContext()->gameType == GameType::GAME_PVE && State::getContext()->startingColor != _currentTurn)
  {
    _aiMain->execute(_gameData, _currentTurn);
  }
  else if (State::getContext()->gameType == GameType::GAME_AI && _aiAuto)
  {
    if (_currentTurn == PAWN_WHITE)
      _aiMain->execute(_gameData, _currentTurn);
    else
      _aiMain2->execute(_gameData, _currentTurn);
  }
}

void state::GameState::movePawns(abalone::MoveData &moveData)
{
  abalone::Verificator::sortClicked(moveData.clickedPawns);

  //==================================================================
  // Moving pawns
  //==================================================================
  for (size_t i = 0; i < moveData.clickedPawns->size(); i++)
  {
    sf::Vector2i newPos = abalone::Verificator::calculateNewPos(moveData.clickedPawns->at(i), moveData.angle);
    int nextPlaceColor = _gameData->getColor(newPos);
    sf::Vector2f placePos;
    if (nextPlaceColor == PAWN_OUTSIDE)
    {
      int color = _gameData->getColor(moveData.clickedPawns->front());
      int row = color == PAWN_WHITE ? 0 : 1;
      int numberDead = PAWN_MAX_NUMBER - _gameData->getNumberOfPawns(color); 
      placePos = _deadPawnPlace.at(row).at(numberDead).getPosition();
      placePos.x -= 2;
      placePos.y -= 2;
    }
    else
    {
      placePos = getPawnPlacePosition(newPos);
    }
    
    // Moving pawn CircleShape
    for (size_t j = 0; j < _pawnShapeContainer.size(); j++)
    {
      abalone::Pawn &pawn = _pawnShapeContainer.at(j);
      if (pawn.getPosition() == moveData.clickedPawns->at(i) && pawn.isDead() == false)
      {
        if (nextPlaceColor == PAWN_OUTSIDE)
          pawn.die(placePos);
        else
          pawn.move(newPos, placePos);
        break;
      }
    }
  }  

  _gameData->makeMove(moveData.clickedPawns, moveData.angle, moveData.direction);
}

void state::GameState::makeMove(abalone::MoveData &moveData)
{
  abalone::Verificator::sortClicked(moveData.clickedPawns);
  std::shared_ptr<pointVector> whiteMove = std::make_shared<pointVector>();
  std::shared_ptr<pointVector> blackMove = std::make_shared<pointVector>();
  for (size_t i = 0; i < moveData.clickedPawns->size(); i++)
  {
    sf::Vector2i pos = moveData.clickedPawns->at(i);
    int color = _gameData->getColor(pos);
    if (color == PAWN_WHITE || (color  &~PAWN_CLICKED) == PAWN_WHITE)
      whiteMove->push_back(pos);
    else if (color == PAWN_BLACK || (color  &~PAWN_CLICKED) == PAWN_BLACK)
      blackMove->push_back(pos);
  }

  abalone::MoveData move;
  move.angle = moveData.angle;
  move.direction = moveData.direction;
  
  int whiteSize = whiteMove->size();
  int blackSize = blackMove->size();
  // First move enemy pawns and then clicked
  // white is bigger <- attacker
  if (whiteSize - blackSize > 0)
  {
    if (blackSize > 0)
    {
      move.clickedPawns = blackMove;
      movePawns(move);
    }
    move.clickedPawns = whiteMove;
    movePawns(move);
  }
  // black is bigger <- attacker
  else
  {
    if (whiteSize > 0)
    {
      move.clickedPawns = whiteMove;
      movePawns(move);
    }
    move.clickedPawns = blackMove;
    movePawns(move);
  }

  switchTurn();
}

void state::GameState::clearSelection()
{
  for (size_t i = 0; i < _pawnShapeContainer.size(); i++)
  {
    _pawnShapeContainer.at(i).setClicked(false);
  }
  _clickedPawns->clear();
  _moveContainer.clear();
  _hitContainer.clear();
}

void state::GameState::pawnSelectedEvent(const sf::Vector2i pawnPos, const int index)
{
  int color = _gameData->getColor(pawnPos);

  //==================================================================
  // UNSELECT PAWN
  //==================================================================
  if (color  & PAWN_CLICKED) 
  {
    if (verificateUnselect(pawnPos))
    {
      _pawnShapeContainer[index].setClicked(false);
      _gameData->setColor(pawnPos, color  &~PAWN_CLICKED);
      for (size_t i = 0; i < _clickedPawns->size(); i++)
      {
        if (pawnPos == _clickedPawns->at(i))
          _clickedPawns->at(i) = std::move(_clickedPawns->back());
      }
      _clickedPawns->pop_back();
      calculateMoves();
    }
  }
  //==================================================================
  // SELECT PAWN
  //==================================================================
  else if (color == _currentTurn) 
  {
    if (verificateSelect(pawnPos))
    {
      _pawnShapeContainer[index].setClicked(true);
      _gameData->setColor(pawnPos, color ^ PAWN_CLICKED);
      _clickedPawns->push_back(pawnPos);
      calculateMoves();
    }
  }    
}

bool state::GameState::verificateSelect(const sf::Vector2i selectPos) const
{
  bool result = false;

  if (_clickedPawns->size() == 0)
  {
    result = true;
  }
  else if (_clickedPawns->size() == 1)
  {
    sf::Vector2i pos = _clickedPawns->front();
    int x = selectPos.x;
    int y = selectPos.y;
    bool x_cond = x == pos.x && (y == pos.y - 1 || y == pos.y + 1);
    bool y_cond = y == pos.y && (x == pos.x - 1 || x == pos.x + 1);
    bool xy_cond = (x == pos.x - 1 && y == pos.y - 1) || (x == pos.x + 1 && y == pos.y + 1);

    if (x_cond || y_cond || xy_cond)
      result = true;
  }
  else if (_clickedPawns->size() == 2)
  {
    sf::Vector2i pos1 = _clickedPawns->front();
    sf::Vector2i pos2 = _clickedPawns->back();
    int x = selectPos.x;
    int y = selectPos.y;
    int x_diff = std::abs(pos1.x - pos2.x);
    int y_diff = std::abs(pos1.y - pos2.y);
    bool pos1_c1 = x == pos1.x - x_diff && y == pos1.y - y_diff;
    bool pos1_c2 = x == pos1.x + x_diff && y == pos1.y + y_diff;
    bool pos2_c1 = x == pos2.x - x_diff && y == pos2.y - y_diff;
    bool pos2_c2 = x == pos2.x + x_diff && y == pos2.y + y_diff;

    if (pos1_c1 || pos1_c2 || pos2_c1 || pos2_c2)
      result = true;
  }

  return result;
}

bool state::GameState::verificateUnselect(const sf::Vector2i unselectPos) const
{
  bool result = true;

  if (_clickedPawns->size() == 3)
  {
    sf::Vector2i pos1 = _clickedPawns->at(0);
    sf::Vector2i pos2 = _clickedPawns->at(1);
    sf::Vector2i pos3 = _clickedPawns->at(2);
    auto getMidFunc = [](const int &a, const int &b, const int &c)->const int
    {
      std::vector<int> vec { a, b, c };
      std::sort(vec.begin(), vec.end());
      return vec.at(1);
    };
    int midX = getMidFunc(pos1.x, pos2.x, pos3.x);
    int midY = getMidFunc(pos1.y, pos2.y, pos3.y);

    if (midX == unselectPos.x && midY == unselectPos.y)
      result = false;
  }

  return result;
}

void state::GameState::calculateMoves()
{
  _moveContainer.clear();
  _hitContainer.clear();

  if (_clickedPawns->empty())
    return;

  // sort clicked pawns
  abalone::Verificator::sortClicked(_clickedPawns);
  // check directions of clicked pawns
  int dir = (MoveDirection)checkClickedDirection();

  for (int angle = MOVE_ANGLE_0; angle <= MOVE_ANGLE_300; angle++)
  {
    std::shared_ptr<pointVector> hitPos = std::make_shared<pointVector>();
    bool status = false;

    if (dir == MOVE_DIR_R_L)
      status = abalone::Verificator::verificate_R_L(_clickedPawns, (MoveAngle)angle, _gameData, hitPos);
    else if (dir == MOVE_DIR_D_U)
      status = abalone::Verificator::verificate_D_U(_clickedPawns, (MoveAngle)angle, _gameData, hitPos);
    else if (dir == MOVE_DIR_D_R)
      status = abalone::Verificator::verificate_D_R(_clickedPawns, (MoveAngle)angle, _gameData, hitPos);

    if (status)
    {
      abalone::MoveShape moveVariation;
      moveVariation.moveData.angle = (MoveAngle)angle;
      moveVariation.moveData.direction = (MoveDirection)dir;
      bool hit = hitPos->size() > 0 ? true : false;   
      if (hit)
      {
        moveVariation.shapeContainer = createHoverPawns(hitPos, (MoveAngle)angle, dir, hit);
        hitPos->insert(hitPos->end(), _clickedPawns->begin(), _clickedPawns->end());
        moveVariation.moveData.clickedPawns = hitPos;
        _hitContainer.push_back(moveVariation);
      }      
      else
      {
        moveVariation.shapeContainer = createHoverPawns(_clickedPawns, (MoveAngle)angle, dir, hit);
        moveVariation.moveData.clickedPawns = _clickedPawns;
        _moveContainer.push_back(moveVariation);
      }
    }
  }
}

int state::GameState::checkClickedDirection() const
{
  if (_clickedPawns->size() == 1)
    return MOVE_DIR_R_L;
  else
  {
    sf::Vector2i pos1 = _clickedPawns->front();
    sf::Vector2i pos2 = _clickedPawns->back();

    if (pos1.x == pos2.x)
      return MOVE_DIR_R_L;
    else if (pos1.y == pos2.y)
      return MOVE_DIR_D_U;
    else
      return MOVE_DIR_D_R;
  }
}

std::vector<sf::Sprite> state::GameState::createHoverPawns(const std::shared_ptr<pointVector> &clickedPos, const int angle, const int dir, const bool hit) const
{
  std::vector<sf::Sprite> result;

  int i = 0;
  int size = clickedPos->size();

  if (hit == false)
  {
    // When moving in line hover 1 circle
    // RIGHT  &LEFT
    bool R_L_first = angle == MOVE_ANGLE_180 && dir == MOVE_DIR_R_L;
    bool R_L_last = angle == MOVE_ANGLE_0 && dir == MOVE_DIR_R_L;
    // DOWN  &UP
    bool D_U_first = angle == MOVE_ANGLE_60 && dir == MOVE_DIR_D_U;
    bool D_U_last = angle == MOVE_ANGLE_240 && dir == MOVE_DIR_D_U;
    // DOWN  &RIGHT
    bool D_R_first = angle == MOVE_ANGLE_120 && dir == MOVE_DIR_D_R;
    bool D_R_last = angle == MOVE_ANGLE_300 && dir == MOVE_DIR_D_R;

    if (R_L_first || D_U_first || D_R_first)
      size = 1;
    else if (R_L_last || D_U_last || D_R_last)
      i = size - 1;
  }

  for (; i < size; i++)
  {
    sf::Vector2i hoverPos = hit ? clickedPos->at(i) : abalone::Verificator::calculateNewPos(clickedPos->at(i), angle);
    sf::Vector2f pawnPlace = getPawnPlacePosition(hoverPos);
    sf::Sprite hoverCircle;
    hoverCircle.setTexture(*_pawnTexture);
    hoverCircle.setPosition(pawnPlace);
    hoverCircle.setColor(sf::Color::Transparent);

    result.push_back(hoverCircle);
  }

  return result;
}

sf::Vector2f state::GameState::getPawnPlacePosition(sf::Vector2i pos) const
{
  try{
    // for bottom part of board 
    // (converting 9x9 board to irregular vector with pawnPlaces size)
    if (pos.x > 4)
      pos.y -= pos.x - 4;
    sf::Vector2f resultPos = _pawnPlace.at(pos.x).at(pos.y).getPosition();

    return resultPos;
  }
  catch (const std::exception &e)
  {
    DEBUG_WITH_LINE << "\nException: " << e.what() << std::endl;
    return sf::Vector2f(0, 0);
  }  
}

void state::GameState::mouseHoverEvent(const sf::Vector2f mousePos)
{
  sf::Texture *texture = new sf::Texture();
  texture->create(50, 50);
  //==================================================================
  // Hightlight possible move
  //==================================================================
  int hoverMove = -1;
  for (size_t i = 0; i < _moveContainer.size(); i++)
  {
    for (size_t j = 0; j < _moveContainer.at(i).shapeContainer.size(); j++)
    {
      sf::Sprite &hoverShape = _moveContainer.at(i).shapeContainer.at(j);
      hoverShape.setColor(sf::Color::Transparent);
      if (hoverMove == -1)
        // If mouse over possible move
        if (hoverShape.getGlobalBounds().contains(sf::Vector2f(mousePos)))
          hoverMove = i;
    }
  }
  //  Hightlight indicated possible move
  if (hoverMove > -1)
  {
    for (size_t i = 0; i < _moveContainer.at(hoverMove).shapeContainer.size(); i++)
      _moveContainer.at(hoverMove).shapeContainer.at(i).setColor(sf::Color::Green);
  }
  //==================================================================
  // Hightlight possible hit
  //==================================================================
  int hoverHit = -1;
  for (size_t i = 0; i < _hitContainer.size(); i++)
  {
    for (size_t j = 0; j < _hitContainer.at(i).shapeContainer.size(); j++)
    {
      sf::Sprite &hoverShape = _hitContainer.at(i).shapeContainer.at(j);
      hoverShape.setColor(sf::Color::Transparent);
      if (hoverHit == -1)
        // If mouse over possible hit
        if (hoverShape.getGlobalBounds().contains(sf::Vector2f(mousePos)))
          hoverHit = i;
    }
  }
  //  Hightlight indicated possible hit
  if (hoverHit > -1)
  {
    for (size_t i = 0; i < _hitContainer.at(hoverHit).shapeContainer.size(); i++)
      _hitContainer.at(hoverHit).shapeContainer.at(i).setColor(sf::Color::Red);
  }
}
