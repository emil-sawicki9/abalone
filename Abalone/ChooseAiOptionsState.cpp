#include "ChooseAiOptionsState.h"

state::ChooseAiOptionsState::ChooseAiOptionsState(StateStack &stack, Context::Ptr &context)
  : State(stack, context)
  , _backgroundSprite()
{
  initialize(context);
}

state::ChooseAiOptionsState::~ChooseAiOptionsState()
{
}

void state::ChooseAiOptionsState::initialize(Context::Ptr &context)
{
  sf::Font &font = context->fonts->get(fonts::Main);
  sf::Vector2f viewSize = context->window->getView().getSize();

  auto instructionText = std::make_shared<GUI::Label>("", *context->fonts);

  if (context->startingColor == PAWN_WHITE)
    _aiColor = PAWN_BLACK;
  else if (context->startingColor == PAWN_BLACK)
    _aiColor = PAWN_WHITE;
  else if (context->aiOptions.whiteAiLevel == AI_LEVEL_NONE)
    _aiColor = PAWN_WHITE;
  else
    _aiColor = PAWN_BLACK;
  
  if (_aiColor == PAWN_WHITE)
    instructionText->setText("Choose WHITE AI Level");
  else
    instructionText->setText("Choose BLACK AI Level");
    
  instructionText->setSize(30);
  instructionText->setPosition(0.5f * viewSize.x, 0.4f * viewSize.y);
  instructionText->center();
  _guiContainer.push(instructionText);

  _backgroundSprite.setFillColor(sf::Color(0, 0, 0, 150));

  sf::Vector2f buttonPos = viewSize / 2.f;
  auto easyButton = std::make_shared<GUI::Button>(*context->fonts, *context->textures);
  easyButton->setPosition(buttonPos);
  easyButton->setText("Easy");
  easyButton->setSize(30);
  easyButton->setCallback([this]()
  {
    setAiLevel(AiLevel::AI_LEVEL_EASY);
  });
  _guiContainer.push(easyButton);

  auto mediumButton = std::make_shared<GUI::Button>(*context->fonts, *context->textures);
  mediumButton->setPosition(buttonPos + sf::Vector2f(0.0f, 70.0f));
  mediumButton->setText("Medium");
  mediumButton->setSize(30);
  mediumButton->setCallback([this]()
  {
    setAiLevel(AiLevel::AI_LEVEL_NORMAL);
  });
  _guiContainer.push(mediumButton);

  auto hardButton = std::make_shared<GUI::Button>(*context->fonts, *context->textures);
  hardButton->setPosition(buttonPos + sf::Vector2f(0.0f, 140.0f));
  hardButton->setText("Hard");
  hardButton->setSize(30);
  hardButton->setCallback([this]()
  {
    setAiLevel(AiLevel::AI_LEVEL_HARD);
  });
  _guiContainer.push(hardButton);
}

void state::ChooseAiOptionsState::setAiLevel(int aiLevel)
{
  if (_aiColor == PAWN_WHITE)
  {
    State::getContext()->aiOptions.whiteAiLevel = (AiLevel) aiLevel;
  }
  else
  {
    State::getContext()->aiOptions.blackAiLevel = (AiLevel) aiLevel;
  }

  requestStackPop();
  if (State::getContext()->gameType == GameType::GAME_PVE || (State::getContext()->aiOptions.blackAiLevel != AiLevel::AI_LEVEL_NONE && State::getContext()->aiOptions.whiteAiLevel != AiLevel::AI_LEVEL_NONE))
    requestStackPush(state::Game);
  else
    requestStackPush(state::ChooseAiOptions);
}

void state::ChooseAiOptionsState::draw()
{
  sf::RenderWindow &window = *State::getContext()->window;
  window.setView(*getContext()->gameView);

  _backgroundSprite.setSize(window.getView().getSize());

  window.draw(_backgroundSprite);
  window.draw(_guiContainer);
}

bool state::ChooseAiOptionsState::update(sf::Time)
{
  return false;
}

bool state::ChooseAiOptionsState::handleEvent(const sf::Event &event)
{
  _guiContainer.handleEvent(event);

  return false;
}