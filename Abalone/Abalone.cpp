#include "Abalone.h"

const sf::Time abalone::Abalone::TimePerFrame = sf::seconds(1.f / 60.f);

abalone::Abalone::Abalone()
  : _mainWindow(std::make_shared<sf::RenderWindow>(sf::VideoMode(800, 640), "Abalone game", sf::Style::Close/*| sf::Style::Resize*/))
  , _textures(std::make_shared<TextureHolder>())
  , _fonts(std::make_shared<FontHolder>())
  , _gameView(std::make_shared<sf::View>())
  , _stateStack(std::make_shared<state::State::Context>(state::State::Context(_mainWindow, _textures, _fonts, _gameView)))
  , _fpsText()
  , _fps()
  , _secPerFrame(0)
{
  initialize();
}

abalone::Abalone::~Abalone()
{
}

void abalone::Abalone::initialize()
{
  _WIDTH_ = (float)_mainWindow->getSize().x;
  _HEIGHT_ = (float)_mainWindow->getSize().y;
  _ASPECT_RATIO_ = (float)_WIDTH_ / (float)_HEIGHT_;

  sf::Image icon;
  if (icon.loadFromFile("abalone.jpg"))
  {
    sf::Vector2u iconSize = icon.getSize();
    _mainWindow->setIcon(iconSize.x, iconSize.y, icon.getPixelsPtr());
  }

  _mainWindow->setKeyRepeatEnabled(false);
  
  _mainWindow->setFramerateLimit(60);
  sf::Vector2i windowPos;
  windowPos.x = sf::VideoMode::getDesktopMode().width / 2 - (int)_WIDTH_ / 2;
  windowPos.y = 10;
  _mainWindow->setPosition(windowPos);

  sf::Vector2f viewSize = sf::Vector2f(_WIDTH_, _HEIGHT_);
  //_gameView->reset(sf::FloatRect(0, 0, viewSize.x, viewSize.y));
  _gameView->setSize(viewSize);
  _gameView->setCenter(viewSize / 2.0f);

  //_fonts->load(fonts::Main, "arial.ttf");
  _fonts->load(fonts::Main, "tnr.ttf");
  //_textures.load(textures::TitleScreen, "textures/TitleScreen.png");
  _textures->load(textures::ButtonNormal, "textures/ButtonNormal.png");
  _textures->load(textures::ButtonSelected, "textures/ButtonSelected.png");
  _textures->load(textures::ButtonPressed, "textures/ButtonPressed.png");
  _textures->load(textures::PawnWhite, "textures/PawnWhite.png");
  _textures->load(textures::LoadingSpin, "textures/LoadingSpin.png");
  _fpsText.setFont(_fonts->get(fonts::Main));
  _fpsText.setColor(sf::Color::Blue);
  _fpsText.setPosition(5.f, _mainWindow->getSize().y - 25.f);
  _fpsText.setCharacterSize(10u);

  registerStates();
  _stateStack.pushState(state::Menu); 
}

void abalone::Abalone::run()
{
  sf::Clock clock;
  sf::Time timeSinceLastUpdate = sf::Time::Zero;

  while (_mainWindow->isOpen())
  {
    sf::Time dt = clock.restart();
    timeSinceLastUpdate += dt;
    while (timeSinceLastUpdate > TimePerFrame)
    {
      timeSinceLastUpdate -= TimePerFrame;

      processInput();
      update(TimePerFrame);

      // Check inside this loop, because stack might be empty before update() call
      if (_stateStack.isEmpty())
      {
        _mainWindow->close();
        exit(0);
      }
        
    }

    updateStatistics(dt);
    render();
  }
}

sf::Vector2f abalone::Abalone::calculateAspectRatio(const sf::Vector2f in, const sf::Vector2f clip)
{
  sf::Vector2f ret(in);
  if ((clip.y * in.x) / in.y >= clip.x)
  {
    ret.y = (clip.x * in.y) / in.x;
    ret.x = clip.x;
  }
  else if ((clip.x * in.y) / in.x >= clip.y)
  {
    ret.x = (clip.y * in.x) / in.y;
    ret.y = clip.y;
  }
  else
    ret = clip;
  return ret;
}

void abalone::Abalone::processInput()
{
  sf::Event event;
  while (_mainWindow->pollEvent(event))
  {
    switch (event.type)
    {
      case sf::Event::Resized:
      {
        sf::Vector2f visible = sf::Vector2f((float)event.size.width, (float)event.size.height);
        if (visible.x < _WIDTH_)
        {
          visible.x = _WIDTH_;
          _mainWindow->setSize((sf::Vector2u)visible);
        }
        if (visible.y < _HEIGHT_)
        {
          visible.y = _HEIGHT_;
          _mainWindow->setSize((sf::Vector2u)visible);
        }

        _gameView->setSize(visible);
        
        _mainWindow->setView(*_gameView);
        break;
      }
      case sf::Event::Closed :
        _mainWindow->close();
        exit(0);
        break;
      default :
        break;
    }   

    _stateStack.handleEvent(event);

  }
}

void abalone::Abalone::update(sf::Time dt)
{
  _stateStack.update(dt);
}

void abalone::Abalone::render()
{
  _mainWindow->clear();

  _stateStack.draw();

  _mainWindow->setView(*_gameView);
  _mainWindow->draw(_fpsText);

  _mainWindow->display();
}

void abalone::Abalone::updateStatistics(sf::Time dt)
{
  _fps += dt;
  _secPerFrame += 1;
  if (_fps >= sf::seconds(1.0f))
  {
    std::stringstream ss;
    ss << _secPerFrame;

    _fpsText.setString("FPS: " + ss.str());

    _fps -= sf::seconds(1.0f);
    _secPerFrame = 0;
  }
}

void abalone::Abalone::registerStates()
{
  _stateStack.registerState<state::MenuState>(state::Menu);
  _stateStack.registerState<state::GameState>(state::Game);
  _stateStack.registerState<state::PauseState>(state::Pause);
  _stateStack.registerState<state::WinState>(state::Win);
  _stateStack.registerState<state::ChooseColorState>(state::ChoosePlayerColor);
  _stateStack.registerState<state::ChooseAiOptionsState>(state::ChooseAiOptions);
  _stateStack.registerState<state::HelpKeyState>(state::HelpKey);
}

std::unique_ptr<sf::Texture> abalone::Abalone::LoadTextureFromResource(const std::string &name)
{
  HRSRC rsrcData = FindResource(NULL, name.c_str(), RT_RCDATA);
  if (!rsrcData)
    throw std::runtime_error("Failed to find resource.");

  DWORD rsrcDataSize = SizeofResource(NULL, rsrcData);
  if (rsrcDataSize <= 0)
    throw std::runtime_error("Size of resource is 0.");

  HGLOBAL grsrcData = LoadResource(NULL, rsrcData);
  if (!grsrcData)
    throw std::runtime_error("Failed to load resource.");

  LPVOID firstByte = LockResource(grsrcData);
  if (!firstByte)
    throw std::runtime_error("Failed to lock resource.");

  std::unique_ptr<sf::Texture> texture = std::make_unique<sf::Texture>();
  if (!texture->loadFromMemory(firstByte, rsrcDataSize))
    throw std::runtime_error("Failed to load image from memory.");

  return texture;
}
