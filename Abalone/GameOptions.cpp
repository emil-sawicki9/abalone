#include "GameOptions.h"

void GameOptions::initialize()
{

  int windowX = 200;
  int windowY = 70;
  int windowWidth = 400;
  int windowHeight = 440;

  //music.openFromFile("ambient.ogg");
  //music.setLoop(true);
  //music.play();
  //soundbuffer.loadFromFile("mouseclick.wav");
  //sound.setBuffer(soundbuffer);

  _bigWindow.setPosition((float)windowX, (float)windowY);
  _bigWindow.setSize(sf::Vector2f((float)windowWidth, (float)windowHeight));
  _bigWindow.setFillColor(sf::Color::Color(50, 40, 20, 255));
  _bigWindow.setOutlineColor(sf::Color::Color(90, 80, 60, 255));
  _bigWindow.setOutlineThickness(3.0f);


  _font.loadFromFile("arial.ttf");

  sf::RectangleShape rectShape_tmp;
  sf::FloatRect floatRect_tmp;

  _logo.setString("Options");
  _logo.setFont(_font);
  _logo.setCharacterSize(40);
  floatRect_tmp = _logo.getLocalBounds();
  _logo.setPosition(sf::Vector2f(windowWidth - floatRect_tmp.width / 2, windowY + 50 - floatRect_tmp.height / 2));

  _save.setString("Save");
  _save.setFont(_font);
  _save.setCharacterSize(30);
  floatRect_tmp = _save.getLocalBounds();
  _save.setPosition(sf::Vector2f((float)(windowWidth * 0.75 - floatRect_tmp.width / 2), (float)(windowY + windowHeight - 50 - floatRect_tmp.height / 2)));
  _button.push_back(rectShape_tmp);

  _cancel.setString("Cancel");
  _cancel.setFont(_font);
  _cancel.setCharacterSize(30);
  floatRect_tmp = _cancel.getLocalBounds();
  _cancel.setPosition(sf::Vector2f((float)(windowWidth * 1.25 - floatRect_tmp.width / 2), (float)(windowY + windowHeight - 50 - floatRect_tmp.height / 2)));
  _button.push_back(rectShape_tmp);

  //==================================================================
  // Buttons
  //==================================================================
  for (size_t i = 0; i < _button.size(); i++)
  {
	_button[i].setSize(sf::Vector2f(150.0f, 50.0f));
	_button[i].setFillColor(sf::Color::Color(90, 80, 60, 255));
	_button[i].setOutlineColor(sf::Color::Color(130, 120, 100, 255));
	_button[i].setOutlineThickness(2.0f);
	floatRect_tmp = _button[i].getLocalBounds();
	_button[i].setPosition((float)(windowWidth * (((i + 1)*0.5) + 0.25) - floatRect_tmp.width / 2), (float)(windowY + windowHeight - 50 - floatRect_tmp.height*0.35));
  }

  //==================================================================
  // Radioboxes + labels
  //==================================================================
  for (size_t i = 0; i < 3 ; i++)
  {
	sf::RectangleShape radiobox_tmp;

	radiobox_tmp.setSize(sf::Vector2f(20.0f, 20.0f));
	radiobox_tmp.setFillColor(sf::Color::Color(90, 80, 60, 255));
	radiobox_tmp.setOutlineColor(sf::Color::Color(130, 120, 100, 255));
	radiobox_tmp.setOutlineThickness(2.0f);
	floatRect_tmp = radiobox_tmp.getLocalBounds();
	radiobox_tmp.setPosition(windowX + floatRect_tmp.width, windowY + 50 + 80 * (i + 1) - floatRect_tmp.height / 2);

	_radioBox.push_back(radiobox_tmp);

	sf::Text label_tmp;
	label_tmp.setString(_label_name[i]);
	label_tmp.setFont(_font);
	label_tmp.setCharacterSize(30);
	floatRect_tmp = radiobox_tmp.getGlobalBounds();
	label_tmp.setPosition(floatRect_tmp.left + 50, floatRect_tmp.top - label_tmp.getLocalBounds().height / 4);
	
	_label.push_back(label_tmp);
	
  }

  //==================================================================
  // Checking radiobox
  //==================================================================
  rectShape_tmp.setSize(sf::Vector2f(15.0f, 15.0f));
  rectShape_tmp.setFillColor(sf::Color::White);
  rectShape_tmp.setOutlineColor(sf::Color::Black);
  rectShape_tmp.setOutlineThickness(0.5f);
  floatRect_tmp = rectShape_tmp.getLocalBounds();
  rectShape_tmp.setPosition((float)(windowX + floatRect_tmp.width*1.5 +2), (float)(windowY + 50 + 78 - floatRect_tmp.height/2 ));

  _radioBox.push_back(rectShape_tmp);

}

bool GameOptions::isActive()
{
  return _is_active;
}
void GameOptions::setActive(bool status)
{
  _is_active = status;

  if (status)
  {
	loadOptions(status);
  }
  
  //if (status) music.play();
  //else music.stop();
}

void GameOptions::loadOptions(bool status)
{
  int pervDiff = _difficulty_tmp;

  for (size_t i = 0; i < _label_name.size(); i++)
  {
	if (_label_name[i] == _difficulty)
	{
	  _difficulty_tmp = i;
	  break;
	}
  }

  int lastIndex = _radioBox.size() - 1;
  sf::FloatRect checkGloPos = _radioBox[lastIndex].getGlobalBounds();

  for (int j = pervDiff; j > 0; j--)
  {
	checkGloPos.top -= 80;
  }

  _radioBox[lastIndex].setPosition((float)(checkGloPos.left + 0.5), (float)(checkGloPos.top + 80 * _difficulty_tmp + 0.5));

}

void GameOptions::saveOptions()
{
  _difficulty = _label_name[_difficulty_tmp];
}

int GameOptions::clickOption(sf::Event event)
{
  sf::Vector2f local_click;

  local_click.x = (float)event.mouseButton.x;
  local_click.y = (float)event.mouseButton.y;
  for (size_t i = 0; i < _button.size(); i++)
  {
	if (_button[i].getGlobalBounds().contains(local_click))
	{
	  //sound.play();
	  return i;
	}
  }
  for (size_t i = 0; i < _radioBox.size()-1; i++)
  {
	if (_radioBox[i].getGlobalBounds().contains(local_click) || _label[i].getGlobalBounds().contains(local_click))
	{
	  if (_difficulty_tmp == i)
		break;

	  int lastIndex = _radioBox.size() - 1;
	  sf::FloatRect checkGloPos = _radioBox[lastIndex].getGlobalBounds();

	  for (int j = _difficulty_tmp; j > 0; j--)
	  {
		checkGloPos.top -= 80;
	  }

	  _difficulty_tmp = i;
	  _radioBox[lastIndex].setPosition((float)(checkGloPos.left + 0.5), (float)(checkGloPos.top + 80 * i + 0.5));
	}
  }
  return -1;

}

void GameOptions::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
  target.draw(_bigWindow);
  for (size_t i = 0; i < _button.size(); i++)
  {
    target.draw(_button[i]);
  }
  for (size_t i = 0; i < _radioBox.size(); i++)
  {
    target.draw(_radioBox[i]);
  }
  for (size_t i = 0; i < _label.size(); i++)
  {
    target.draw(_label[i]);
  }
  target.draw(_logo);
  target.draw(_save);
  target.draw(_cancel);
}