#ifndef MENUSTATE_H
#define MENUSTATE_H

#include "State.h"
#include "GuiContainer.h"
#include "Utility.h"
#include "ResourceHolder.h"
#include "Button.h"
#include "Label.h"

#include <SFML/Graphics/RenderWindow.hpp>

namespace state
{
  class MenuState : public State
  {
  public:
    MenuState(StateStack &stack, Context::Ptr &context);
    ~MenuState() {};

    virtual void			draw();
    virtual bool			update(sf::Time dt);
    virtual bool			handleEvent(const sf::Event &event);

  private:
    enum OptionNames
    {
      Play,
      Ai,
      Options,
      Exit,
    };

    void                  initialize(Context::Ptr &context);
    void                  setGameType(int gameType);

  private:
    sf::Sprite				    _backgroundSprite;
    GUI::GuiContainer     _guiContainer;
  };
}
#endif // MENUSTATE_H
