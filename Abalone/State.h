#ifndef STATE_H
#define STATE_H

#include "StateIdentifiers.h"
#include "ResourceIdentifiers.h"
#include "Globals.h"

#include <SFML/System/Time.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/View.hpp>

#include <memory>

namespace sf
{
  class RenderWindow;
}

namespace state
{
  class StateStack;

  class State
  {
  public:

    struct Context
    {
      typedef std::shared_ptr<Context> Ptr;

      Context(std::shared_ptr<sf::RenderWindow> &window, std::shared_ptr<TextureHolder> &textures, std::shared_ptr<FontHolder> &fonts, std::shared_ptr<sf::View> &gameView)
        : window(window)
        , textures(textures)
        , fonts(fonts)
        , gameView(gameView)
        , winColor(0)
        , optionIndex(0)
        , startingColor(-1)
      {
      }

      struct AiOptions
      {
        AiOptions()
          : whiteAiLevel(AI_LEVEL_NONE)
          , blackAiLevel(AI_LEVEL_NONE)
        {}
        AiType        whiteAiType;
        AiLevel  whiteAiLevel;
        AiType        blackAiType;
        AiLevel  blackAiLevel;
      };

      std::shared_ptr<sf::RenderWindow>   window;
      std::shared_ptr<TextureHolder>	    textures;
      std::shared_ptr<FontHolder>			    fonts;
      std::shared_ptr<sf::View>           gameView;
      int                                 winColor;
      int                                 optionIndex;
      int                                 startingColor;
      AiOptions                           aiOptions;
      GameType                            gameType;
    };

    typedef std::unique_ptr<State> Ptr;

    State(StateStack &stack, Context::Ptr &context);
    virtual	~State() ;

    virtual void		draw() = 0;
    virtual bool		update(sf::Time dt) = 0;
    virtual bool		handleEvent(const sf::Event &event) = 0;
    
  protected:
    void				    requestStackPush(state::ID stateID);
    void			    	requestStackPop();
    void			    	requestStateClear();

    Context::Ptr				  getContext() const;
    
  private:
    StateStack*			      _stack;
    Context::Ptr				  _context;
  };
}

#endif // STATE_H
