#ifndef _GLOBALS_H_
#define _GLOBALS_H_

#include <iostream>

#define DEBUG_WITH_LINE std::cout << "\n" << __FUNCSIG__ << " :[" << __LINE__ << "] "

#define BOARD_SIZE    9
#define PAWN_EMPTY    0
#define PAWN_BLACK    1
#define PAWN_WHITE    2
#define PAWN_OUTSIDE  4
#define PAWN_CLICKED  8

#define PAWN_MAX_NUMBER  14

enum MoveDirection
{
  MOVE_DIR_R_L = 0,
  MOVE_DIR_D_U,
  MOVE_DIR_D_R
};

enum MoveAngle
{
  MOVE_ANGLE_0 = 0,
  MOVE_ANGLE_60,
  MOVE_ANGLE_120,
  MOVE_ANGLE_180,
  MOVE_ANGLE_240,
  MOVE_ANGLE_300
};

enum AiLevel
{
  AI_LEVEL_NONE = 0,
  AI_LEVEL_EASY,
  AI_LEVEL_NORMAL,
  AI_LEVEL_HARD
};

enum AiType
{
  AI_TYPE_MINIMAX = 0,
  AI_TYPE_ALPHABETA,
  AI_TYPE_ALPHABETA_TT,
  AI_TYPE_MTDF
};

enum GameType
{
  GAME_PVP = 0,
  GAME_PVE,
  GAME_AI
};

#endif //_GLOBALS_H_