#ifndef NODE_H
#define NODE_H

#include "Verificator.h"

namespace ai
{
  class Node : public std::enable_shared_from_this<Node>
  {
  public:
    typedef std::shared_ptr<Node> Ptr;

    Node(const abalone::GameData::Ptr &data, const Node::Ptr parentNode);
    ~Node();

    int                                 getChildrenSize() const;
    const abalone::GameData::Ptr        getGameData() const;
    Node::Ptr                           getChild(int index) const;
    bool                                isHitMove() const;
    int                                 calculateChildren(int color);
    void                                clearChildren();
    void                                setBestChild(int i);
    const Node::Ptr                     getBestChild() const;
    void                                setMoveData(abalone::MoveData moveData);
    abalone::MoveData                   getMoveData() const;
    abalone::MoveData                   getBestMove() const;

  protected:
    void                                setMoveData(std::shared_ptr<pointVector> &clickedPawns, int angle, int dir);
    void                                addChildFor(const sf::Vector2i pawnPos);
    bool                                checkClickedColor(std::shared_ptr<pointVector> &clicked, abalone::GameData::Ptr &data);
    bool                                checkMove(std::shared_ptr<pointVector> &clicked, int dir, int angle, abalone::GameData::Ptr &data);
    std::shared_ptr<pointVector>        getClicked(int dir, int step, sf::Vector2i pawnPos);

  private:
    std::weak_ptr<Node>                 _parent;
    std::vector<Node::Ptr>              _children;
    abalone::GameData::Ptr              _data;
    bool                                _hitMove;
    abalone::MoveData                   _moveData;
    int                                 _bestChild;

  };
}
#endif // NODE_H